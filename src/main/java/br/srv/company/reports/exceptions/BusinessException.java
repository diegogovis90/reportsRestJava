package br.srv.company.reports.exceptions;

public class BusinessException extends Exception{
	private static final long serialVersionUID = 1L;

	public BusinessException(String msg){
		super(msg);
	}
	
	public BusinessException(Throwable ex){
		super(ex);
	}
	
	public BusinessException(String msg, Throwable ex){
		super(msg, ex);
	}
}
