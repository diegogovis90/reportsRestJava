package br.srv.company.reports.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import br.srv.company.reports.model.ErroValidacao;

public class ValidacaoUtil {

	public static List<ErroValidacao> processarErros(BindingResult bind){
		List<ErroValidacao> erros = new ArrayList<>();
		for (ObjectError error : bind.getAllErrors()){
			Object[] arguments = error.getArguments();
			Object argument = arguments[arguments.length - 1];
			String field = null;
			String propriedadeListaComErro = null;
			String nomeCampoLista = null;
			Boolean erroEmLista = false;
			
			if(arguments[0] instanceof MessageSourceResolvable) {
				propriedadeListaComErro = ((MessageSourceResolvable) arguments[0]).getDefaultMessage();
				erroEmLista = propriedadeListaComErro.contains("[");
				if(erroEmLista) {
					Integer indiceInicioColchete = propriedadeListaComErro.indexOf("[")+1;
					Integer indiceFimColchete = propriedadeListaComErro.indexOf("]");
					Integer indiceObjetoLista = Integer.parseInt(propriedadeListaComErro.substring(indiceInicioColchete, indiceFimColchete));
					
					field = ((MessageSourceResolvable) argument).getDefaultMessage();
					nomeCampoLista = ((MessageSourceResolvable) arguments[0]).getDefaultMessage();
					erros.add(new ErroValidacao(field, error.getDefaultMessage(), indiceObjetoLista, nomeCampoLista));
				}
			}
			
			if(!erroEmLista) {
				if(argument instanceof MessageSourceResolvable){
					field = ((MessageSourceResolvable) argument).getDefaultMessage();
					erros.add(new ErroValidacao(field, error.getDefaultMessage()));
				}
			}
			
			
		}
		return erros;
	}
}

