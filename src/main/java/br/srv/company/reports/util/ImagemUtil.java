package br.srv.company.reports.util;

public class ImagemUtil {
	public static String obterTipoConteudo(String formato) {
		String tipoConteudo;
		switch (formato) {
		case "csv":
			tipoConteudo = "text/csv";
		case "doc":
			tipoConteudo = "application/msword";
		case "docx":
			tipoConteudo = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		case "gif":
			tipoConteudo = "image/gif";
		case "jpg":
			tipoConteudo = "image/jpeg";
		case "jpeg":
			tipoConteudo = "image/jpeg";
		case "ods":
			tipoConteudo = "application/vnd.oasis.opendocument.spreadsheet";
		case "odt":
			tipoConteudo = "application/vnd.oasis.opendocument.text";
		case "pdf":
			tipoConteudo = "application/pdf";
		case "png":
			tipoConteudo = "image/png";
		case "xls":
			tipoConteudo = "application/vnd.ms-excel";
		case "xlsx":
			tipoConteudo = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		default:
			tipoConteudo = "application/octet-stream";
		}
		return tipoConteudo;
	}
}
