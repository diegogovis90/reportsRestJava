package br.srv.company.reports.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RequestUtil {

	public static String getUsuarioCorrente() {
		return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	public static String getIp() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    	return IpUtil.getRemoteIp(request);
	}
}
