package br.srv.company.reports.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.srv.company.reports.pesquisa.Campo;
import br.srv.company.reports.pesquisa.Filtro;
import br.srv.company.reports.pesquisa.FiltroSubstring;
import br.srv.company.reports.pesquisa.Ordem;
import br.srv.company.reports.pesquisa.Pagina;
import br.srv.company.reports.pesquisa.TipoFiltro;
import br.srv.company.reports.pesquisa.TipoOrdem;

public class PesquisaUtil {


	public static Pagina pagina(Integer numero, Integer limiteRegistros){
		if(numero != null && limiteRegistros != null){
			return new Pagina(numero, limiteRegistros);
		}
		return null;
	}
	
	public static Ordem ordem(String order, String orderBy){
		TipoOrdem tpOrdem = null;
		if(orderBy == null || (tpOrdem = TipoOrdem.apartirAbreviacao(order)) == null){
			return null;
		}
		return new Ordem(orderBy, tpOrdem);
	}
	
	public static Ordem ordem(String order, String orderBy, DePara dePara){
		TipoOrdem tpOrdem = null;
		if(orderBy == null || (tpOrdem = TipoOrdem.apartirAbreviacao(order)) == null 
				|| dePara.get(orderBy) == null){
			return null;
		}
		return new Ordem(dePara.get(orderBy).get(0), tpOrdem);
	}
	
	public static List<Ordem> ordens(String order, String orderBy, DePara dePara){
		final TipoOrdem tpOrdem;
		if(orderBy == null || (tpOrdem = TipoOrdem.apartirAbreviacao(order)) == null 
				|| dePara.get(orderBy) == null){
			return null;
		}
		List<Ordem> ordens = new ArrayList<>();
		dePara.get(orderBy).forEach(_orderBy -> {
			ordens.add(new Ordem(_orderBy, tpOrdem));
		});
		return ordens;
	}
	
	public static Filtro like(String campo, String valor){
		if(campo != null && valor != null){
			return new Filtro(campo, "%" + valor + "%", TipoFiltro.LIKE);
		}
		return null;
	}
	
	public static Filtro likePre(String campo, String valor){
		if(campo != null && valor != null){
			return new Filtro(campo, "%" + valor, TipoFiltro.LIKE);
		}
		return null;
	}
	
	public static Filtro likePos(String campo, String valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor + "%", TipoFiltro.LIKE);
		}
		return null;
	}
	
	public static Filtro equal(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.EQUAL);
		}
		return null;
	}
	
	public static Filtro notEqual(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.NOT_EQUAL);
		}
		return null;
	}
	
	public static Filtro lessThan(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.LESS_THAN);
		}
		return null;
	}
	
	public static Filtro lessThanOrEqualTo(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.LESS_THAN_OR_EQUAL_TO);
		}
		return null;
	}
	
	public static Filtro greaterThan(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.GREATER_THAN);
		}
		return null;
	}
	
	public static Filtro greaterThanOrEqualTo(String campo, Object valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.GREATER_THAN_OR_EQUAL_TO);
		}
		return null;
	}
	
	public static Filtro substring(String campo, FiltroSubstring valor){
		if(campo != null && valor != null){
			return new Filtro(campo, valor, TipoFiltro.SUBSTRING);
		}
		return null;
	}
	
	public static Campo campo(String campo){
		if(campo != null){
			return new Campo(campo);
		}
		return null;
	}
	
	public static Campo campo(String campo, boolean condicional){
		if(campo != null && condicional){
			return new Campo(campo);
		}
		return null;
	}
	
	public static Filtro in(String campo, Collection<?> valores){
		if(campo != null && valores != null){
			return new Filtro(campo, valores, TipoFiltro.IN);
		}
		return null;
	}
	
	public static class DePara{
		private Map<String, List<String>> depara = new HashMap<>();

		public DePara add(String de, String ...para){
			if(de != null && para != null && para.length > 0){
				depara.put(de, Arrays.asList(para));
			}
			return this;
		}
		
		public List<String> get(String de){
			return depara.get(de);
		}
	}
}
