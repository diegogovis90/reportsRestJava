package br.srv.company.reports.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;

import br.srv.company.reports.dto.FiltroRelatorioDto;
import br.srv.company.reports.dto.OpcaoFiltroRelatorio;
import br.srv.company.reports.model.Consulta;
import br.srv.company.reports.model.FonteDeDados;
import br.srv.company.reports.model.Parametro;

public class ParametroUtil {
  public static String substituirParametros(String query, List<FiltroRelatorioDto> parametros) {
    String result = query;
    try (Scanner scanner = new Scanner(query)) {

      ArrayList<String> buffer = new ArrayList<>();
      while (scanner.findWithinHorizon("\\$P\\{([A-Za-z_]+)\\}", 0) != null) {
        MatchResult matchResult = scanner.match();
        for (int i = 1; i <= matchResult.groupCount(); i++) {
          if (buffer.contains(matchResult.group(i))) {
            continue;
          }

          String value = "''";
          for (FiltroRelatorioDto parametro : parametros) {
            if (parametro.getNome().equals(matchResult.group(i))) {
              buffer.add(matchResult.group(i));

              if (parametro.getTipo().toLowerCase().endsWith("string") && parametro.getValor() != null) {
                value = new StringBuilder("'").append(parametro.getValor()).append("'").toString();
              } else if (parametro.getTipo().toLowerCase().endsWith("integer")) {
                if (parametro.getValor() != null) {
                  value = parametro.getValor().toString();
                } else {
                  value = "0";
                }
              }
              break;
            }
          }
          result = result.replaceAll("\\$P\\{" + matchResult.group(i) + "\\}", value);
        }
      }
    }

    return result;
  }

  public static String substituirLocais(String query, Parametro parametro) {

    // TODO: [CARREGAMENTO DE FILTRO]
    // -- Implementar substituição de locais na consulta
    // - Obter usuário logado com suas ugs
    // - Obter todas as ugs do acesso
    // ------

    // Scanner scanner = new Scanner(query);
    // List<Local> locais;
    // if (usuario.getUsuCPF().equals("111.111.111-11")) {
    // locais = locaisacesso;
    // } else {
    // locais =
    // Local.converterUGacesso(usuario.getAcesso().getUGsExecucao().getUG());
    // }
    // boolean consolidador = locais.size() == locaisacesso.size();

    // if (query.contains("$D{CONS}")) {
    // parametro.getControle().setConObrigatorio(!consolidador);
    // } else if (query.contains("$D{CONSV}")) {
    // parametro.getControle().setConVisivel(consolidador);
    // }

    // String s;
    // while ((s = scanner.findWithinHorizon("\\$D\\{(LOCAIS|CONS|CONSV)\\}", 0)) !=
    // null) {
    // MatchResult matchResult = scanner.match();

    // if (matchResult.group(1).startsWith("CONS")) {
    // query = query.replace(s, (locais.size() == locaisacesso.size() ? "1" :
    // "0"));
    // } else {
    // StringBuilder ugList = new StringBuilder();
    // for (Local local : locais) {
    // if (ugList.length() != 0) {
    // ugList.append(", ");
    // }

    // ugList.append("'").append(local.getLocCodigo()).append("'");
    // }
    // query = query.replace(s, String.format("(%s)", ugList.toString()));
    // }
    // }
    return query;
  }

  public static List<OpcaoFiltroRelatorio> opcoesPorConsulta(Parametro parametro, String consulta)
      throws SQLException, Exception {

    Consulta consultaBanco = (Consulta) parametro.getPropriedadeControle();
    FonteDeDados fonte = consultaBanco.getFonte();

    String[] colunasExibicao = consultaBanco.getCosColunasExibicao().equals("") ? new String[] {}
        : consultaBanco.getCosColunasExibicao().split(";");

    List<OpcaoFiltroRelatorio> resultado = new ArrayList<>();

    try (Connection connection = DriverManager.getConnection(fonte.getFddUrl(), fonte.getFddUsuario(),
        CryptoUtil.decodificar(fonte.getFddSenha()))) {
      try (PreparedStatement stmt = connection.prepareStatement(consulta)) {
        ResultSet rSet = stmt.executeQuery();
        while (rSet.next()) {
          String label = "";
          for (String coluna : colunasExibicao) {
            if (!label.isEmpty()) {
              label += " | ";
            }
            label += rSet.getString(coluna);
          }
          resultado
              .add(new OpcaoFiltroRelatorio(label, String.valueOf(rSet.getObject(consultaBanco.getCosColunaValor()))));
        }
      }

    }

    return resultado;

  }

  public static Object obterValorFiltro(Parametro param, String[] valor) {
    if (!param.getControle().getConVisivel() || valor == null || valor.length == 0 || valor[0].isBlank()) {
      return null;
    }

    if (param.getParTipoClasse().toLowerCase().endsWith("integer")) {
        try {
          return Integer.parseInt(valor[0]);
        } catch (Exception e) {
          return null;
        }
    } else if (param.getParTipoClasse().toLowerCase().endsWith("boolean")) {
    	try {
    		return Boolean.parseBoolean(valor[0]);
        } catch (Exception e) {
            return null;
        }
    } else {
    	return valor[0];
    }
  }
}