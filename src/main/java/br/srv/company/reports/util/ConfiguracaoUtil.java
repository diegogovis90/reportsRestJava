package br.srv.company.reports.util;

import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.Properties;

public final class ConfiguracaoUtil {
  private static final String PARAMETRO_CONFIGURACAO = "spring.config.location";
  private static final String ARQUIVO_CONFIGURACAO_BASE = "application.properties";
  private static final String ARQUIVO_CONFIGURACAO_INICIAL = "acesso.properties";
  private static final Integer ID_USUARIO_MASTER = 1;

  public static boolean temParametroComConfigExterna(String[] args) {
    if (args != null) {
      for (String arg : args) {
        if (arg.contains(PARAMETRO_CONFIGURACAO))
          return true;
      }
    }
    return false;
  }

  public static boolean temConfigInterna() {
    return carregaArquivo(ARQUIVO_CONFIGURACAO_BASE) != null;
  }

  public static Properties carregaConfigAcesso() {
    try {
      Properties configInicial = new Properties();
      configInicial.load(carregaArquivo(ARQUIVO_CONFIGURACAO_INICIAL));

      URL url = new URL(configInicial.getProperty("acesso.url"));
      
      String dsConfig = "";

      Properties configAcesso = new Properties();
      configAcesso.load(new StringReader(dsConfig));

      return configAcesso;
    } catch (Exception e) {
      throw new RuntimeException("Nao foi possivel carregar configuracoes de acesso.", e);
    }
  }

  private static InputStream carregaArquivo(String nome) {
    return ConfiguracaoUtil.class.getClassLoader().getResourceAsStream(nome);
  }
}
