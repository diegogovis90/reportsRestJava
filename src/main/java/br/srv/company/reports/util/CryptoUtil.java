package br.srv.company.reports.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

public class CryptoUtil {

  private static final String METODO_ENCRIPTACAO = "AES";
  private static final byte[] CHAVE = { 85, 10, 0, -25, 68, 88, 46, 37, 107, 48, 10, -1, -37, -90, 70, -36 };

  public static String codificar(String value) throws Exception {

    try {
      SecretKeySpec skeySpec = new SecretKeySpec(CHAVE, METODO_ENCRIPTACAO);

      Cipher cipher = Cipher.getInstance(METODO_ENCRIPTACAO);
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
      byte[] encrypted = cipher.doFinal(value.getBytes());

      return new String(Base64.encode(encrypted));
    } catch (Exception e) {
      throw new Exception("Erro ao codificar informações " + e.getMessage());
    }
  }

  public static String decodificar(String encrypted) throws Exception {

    byte[] decrypted = null;

    try {
      SecretKeySpec skeySpec = new SecretKeySpec(CHAVE, METODO_ENCRIPTACAO);

      byte[] decoded = Base64.decode(encrypted);

      Cipher cipher = Cipher.getInstance(METODO_ENCRIPTACAO);
      cipher.init(Cipher.DECRYPT_MODE, skeySpec);
      decrypted = cipher.doFinal(decoded);
    } catch (Exception e) {
      throw new Exception("Erro ao decodificar informações " + e.getMessage());
    }

    return new String(decrypted);
  }

}