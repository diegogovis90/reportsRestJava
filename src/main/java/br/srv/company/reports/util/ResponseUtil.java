package br.srv.company.reports.util;

public class ResponseUtil {

  public static String obterContentType(String formato) {
    String contentType = "";
    switch (formato) {
      case "csv":
        contentType = "text/csv";
      case "doc":
        contentType = "application/msword";
      case "docx":
        contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      case "gif":
        contentType = "image/gif";
      case "jpg":
      case "jpeg":
        contentType = "image/jpeg";
      case "ods":
        contentType = "application/vnd.oasis.opendocument.spreadsheet";
      case "odt":
        contentType = "application/vnd.oasis.opendocument.text";
      case "pdf":
        contentType = "application/pdf";
      case "png":
        contentType = "image/png";
      case "xls":
        contentType = "application/vnd.ms-excel";
      case "xlsx":
        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      default:
        contentType = "application/octet-stream";
    }
    return contentType;
  }
}