package br.srv.company.reports.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.srv.company.reports.dao.componente.ExecutorQueryDinamica;
import br.srv.company.reports.pesquisa.Filtro;
import br.srv.company.reports.pesquisa.Pesquisa;

public abstract class GenericJPADao <T extends Serializable> {
	@PersistenceContext
	private EntityManager entityManager;
	protected Class<T> entityClass;
	private ExecutorQueryDinamica<T> dynamicQueryExecutor;
	
	@SuppressWarnings("unchecked")
	public GenericJPADao() {
		this.entityClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	@PostConstruct
	private void init(){
		this.dynamicQueryExecutor = new ExecutorQueryDinamica<>(entityManager, entityClass);
	}
	
	public void salvar(final T entity) {
		getEntityManager().persist(entity);
	}

	public void excluir(final T entity) {
		getEntityManager().remove(entity);
	}

	public T atualizar(final T entity) {
		return getEntityManager().merge(entity);
	}

	public T buscar(final Object id) {
		return getEntityManager().find(entityClass, id);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> buscarPorCodigo(String codigo) {
		final	CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		final CriteriaQuery cq = cb.createQuery();
		Root<T> c = cq.from(entityClass);
		cq.select(c);
		cq.where(cb.equal(c.get("codigo"), codigo));
		return getEntityManager().createQuery(cq).getResultList();
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> buscarTodos() {
		final CriteriaQuery cq = getEntityManager().getCriteriaBuilder()
				.createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}
	
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public Long totalRegistros(List<Filtro> filtros, boolean distinct) {
		return dynamicQueryExecutor.totalRegistros(filtros, distinct);
	}
	
	public List<T> pesquisar(Pesquisa pesquisa) {
		return dynamicQueryExecutor.pesquisar(pesquisa);
	}

}
