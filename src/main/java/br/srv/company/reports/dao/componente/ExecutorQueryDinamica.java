package br.srv.company.reports.dao.componente;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import br.srv.company.reports.pesquisa.Campo;
import br.srv.company.reports.pesquisa.Filtro;
import br.srv.company.reports.pesquisa.FiltroSubstring;
import br.srv.company.reports.pesquisa.Ordem;
import br.srv.company.reports.pesquisa.Pagina;
import br.srv.company.reports.pesquisa.Pesquisa;

public final class ExecutorQueryDinamica<T> {
		private EntityManager em;
		private Class<T> entityClass;
		
		public ExecutorQueryDinamica(EntityManager em, Class<T> entityClass) {
			this.em = em;
			this.entityClass = entityClass;
		}
		
		public List<T> pesquisar(Pesquisa pesquisa){
			CriteriaBuilder builder = em.getCriteriaBuilder();
	        CriteriaQuery<T> cQuery = builder.createQuery(entityClass);
	        CriteriaQuery<T> select = null;
	        if(pesquisa.isDistinct()){
	        	cQuery.distinct(true);
	        }
	        
	        Root<T> from = cQuery.from(entityClass);
	        

	        if(pesquisa.getCampos().isEmpty()){
	            select = cQuery.select(from);
	        }
	        else{
	        	List<Selection<?>> selection = new ArrayList<>();
	        	List<Campo> campos = pesquisa.getCampos();
	        	for (int i = 0; i < campos.size(); i++) {
	        		selection.add(from.get(campos.get(i).getCampo()));
	        	}
	        	select = cQuery.multiselect(selection);
	        }
	        
	        
	        if(!pesquisa.getFiltros().isEmpty()){
	        	List<Predicate> predicates = processarFiltros(pesquisa.getFiltros(), from, builder);
	        	select.where(predicates.toArray(new Predicate[] {}));
	        }
	        
	        if (!pesquisa.getOrdens().isEmpty()) {
	        	List<Order> orders = processarOrdens(pesquisa.getOrdens(), from, builder);
	        	select.orderBy(orders);
	        }
	        TypedQuery<T> listQuery = em.createQuery(select);
	        Pagina pagina = pesquisa.getPagina();
	        if(pagina != null){
	        	listQuery.setFirstResult((pagina.getNumero() - 1) * pagina.getLimiteRegistros());
	            listQuery.setMaxResults(pagina.getLimiteRegistros());
	        }
	        
			return listQuery.getResultList();
		}
			
		
		public Long totalRegistros(List<Filtro> filtros, boolean distinct){
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
			Root<T> root = countQuery.from(entityClass);
	        
	        if(!filtros.isEmpty()){
	        	List<Predicate> predicates = processarFiltros(filtros, root, builder);
	        	countQuery.where(predicates.toArray(new Predicate[] {}));
	        }
	        
	        if(distinct){
	        	countQuery.select(builder.countDistinct(root));
	        }else{
	        	countQuery.select(builder.count(root));
	        }
	        
			return em.createQuery(countQuery).getSingleResult();
		}
		
		private List<Predicate> processarFiltros(List<Filtro> filtros, Root<T> from, CriteriaBuilder builder){
			List<Predicate> predicates = new ArrayList<Predicate>();
			for(Filtro filtro : filtros){
	        	String campoFiltro = filtro.getCampo();
	            Expression<T> expression = getExpression(from, campoFiltro);
	            predicates.add(getPredicate(filtro, builder, expression, from));
	        }

	    	return predicates;
		}
		
		private List<Order> processarOrdens(List<Ordem> ordens, Root<T> from, CriteriaBuilder builder){
			List<Order> ordensCriteira = new ArrayList<>();
			for(Ordem ordem : ordens){
	    		Expression<T> expression = getExpression(from, ordem.getCampo());
	        	switch (ordem.getTipoOrdem()) {
	            	case ASCENDING:
	            		ordensCriteira.add(builder.asc(expression));
	            		break;
	            	case DESCENDING:
	            		ordensCriteira.add(builder.desc(expression));
	            		break;
	            	default : throw new IllegalArgumentException("tipo de Ordem nao suportado. tipoOrdem="+ordem.getTipoOrdem());
	        	}
	        }
	    	return ordensCriteira;
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private Expression<T> getExpression(Root<T> root, String field){
			String[] fields = field.split("\\.");
			int fieldSize = fields.length;
			if(fieldSize == 1){
				return root.get(field);
			}else{
				Join join = null;
				for(int x=0; x < (fieldSize - 1); x++){
					String fieldSelected = fields[x];
					join = (join == null) ? root.join(fieldSelected) : join.join(fieldSelected);
				}
				return join.get(fields[fieldSize - 1]);
			}
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private Predicate getPredicate(Filtro filtro, CriteriaBuilder builder, Expression expression, Root<T> root){
			Object valorFiltro = filtro.getValor();
			switch(filtro.getTipoFiltro()){
				case EQUAL : {
					if(valorFiltro != null) {
						return builder.equal(expression, valorFiltro);
					}else {
						return builder.isNull(expression);
					}
				}
				case NOT_EQUAL : {
					if(valorFiltro != null) {
						return builder.notEqual(expression, valorFiltro);
					}else {
						return builder.isNotNull(expression);
					}
				}
				case LIKE : return builder.like(expression, valorFiltro.toString());
				case IN : return expression.in((Collection) valorFiltro);
				case GREATER_THAN : {
					if(valorFiltro instanceof LocalDate) 
						return builder.greaterThan(expression, (LocalDate)valorFiltro);
					if(valorFiltro instanceof LocalDateTime) 
						return builder.greaterThan(expression, (LocalDateTime)valorFiltro);
					if(valorFiltro instanceof Integer) 
						return builder.greaterThan(expression, (Integer)valorFiltro);
					if(valorFiltro instanceof BigDecimal)
						return builder.greaterThan(expression, (BigDecimal)valorFiltro);
					throw new IllegalArgumentException(valorFiltro.getClass() + " não oferece suporte para tipo de filtro GREATER_THAN");
				}
				case GREATER_THAN_OR_EQUAL_TO : {
					if(valorFiltro instanceof LocalDate) 
						return builder.greaterThanOrEqualTo(expression, (LocalDate)valorFiltro);
					if(valorFiltro instanceof LocalDateTime) 
						return builder.greaterThanOrEqualTo(expression, (LocalDateTime)valorFiltro);
					if(valorFiltro instanceof Integer) 
						return builder.greaterThanOrEqualTo(expression, (Integer)valorFiltro);
					if(valorFiltro instanceof Long) 
						return builder.greaterThanOrEqualTo(expression, (Long)valorFiltro);
					if(valorFiltro instanceof BigDecimal)
						return builder.greaterThanOrEqualTo(expression, (BigDecimal)valorFiltro);
					throw new IllegalArgumentException(valorFiltro.getClass() + " não oferece suporte para tipo de filtro GREATER_THAN_OR_EQUAL_TO");
				}
				case LESS_THAN : {
					if(valorFiltro instanceof LocalDate) 
						return builder.lessThan(expression, (LocalDate)valorFiltro);
					if(valorFiltro instanceof LocalDateTime) 
						return builder.lessThan(expression, (LocalDateTime)valorFiltro);
					if(valorFiltro instanceof Integer) 
						return builder.lessThan(expression, (Integer)valorFiltro);
					if(valorFiltro instanceof BigDecimal)
						return builder.lessThan(expression, (BigDecimal)valorFiltro);
					throw new IllegalArgumentException(valorFiltro.getClass() + " não oferece suporte para tipo de filtro LESS_THAN: ");
				}
				case LESS_THAN_OR_EQUAL_TO : {
					if(valorFiltro instanceof LocalDate) 
						return builder.lessThanOrEqualTo(expression, (LocalDate)valorFiltro);
					if(valorFiltro instanceof LocalDateTime) 
						return builder.lessThanOrEqualTo(expression, (LocalDateTime)valorFiltro);
					if(valorFiltro instanceof Integer) 
						return builder.lessThanOrEqualTo(expression, (Integer)valorFiltro);
					if(valorFiltro instanceof Long) 
						return builder.lessThanOrEqualTo(expression, (Long)valorFiltro);
					if(valorFiltro instanceof BigDecimal)
						return builder.lessThanOrEqualTo(expression, (BigDecimal)valorFiltro);
					throw new IllegalArgumentException(valorFiltro.getClass() + " não oferece suporte para tipo de filtro LESS_THAN_OR_EQUAL_TO: ");
				}

				case SUBSTRING : {
					if(valorFiltro instanceof FiltroSubstring) {
						Expression expressaoCampo1, expressaoCampo2 = null;
						Predicate like = null;
						FiltroSubstring filtroSubstring = (FiltroSubstring) valorFiltro;
						
						if(filtroSubstring.getStartIndexCampo1() != null && filtroSubstring.getLengthCampo1() != null) {
							expressaoCampo1 = builder.substring(expression, filtroSubstring.getStartIndexCampo1(), filtroSubstring.getLengthCampo1());
						}else {
							expressaoCampo1 = expression;
						}
						
						if(filtroSubstring.getStartIndexCampo2() != null && filtroSubstring.getLengthCampo2() != null) {
							expressaoCampo2 = builder.substring(getExpression(root, filtroSubstring.getNomeCampo2()).as(String.class), 
									filtroSubstring.getStartIndexCampo2(), filtroSubstring.getLengthCampo2());
						}else {
							expressaoCampo2 = getExpression(root, filtroSubstring.getNomeCampo2()).as(String.class);
						}
						if(filtroSubstring.getLike()) {
							like = builder.like(expressaoCampo2, builder.concat("%", builder.concat(expressaoCampo1, "%")));
						}else {
							like = builder.notLike(expressaoCampo2, builder.concat("%", builder.concat(expressaoCampo1, "%"))); 
						}
						
						if(filtroSubstring.getIncluirNull()) {
							return builder.or(like, builder.isNull(expressaoCampo2));	
						}else {
							return like;
						}
					}else {
						throw new IllegalArgumentException(valorFiltro.getClass() + " não oferece suporte para tipo de filtro SUBSTRING, use a classe 'FiltroSubstring': ");
					}
				}
				default : throw new IllegalArgumentException("tipo de filtro nao suportado. tipofiltro="+filtro.getTipoFiltro());
			}
		}
}