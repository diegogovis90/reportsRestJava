package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Consulta;

@Repository
public class ConsultaDao extends GenericJPADao<Consulta> {
	public List<Consulta> listar(String termo) {
		String query = "Select new Consulta(pcoID, pcoNome) from PropriedadeControle "
		+ "where pcoTipo = 'Consulta' and pcoNome like '%' + :termo + '%' escape '\\' "
		+ "order by pcoNome";
		
		List<Consulta> consultas = getEntityManager().createQuery(query, Consulta.class)
				.setParameter("termo", termo).getResultList();

		return consultas;
	}
}