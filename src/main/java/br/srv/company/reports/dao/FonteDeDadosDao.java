package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.FonteDeDados;

@Repository
public class FonteDeDadosDao extends GenericJPADao<FonteDeDados> {
	public List<FonteDeDados> listar() {
		String query = "select new FonteDeDados(f.fddID, f.fddNome, "
				+ "(select count(*) from RelatorioModelo rm where rm.fonte = f) + " 
				+ "(select count(*) from Consulta c where c.fonte = f) + "
				+ "(select count(*) from RelatorioUsuario ru where ru.fonte = f)) "
				+ "from FonteDeDados f order by f.fddNome asc";

		List<FonteDeDados> fontes = getEntityManager().createQuery(query, FonteDeDados.class).getResultList();

		return fontes;
	}
}