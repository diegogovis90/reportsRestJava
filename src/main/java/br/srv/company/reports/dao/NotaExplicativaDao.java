package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.NotaExplicativa;

@Repository
public class NotaExplicativaDao extends GenericJPADao<NotaExplicativa> {

}