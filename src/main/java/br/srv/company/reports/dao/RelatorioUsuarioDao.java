package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.RelatorioUsuario;

@Repository
public class RelatorioUsuarioDao extends GenericJPADao<RelatorioUsuario> {

}