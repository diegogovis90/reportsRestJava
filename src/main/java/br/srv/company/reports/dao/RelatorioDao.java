package br.srv.company.reports.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Relatorio;

@Repository
public class RelatorioDao extends GenericJPADao<Relatorio> {

  public Relatorio obterPorAno(Integer id, Integer ano) {
    // FIXME: Refatorar para verificar permissão do usuário ao relatório através do
    // local
    String q = "select rel from Relatorio rel where rel.relID = :id"
        + "          and (select count(ar) from AnosRelatorios ar where ar.relatorio.relID = rel.relID and ar.ano.areAno = :ano) > 0";
    Relatorio rel = getEntityManager().createQuery(q, Relatorio.class).setParameter("id", id).setParameter("ano", ano)
        .getSingleResult();
    return rel;
  }

  public List<Relatorio> listarPorAnoCategoria(Integer ano, Integer categoria) {
    String q = "select rel from AnosRelatorios are join are.relatorio rel where rel.relTipo <> 'RelatorioUsuario' "
        + " and (:ano = 0 or are.ano.areAno = :ano)"
        + " and ((:categoria = 0 and rel.categoria = null) or rel.categoria.catID = :categoria)"
       // + " group by rel.relID, rel.categoria.catID, rel.relNome, rel.relTipo, rel.relMensagem, rel.relCodigo, rel.relOrdenacao, rel.relReferencia, rel.categoria.catOrdenacao, rel.categoria.catNome"
        + " order by rel.categoria.catOrdenacao asc, rel.categoria.catNome asc, rel.relOrdenacao asc, rel.relNome asc";

    List<Relatorio> rels = getEntityManager().createQuery(q, Relatorio.class).setParameter("ano", ano)
        .setParameter("categoria", categoria).getResultList();
    return rels;
  }
  
	public List<Relatorio> listarTodos(int ano, String termo) {
		String q = "select new Relatorio(r.relID, r.relNome, r.relTipo, r.relMensagem, r.relCodigo, " 
				+ "		r.relOrdenacao, c, ars.ano.areAno) from Relatorio r "
				+ "		left join r.categoria c  "
				+ "		join r.anos ars " 
				+ "		where r.relTipo <> 'RelatorioUsuario' "
				+ "		and (:ano = 0 or ars.ano = :ano) "
				+ "		and ( "
				+ "			r.relCodigo like :termo escape '\\' or replace(r.relCodigo, '.', '') like :termo escape '\\' or r.relNome like :termo escape '\\' "
				+ "			or c.catCodigo like :termo escape '\\' or replace(c.catCodigo, '.', '') like :termo escape '\\' or c.catNome like :termo escape '\\' "
				+ "		) "
				+ "	order by ars.ano desc, c.catOrdenacao asc, c.catCodigo asc, r.relOrdenacao asc, r.relCodigo asc ";

		List<Relatorio> rels = getEntityManager().createQuery(q, Relatorio.class).setParameter("ano", ano)
				.setParameter("termo", "%"+termo+"%").getResultList();

		return rels;
	}
	
	public List<Relatorio> listarPorCategoria(int ano, int catID, String termo, int tipoMarcacao){
		String q = "select new Relatorio(r.relID, r.relNome, r.relTipo, r.relMensagem, r.relCodigo, " + 
				"		  r.relOrdenacao, cat, ars.ano.areAno, "
				+ (tipoMarcacao == 0 ? "(case when cat.catID = :catID then 1 else 0 end), " : 
					tipoMarcacao == 2 ? "1, " : "0, ")
				+ " (case when (select count(*) from LocaisRelatorios lr where lr.relatorio.relID = r.relID) > 0 then 1 else 0 end) "
				+ " ) "
				+ " from Relatorio r "
				+ "		left join r.categoria cat "	
				+ "		join r.anos ars " 
				+ "		where r.relTipo <> 'RelatorioUsuario' "
				+ "		and (:ano = 0 or ars.ano = :ano) "
				+ "		and ( "
				+ "			r.relCodigo like :termo escape '\\' or replace(r.relCodigo, '.', '') like :termo escape '\\' or r.relNome like :termo escape '\\' ) "
				+ "	order by ars.ano desc, cat.catOrdenacao asc, cat.catCodigo asc, r.relOrdenacao asc, r.relCodigo asc ";

		TypedQuery<Relatorio> tq = getEntityManager().createQuery(q, Relatorio.class)
				.setParameter("ano", ano)
				.setParameter("termo", "%"+termo+"%");
		
		tq = (tipoMarcacao == 0 ? tq.setParameter("catID", catID) : tq);
				
		List<Relatorio> rels = tq.getResultList();

		return rels;
	}
	
	public List<Relatorio> listarAlteracaoMarcacao(int ano, int catID, String termo, int tipoMarcacao){
		String q = "select new Relatorio(r.relID, r.relCodigo, r.relNome, "
				+ (tipoMarcacao == 2 ? "1" : "0")
				+ " ) "
				+ " from Relatorio r "
				+ "		left join r.categoria cat "	
				+ "		join r.anos ars " 
				+ "		where r.relTipo <> 'RelatorioUsuario' "
				+ "		and ((cat.catID = :catID and :tipoMarcacao = 1) or ((cat.catID is null or cat.catID != :catID) and :tipoMarcacao = 2)) "	
				+ "		and (:ano = 0 or ars.ano = :ano) "
				+ "		and ( "
				+ "			r.relCodigo like :termo escape '\\' or replace(r.relCodigo, '.', '') like :termo escape '\\' or r.relNome like :termo escape '\\' ) "
				+ "	order by ars.ano desc, cat.catOrdenacao asc, cat.catCodigo asc, r.relOrdenacao asc, r.relCodigo asc ";

		List<Relatorio> rels = getEntityManager().createQuery(q, Relatorio.class)
				.setParameter("ano", ano)
				.setParameter("catID", catID)
				.setParameter("termo", "%"+termo+"%")
				.setParameter("tipoMarcacao", tipoMarcacao)
				.getResultList();

		return rels;
	}
	
	public List<Relatorio> listarPorLocal(int ano, int locID, String termo, int tipoMarcacao){
		String q = "select new Relatorio(r.relID, r.relNome, r.relTipo, r.relMensagem, r.relCodigo, " + 
				"		  r.relOrdenacao, cat, ars.ano.areAno, "
				+ (tipoMarcacao == 0 ? "(case when (select lr from LocaisRelatorios lr where lr.relatorio.relID = r.relID and lr.local.locID = :locID) > 0 then 1 else 0 end), " : 
					tipoMarcacao == 2 ? "1, " : "0, ")
				+ " (case when (select count(*) from LocaisRelatorios lr where lr.relatorio.relID = r.relID) > 0 then 1 else 0 end) "
				+ " ) "
				+ " from Relatorio r "
				+ "		left join r.categoria cat "	
				+ "		join r.anos ars " 
				+ "		where r.relTipo <> 'RelatorioUsuario' "
				+ "		and (:ano = 0 or ars.ano = :ano) "
				+ "		and ( "
				+ "			r.relCodigo like :termo escape '\\' or replace(r.relCodigo, '.', '') like :termo escape '\\' or r.relNome like :termo escape '\\' ) "
				+ "	order by ars.ano desc, cat.catOrdenacao asc, cat.catCodigo asc, r.relOrdenacao asc, r.relCodigo asc ";

		TypedQuery<Relatorio> tq = getEntityManager().createQuery(q, Relatorio.class)
				.setParameter("ano", ano)
				.setParameter("termo", "%"+termo+"%");
		
		tq = (tipoMarcacao == 0 ? tq.setParameter("locID", locID) : tq);
				
		List<Relatorio> rels = tq.getResultList(); 
		return rels;
	}
}