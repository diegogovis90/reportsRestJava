package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.ConfiguracaoJob;

@Repository
public class ConfiguracaoJobDao extends GenericJPADao<ConfiguracaoJob> {

}