package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.CampoInformativo;

@Repository
public class CampoInformativoDao extends GenericJPADao<CampoInformativo> {

}