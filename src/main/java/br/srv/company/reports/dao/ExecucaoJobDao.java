package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.ExecucaoJob;

@Repository
public class ExecucaoJobDao extends GenericJPADao<ExecucaoJob> {

}