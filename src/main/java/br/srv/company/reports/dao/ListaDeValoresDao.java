package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.ListaDeValores;

@Repository
public class ListaDeValoresDao extends GenericJPADao<ListaDeValores> {
	public List<ListaDeValores> listar(String termo) {
		String query = "Select new ListaDeValores(pcoID, pcoNome, "
		+ " (select count(i) from ItemDeLista i where i.listaDeValores.pcoID = pc.pcoID)) from PropriedadeControle pc "
		+ " where pcoTipo = 'ListaDeValores' and pcoNome like '%' + :termo + '%' escape '\\' "
		+ " order by pcoNome";
		
		List<ListaDeValores> listas = getEntityManager().createQuery(query, ListaDeValores.class)
				.setParameter("termo", termo).getResultList();

		return listas;
	}
	
	public ListaDeValores buscarComTotalItens(int id) {
		String query = "Select new ListaDeValores(pcoID, pcoNome, "
		+ " (select count(i) from ItemDeLista i where i.listaDeValores.pcoID = pc.pcoID)) from PropriedadeControle pc "
		+ " where pcoID = :id "
		+ " order by pcoNome";
		
		ListaDeValores lista = getEntityManager().createQuery(query, ListaDeValores.class)
				.setParameter("id", id).getSingleResult();

		return lista;
	}
}