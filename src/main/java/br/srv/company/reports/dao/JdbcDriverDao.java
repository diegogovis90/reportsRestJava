package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.JdbcDriver;

@Repository
public class JdbcDriverDao extends GenericJPADao<JdbcDriver> {

}