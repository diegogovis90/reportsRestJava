package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Local;

@Repository
public class LocalDao extends GenericJPADao<Local> {
	public List<Local> listar(String termo) {
		String query = "select new Local(l.locID, l.locCodigo, l.locNome, "
				+ "(select count(*) from LocaisRelatorios lr where lr.local = l.locID)) "
				+ "from Local l "
				+ "where l.locNome like '%'+ :termo +'%' or l.locCodigo like '%'+ :termo +'%'  "
				+ "order by l.locCodigo";

		List<Local> locais = getEntityManager().createQuery(query, Local.class)
				.setParameter("termo", termo).getResultList();

		return locais;
	}
	public void removerRelatorio(Integer locID, Integer relID) {
	    String q = "delete from LocaisRelatorios lr "
	    		+ " where lr.local.locID = :locID and lr.relatorio.relID = :relID";
	    getEntityManager().createQuery(q).setParameter("locID", locID)
	    		.setParameter("relID", relID).executeUpdate();
	}
  
	public void adicionarRelatorio(Integer locID, Integer relID) {
	    String q = "insert into LocaisRelatorios (locID, relID) VALUES (:locID, :relID) ";
	    getEntityManager().createNativeQuery(q).setParameter("locID", locID)
	    		.setParameter("relID", relID).executeUpdate();
	}
}