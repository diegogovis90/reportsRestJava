package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.PropriedadeControle;

@Repository
public class PropriedadeControleDao extends GenericJPADao<PropriedadeControle> {
	
}