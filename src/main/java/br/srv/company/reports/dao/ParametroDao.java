package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Parametro;

@Repository
public class ParametroDao extends GenericJPADao<Parametro> {

  public List<Parametro> obterParametrosDoRelatorio(Integer relID) {
    String q = "select p from Parametro p where p.relatorio.relID = :relID";
    List<Parametro> ps = getEntityManager().createQuery(q, Parametro.class).setParameter("relID", relID)
        .getResultList();
    return ps;
  }
}