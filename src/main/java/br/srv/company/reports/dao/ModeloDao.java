package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Modelo;

@Repository
public class ModeloDao extends GenericJPADao<Modelo> {
	public List<Modelo> listar(String termo) {
		String query = "select new Modelo(m.modID, m.modNome, "
				+ "(select count(*) from RelatorioModelo rm where rm.modelo = m.modID)) "
				+ "from Modelo m "
				+ "where m.modNome like '%'+ :termo +'%' "
				+ "order by m.modNome asc";

		List<Modelo> modelos = getEntityManager().createQuery(query, Modelo.class)
				.setParameter("termo", termo).getResultList();

		return modelos;
	}
	
	public boolean existeNome(String nome, int id) {
		String query = "select m.modNome from Modelo m "
				+ "where m.modNome like :nome and m.modID != :id ";

		int qtd = getEntityManager().createQuery(query, String.class)
				.setParameter("nome", nome).setParameter("id", id).getResultList().size();

		return qtd > 0;
	}
}