package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.LocaisRelatorios;

@Repository
public class LocaisRelatoriosDao extends GenericJPADao<LocaisRelatorios> {

}