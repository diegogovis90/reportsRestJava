package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.RelatorioPermissao;

@Repository
public class RelatorioPermissaoDao extends GenericJPADao<RelatorioPermissao> {

}