package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.RelatorioArquivo;

@Repository
public class RelatorioArquivoDao extends GenericJPADao<RelatorioArquivo> {

}