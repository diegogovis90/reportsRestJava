package br.srv.company.reports.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Imagem;

@Repository
public class ImagemDao extends GenericJPADao<Imagem> {

	public List<Imagem> listar(String termo, List<Integer> exclusoes) {
		String query = "select new Imagem(i.imaID, i.imaNome, i.imaFormato, i.imaData, "
				+ "(select count(*) from RelatoriosModelosImagens rmi where rmi.imagem.imaID = i.imaID) as utilizacao) "
				+ "from Imagem i ";
		if (termo != null) {
			query += "where i.imaNome like '%'+ :termo +'%' escape '\\'";
		}

		if (exclusoes != null && exclusoes.size() > 0) {
			query += "and i.imaID not in :exclusoes ";
		}
		query += "order by i.imaNome asc";

		TypedQuery<Imagem> jpql = getEntityManager().createQuery(query, Imagem.class);
		if (termo != null) {
			jpql = jpql.setParameter("termo", termo);
		}
		if (exclusoes != null) {
			jpql = jpql.setParameter("exclusoes", exclusoes);
		}

		List<Imagem> imgs = jpql.getResultList();

		return imgs;
	}
}