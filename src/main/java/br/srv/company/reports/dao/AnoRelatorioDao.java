package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.AnoRelatorio;

@Repository
public class AnoRelatorioDao extends GenericJPADao<AnoRelatorio> {

}