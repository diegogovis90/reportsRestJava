package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.ConfigControle;

@Repository
public class ConfigControleDao extends GenericJPADao<ConfigControle> {

}