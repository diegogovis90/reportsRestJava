package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.AnosRelatorios;

@Repository
public class AnosRelatoriosDao extends GenericJPADao<AnosRelatorios> {
	public List<AnosRelatorios> obterAnos(Integer relID) {
		String q = "select are from AnosRelatorios are join are.relatorio rel "
				+ " where are.relatorio.relID = :relID "
				+ " order by are.ano.areAno ";

		List<AnosRelatorios> anos = getEntityManager().createQuery(q, AnosRelatorios.class).setParameter("relID", relID)
				.getResultList();
		return anos;
	}
}