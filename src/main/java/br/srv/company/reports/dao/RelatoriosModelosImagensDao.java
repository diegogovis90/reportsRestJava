package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.RelatoriosModelosImagens;

@Repository
public class RelatoriosModelosImagensDao extends GenericJPADao<RelatoriosModelosImagens> {

}