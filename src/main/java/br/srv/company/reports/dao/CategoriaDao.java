package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Categoria;

@Repository
public class CategoriaDao extends GenericJPADao<Categoria> {

  public List<Categoria> listarPorAno(Integer ano) {
    String q = "select cat from AnosRelatorios are join are.relatorio.categoria cat where (:ano = 0 or are.ano.areAno = :ano) group by cat.catID, cat.catNome, cat.catCodigo, cat.catOrdenacao order by cat.catOrdenacao, cat.catNome";

    List<Categoria> cats = getEntityManager().createQuery(q, Categoria.class).setParameter("ano", ano).getResultList();
    return cats;
  }

  public Boolean existeSemCategoria(Integer ano) {
    String q = "select case when count(are) > 0 then true else false end from AnosRelatorios are where (:ano = 0 or are.ano.areAno = :ano) and are.relatorio.categoria = null";
    return getEntityManager().createQuery(q, Boolean.class).setParameter("ano", ano).getSingleResult();
  }
  
  public void removerRelatorio(Integer catID, Integer relID) {
	    String q = "update Relatorio r set r.categoria = null "
	    		+ " where r.categoria.catID = :catID and r.relID = :relID";
	    getEntityManager().createQuery(q).setParameter("catID", catID)
	    		.setParameter("relID", relID).executeUpdate();
  }
  
  public void adicionarRelatorio(Integer catID, Integer relID) {
	    String q = "update Relatorio r set r.categoria.catID = :catID "
	    		+ " where r.relID = :relID";
	    getEntityManager().createQuery(q).setParameter("catID", catID)
	    		.setParameter("relID", relID).executeUpdate();
  }

}