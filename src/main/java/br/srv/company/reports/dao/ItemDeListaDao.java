package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.dto.OpcaoFiltroRelatorio;
import br.srv.company.reports.model.ItemDeLista;

@Repository
public class ItemDeListaDao extends GenericJPADao<ItemDeLista> {

  public List<OpcaoFiltroRelatorio> obterOpcoesPreDefinidas(int pcoID) {
    String q = "select new br.srv.company.reports.dto.OpcaoFiltroRelatorio(i.idlNome, i.idlValor) from ItemDeLista i where i.listaDeValores.pcoID = :pcoID";
    List<OpcaoFiltroRelatorio> ops = getEntityManager().createQuery(q, OpcaoFiltroRelatorio.class)
        .setParameter("pcoID", pcoID).getResultList();
    return ops;
  }
  
  public Integer ultimaPosicao(int pcoID) {
	  String q = "select max(idlPosicao) from ItemDeLista i where i.listaDeValores.pcoID = :pcoID";
	  
	  Integer pos = getEntityManager().createQuery(q, Integer.class)
		        .setParameter("pcoID", pcoID).getSingleResult();
	  return pos;
  }
  
  public void atualizarPosicoes(int pcoID, int posicao) {
	  String q = "update ItemDeLista i set i.idlPosicao = i.idlPosicao-1 where i.listaDeValores.pcoID = :pcoID and i.idlPosicao > :posicao";
	  
	  getEntityManager().createQuery(q).setParameter("pcoID", pcoID).setParameter("posicao", posicao).executeUpdate();
  }
  
  public boolean existeNome(int pcoID, String nome, int itemID) {
	  String q = "select i from ItemDeLista i where i.listaDeValores.pcoID = :pcoID and "
	  		+ " i.idlNome = :nome and i.idlID <> :itemID";
	  
	  List<ItemDeLista> res = getEntityManager().createQuery(q, ItemDeLista.class)
		        .setParameter("pcoID", pcoID)
		        .setParameter("nome", nome)
		        .setParameter("itemID", itemID).getResultList();
	  return !res.isEmpty();
  }

}