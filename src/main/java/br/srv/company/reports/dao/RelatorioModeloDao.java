package br.srv.company.reports.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.Imagem;
import br.srv.company.reports.model.RelatorioModelo;

@Repository
public class RelatorioModeloDao extends GenericJPADao<RelatorioModelo> {
	
	public List<Imagem> obterImagens(int relID) {
		String query = "Select new Imagem(img.imaID, img.imaNome, img.imaFormato, img.imaData, "
				+ "(select count(*) from RelatoriosModelosImagens aux where aux.imagem.imaID = img.imaID) as utilizacao) "
				+ "from Imagem img "
				+ "join img.relatoriosModelosImagens rmi "
				+ "where rmi.relatorioModelo.relID = :relID";
		
		List<Imagem> imgs = getEntityManager().createQuery(query, Imagem.class)
				.setParameter("relID", relID).getResultList();

		return imgs;
	}
}