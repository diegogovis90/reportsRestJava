package br.srv.company.reports.dao;

import org.springframework.stereotype.Repository;

import br.srv.company.reports.model.ColunaRelatorio;

@Repository
public class ColunaRelatorioDao extends GenericJPADao<ColunaRelatorio> {

}