package br.srv.company.reports.pesquisa;

import java.io.Serializable;

public final class Pagina implements Serializable {
	private static final long serialVersionUID = 1L;
	private int numero;
	private int limiteRegistros;
	
	public Pagina(int numero, int limiteRegistros) {
		this.numero = numero;
		this.limiteRegistros = limiteRegistros;
	}

	public int getNumero() {
		return numero;
	}

	public int getLimiteRegistros() {
		return limiteRegistros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + limiteRegistros;
		result = prime * result + numero;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagina other = (Pagina) obj;
		if (limiteRegistros != other.limiteRegistros)
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}

}
