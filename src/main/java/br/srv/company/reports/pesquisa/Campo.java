package br.srv.company.reports.pesquisa;
import java.io.Serializable;

public class Campo  implements Serializable{

		private static final long serialVersionUID = 1L;
		private String campo;
		
		public Campo(String campo) {
			this.campo = campo;
		}
		
		public String getCampo() {
			return campo;
		}
		

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((campo == null) ? 0 : campo.hashCode());
			return result;
		}

		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Campo other = (Campo) obj;
			if (campo == null) {
				if (other.campo != null)
					return false;
		}
			return true;
	}
}
