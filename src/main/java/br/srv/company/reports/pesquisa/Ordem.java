package br.srv.company.reports.pesquisa;

import java.io.Serializable;

public final class Ordem implements Serializable{
	private static final long serialVersionUID = 1L;
	private String campo;
	private TipoOrdem tipoOrdem;
	
	public Ordem(String campo, TipoOrdem tipoOrdem) {
		this.campo = campo;
		this.tipoOrdem = tipoOrdem;
	}
	
	public String getCampo() {
		return campo;
	}
	public TipoOrdem getTipoOrdem() {
		return tipoOrdem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campo == null) ? 0 : campo.hashCode());
		result = prime * result + ((tipoOrdem == null) ? 0 : tipoOrdem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ordem other = (Ordem) obj;
		if (campo == null) {
			if (other.campo != null)
				return false;
		} else if (!campo.equals(other.campo))
			return false;
		if (tipoOrdem != other.tipoOrdem)
			return false;
		return true;
	}
}
