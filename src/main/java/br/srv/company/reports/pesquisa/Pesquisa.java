package br.srv.company.reports.pesquisa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Pesquisa implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean distinct;
	private Pagina pagina;
	private List<Filtro> filtros;
	private List<Campo> campos;
	private List<Ordem> ordens;

	private Pesquisa(PesquisaBuilder pesquisaBuilder) {
		this.pagina = pesquisaBuilder.pagina;
		this.filtros = Collections.unmodifiableList(pesquisaBuilder.filtros);
		this.ordens = Collections.unmodifiableList(pesquisaBuilder.ordens);
		this.campos = Collections.unmodifiableList(pesquisaBuilder.campos);
		this.distinct = pesquisaBuilder.distinct;
		
	}
	
	public Pagina getPagina() {
		return pagina;
	}

	public List<Filtro> getFiltros() {
		return filtros;
	}

	public List<Ordem> getOrdens() {
		return ordens;
	}
	
	public boolean isDistinct() {
		return distinct;
	}
	
	public List<Campo> getCampos() {
		return campos;
	}
	
	public static PesquisaBuilder builder() {
		return new PesquisaBuilder();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campos == null) ? 0 : campos.hashCode());
		result = prime * result + (distinct ? 1231 : 1237);
		result = prime * result + ((filtros == null) ? 0 : filtros.hashCode());
		result = prime * result + ((ordens == null) ? 0 : ordens.hashCode());
		result = prime * result + ((pagina == null) ? 0 : pagina.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pesquisa other = (Pesquisa) obj;
		if (campos == null) {
			if (other.campos != null)
				return false;
		} else if (!campos.equals(other.campos))
			return false;
		if (distinct != other.distinct)
			return false;
		if (filtros == null) {
			if (other.filtros != null)
				return false;
		} else if (!filtros.equals(other.filtros))
			return false;
		if (ordens == null) {
			if (other.ordens != null)
				return false;
		} else if (!ordens.equals(other.ordens))
			return false;
		if (pagina == null) {
			if (other.pagina != null)
				return false;
		} else if (!pagina.equals(other.pagina))
			return false;
		return true;
	}

	public static class PesquisaBuilder{
	
		private boolean distinct;
		private Pagina pagina;
		private List<Campo> campos = new ArrayList<>();
		private List<Filtro> filtros = new ArrayList<>();
		private List<Ordem> ordens = new ArrayList<>();
		
		public PesquisaBuilder(){
		}
		public PesquisaBuilder(Pagina pagina){
			this.pagina = pagina;
		}
		
		public PesquisaBuilder setPagina(Pagina pagina){
			this.pagina = pagina;
			return this;
		}
		
		public PesquisaBuilder setDistinct(boolean distinct){
			this.distinct = distinct;
			return this;
		}
		
		public PesquisaBuilder addOrdem(Ordem ordem){
			if(ordem != null){
				this.ordens.add(ordem);
			}
			return this;
		}
		
		public PesquisaBuilder addOrdens(List<Ordem> ordens){
			if(ordens != null && !ordens.isEmpty()){
				this.ordens.addAll(ordens);
			}
			return this;
		}
		
		public PesquisaBuilder addFiltro(Filtro filtro){
			if(filtro != null){
				this.filtros.add(filtro);
			}
			return this;
		}
		
		public PesquisaBuilder addFiltros(List<Filtro> filtros){
			if(filtros != null && !filtros.isEmpty()){
				this.filtros.addAll(filtros);
			}
			return this;
		}
		
		public PesquisaBuilder addCampo(Campo campo){
			if(campo != null){
				this.campos.add(campo);
			}
			return this;
		}
		
		public PesquisaBuilder addCampos(List<Campo> campos){
			if(campos != null && !campos.isEmpty()){
				this.campos.addAll(campos);
			}
			return this;
		}
		
		public Pesquisa build(){
			/*if(pagina == null && filtros.isEmpty() && ordens.isEmpty()){
				throw new IllegalStateException("Ao menos um parametro de pesquisa deve ser definido");
			}*/
			return new Pesquisa(this);
		}
		
	}
	
}
