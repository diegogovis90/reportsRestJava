package br.srv.company.reports.pesquisa;

import java.io.Serializable;

public class Filtro implements Serializable{
	private static final long serialVersionUID = 1L;
	private String campo;
	private Object valor;
	private TipoFiltro tipoFiltro;
	
	public Filtro(String campo, Object valor, TipoFiltro tipoFiltro) {
		this.valor = valor;
		this.campo = campo;
		this.tipoFiltro = tipoFiltro;
	}
	
	public String getCampo() {
		return campo;
	}
	
	public Object getValor() {
		return valor;
	}

	public TipoFiltro getTipoFiltro() {
		return tipoFiltro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campo == null) ? 0 : campo.hashCode());
		result = prime * result + ((tipoFiltro == null) ? 0 : tipoFiltro.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Filtro other = (Filtro) obj;
		if (campo == null) {
			if (other.campo != null)
				return false;
		} else if (!campo.equals(other.campo))
			return false;
		if (tipoFiltro != other.tipoFiltro)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
}
