package br.srv.company.reports.pesquisa;

public enum TipoOrdem {
	ASCENDING,
	DESCENDING;
	
	public static TipoOrdem apartirAbreviacao(String abreviacao){
		switch(abreviacao){
			case "asc" : return TipoOrdem.ASCENDING;
			case "desc" : return TipoOrdem.DESCENDING;
			default : return null;
		}
	}
}
