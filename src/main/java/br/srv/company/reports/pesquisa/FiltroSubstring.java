package br.srv.company.reports.pesquisa;

import java.util.Objects;

public class FiltroSubstring {

	private Integer startIndexCampo1;
	private Integer lengthCampo1;
	private Integer startIndexCampo2;
	private Integer lengthCampo2;
	private String nomeCampo2;
	private Boolean like;
	private Boolean incluirNull;
	
	//private FiltroSubstring() {}
	
	private FiltroSubstring(FiltroSubstringBuilder builder) {
		this.startIndexCampo1 = builder.startIndexCampo1;
		this.lengthCampo1 = builder.lengthCampo1;
		this.startIndexCampo2 = builder.startIndexCampo2;
		this.lengthCampo2 = builder.lengthCampo2;
		this.nomeCampo2 = builder.nomeCampo2;
		this.like = builder.like;
		this.incluirNull = builder.incluirNull;
	}

	public Integer getStartIndexCampo2() {
		return startIndexCampo2;
	}

	public Integer getLengthCampo2() {
		return lengthCampo2;
	}

	public String getNomeCampo2() {
		return nomeCampo2;
	}
	
	public Integer getStartIndexCampo1() {
		return startIndexCampo1;
	}
	public Integer getLengthCampo1() {
		return lengthCampo1;
	}
	
	public Boolean getLike() {
		return like;
	}

	public Boolean getIncluirNull() {
		return incluirNull;
	}
	
	public static FiltroSubstringBuilder builder() {
		return new FiltroSubstringBuilder();
	}
	
	public static class FiltroSubstringBuilder{
		private Integer startIndexCampo1;
		private Integer lengthCampo1;
		private Integer startIndexCampo2;
		private Integer lengthCampo2;
		private String  nomeCampo2;
		private Boolean like = true;
		private Boolean incluirNull = true;
		
		public FiltroSubstringBuilder() {}

		public FiltroSubstringBuilder setSubstringCampo1(Integer startIndexCampo1, Integer lengthCampo1) {
			Objects.requireNonNull(startIndexCampo1); Objects.requireNonNull(lengthCampo1);
			this.startIndexCampo1 = startIndexCampo1;
			this.lengthCampo1 = lengthCampo1;
			return this;
		}

		public FiltroSubstringBuilder setSubstringCampo2(Integer startIndexCampo2, Integer lengthCampo2) {
			Objects.requireNonNull(startIndexCampo2); Objects.requireNonNull(lengthCampo2);
			this.startIndexCampo2 = startIndexCampo2;
			return this;
		}

		public FiltroSubstringBuilder setNomeCampo2(String nomeCampo2) {
			Objects.requireNonNull(nomeCampo2);
			this.nomeCampo2 = nomeCampo2;
			return this;
		}
		
		public FiltroSubstringBuilder like(Boolean like) {
			Objects.requireNonNull(like);
			this.like = like;
			return this;
		}
		
		public FiltroSubstringBuilder incluirNull(Boolean incluirNull) {
			Objects.requireNonNull(incluirNull);
			this.incluirNull = incluirNull;
			return this;
		}
		
		public FiltroSubstring build() {
			if(nomeCampo2 == null || nomeCampo2.equals("")) {
				throw new IllegalStateException("Nome do campo 2 precisa ser definido");
			}
			return new FiltroSubstring(this);
		}
		
	}
}


