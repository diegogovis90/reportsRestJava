package br.srv.company.reports.pesquisa;

public enum TipoFiltro {
	EQUAL, NOT_EQUAL, LIKE, IN, GREATER_THAN, GREATER_THAN_OR_EQUAL_TO, LESS_THAN, LESS_THAN_OR_EQUAL_TO, SUBSTRING
}
