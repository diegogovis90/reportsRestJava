package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseConverter <E, D> {

	public List<E> toObject(List<D> dtos) {
		List<E> results = new ArrayList<>();
		for(D dto : dtos){
			results.add(toObject(dto));
		}
		return results;
	}
	
	public abstract E toObject(D dto);

	public List<D> toDto(List<E> entities){
		List<D> dtos = new ArrayList<>();
		for(E entity : entities){
			dtos.add(toDto(entity));
		}
		return dtos;
	}
	
	public abstract D toDto(E entity);
	
}
