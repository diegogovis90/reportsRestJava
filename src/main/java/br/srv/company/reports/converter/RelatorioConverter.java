package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.RelatorioAcaoDto;
import br.srv.company.reports.dto.RelatorioBasicoDto;
import br.srv.company.reports.dto.RelatorioDto;
import br.srv.company.reports.model.Relatorio;

@Component
public class RelatorioConverter extends BaseConverter<Relatorio, RelatorioDto> {
  @Autowired
  AnosRelatoriosConverter arconverter;
	
  @Override
  public Relatorio toObject(RelatorioDto dto) {
    // TODO: Implementar
    throw new NotImplementedException();
  }

  @Override
  public RelatorioDto toDto(Relatorio entity) {
    RelatorioDto dto = new RelatorioDto();

    // TODO: Setar referências caso necessário

    dto.setId(entity.getRelID());
    dto.setAnos(arconverter.toInt(entity.getAnos()));
    dto.setNome(entity.getRelNome());
    dto.setTipo(entity.getRelTipo());
    dto.setMensagem(entity.getRelMensagem());
    dto.setCodigo(entity.getRelCodigo());
    dto.setOrdenacao(entity.getRelOrdenacao());

    return dto;
  }
  
	public List<RelatorioBasicoDto> toDtoBasico(List<Relatorio> lista) {
		List<RelatorioBasicoDto> dtos = new ArrayList<RelatorioBasicoDto>();
		
		for(Relatorio entity : lista) {
			RelatorioBasicoDto dto = new RelatorioBasicoDto();

			dto.setId(entity.getRelID());
			dto.setNome(entity.getRelCodigo()+ " - "+entity.getRelNome());
			dto.setCategoriaId(entity.getCategoria() != null ? entity.getCategoria().getCatID() : null);
			dto.setCategoria(entity.getCategoria() != null ? entity.getCategoria().getCatCodigo() + " - " + entity.getCategoria().getCatNome() : null);
			dto.setAno(entity.getAno());
			dto.setTipo(entity.getRelTipo());
			dto.setMarcado(entity.isMarcado());
			dto.setTemLocal(entity.isTemLocal());
			
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	public RelatorioAcaoDto toDtoRelatorioAcao(Relatorio entity) {
		RelatorioAcaoDto dto = new RelatorioAcaoDto();
		
		dto.setId(entity.getRelID());
		dto.setNome(entity.getRelCodigo() + " - " + entity.getRelNome());
		dto.setAcao(entity.getAcao());

		return dto;
	}
}