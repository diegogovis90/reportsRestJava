package br.srv.company.reports.converter;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ParametroFiltroDto;
import br.srv.company.reports.dto.RelatorioModeloBasicoDto;
import br.srv.company.reports.dto.RelatorioModeloDto;
import br.srv.company.reports.dto.RelatorioModeloGerenciamentoDto;
import br.srv.company.reports.model.AnoRelatorio;
import br.srv.company.reports.model.AnosRelatorios;
import br.srv.company.reports.model.Controle;
import br.srv.company.reports.model.FonteDeDados;
import br.srv.company.reports.model.Modelo;
import br.srv.company.reports.model.Parametro;
import br.srv.company.reports.model.PropriedadeControle;
import br.srv.company.reports.model.RelatorioModelo;
import br.srv.company.reports.service.AnoRelatorioService;
import br.srv.company.reports.service.FonteDeDadosService;
import br.srv.company.reports.service.ImagemService;
import br.srv.company.reports.service.ModeloService;
import br.srv.company.reports.service.ParametroService;
import br.srv.company.reports.service.PropriedadeControleService;
import br.srv.company.reports.service.RelatorioModeloService;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Component
public class RelatorioModeloConverter extends BaseConverter<RelatorioModelo, RelatorioModeloDto> {

	@Autowired
	FonteDeDadosConverter fonteConverter;
	@Autowired
	ModeloConverter modeloConverter;
	@Autowired
	RelatorioConverter relatorioConverter;
	@Autowired
	RelatorioModeloService relatorioModeloService;
	@Autowired
	AnoRelatorioService anoService;
	@Autowired
	ModeloService modeloService;
	@Autowired
	FonteDeDadosService fonteService;
//	@Autowired
//	RelatoriosModelosImagensService rmiService;
	@Autowired
	ParametroService parametroService;
	@Autowired
	PropriedadeControleService pcoService;
	@Autowired
	ImagemService imgService;
	
	@Override
	public RelatorioModelo toObject(RelatorioModeloDto dto) {
		// TODO: Implementar
		throw new NotImplementedException();
	}

	@Override
	public RelatorioModeloDto toDto(RelatorioModelo entity) {
		RelatorioModeloDto dto = new RelatorioModeloDto();

		// TODO: Setar referências caso necessário

		dto.setFonte(fonteConverter.toDto(entity.getFonte()));
		dto.setModelo(modeloConverter.toDto(entity.getModelo()));
		dto.setPersonalizavel(entity.getRmoPersonalizavel());

		return dto;
	}

	public RelatorioModeloBasicoDto toBasicoDto(RelatorioModelo entity) {
		RelatorioModeloBasicoDto dto = new RelatorioModeloBasicoDto();

		dto.setFonteDeDadosId(entity.getFonte().getFddID());
		dto.setId(entity.getRelID());
		dto.setModeloId(entity.getModelo().getModID());

		return dto;
	}
	
	public RelatorioModelo toObject(Integer id, RelatorioModeloGerenciamentoDto dto) throws Exception {
		RelatorioModelo obj = new RelatorioModelo();
		RelatorioModelo relPersistido = new RelatorioModelo();
		
		AnoRelatorio ano = anoService.buscar(dto.getAno());
		List<AnosRelatorios> anos = new ArrayList<AnosRelatorios>();
		AnosRelatorios ar = new AnosRelatorios();
		ar.setAno(ano);
		ar.setRelatorio(obj);
		anos.add(ar);
	
		obj.setRelTipo("RelatorioModelo");
		
		if(id != null) {
			relPersistido = relatorioModeloService.buscar(id);
			if(relPersistido == null) {
				throw new EntityNotFoundException();
			}else {
				ar.setAreID(relPersistido.getAnos().get(0).getAreID());
	
				obj.setRelID(relPersistido.getRelID());
				obj.setCategoria(relPersistido.getCategoria());
				obj.setLocais(relPersistido.getLocais());
				obj.setNotasExplicativas(relPersistido.getNotasExplicativas());
				obj.setPermissoes(relPersistido.getPermissoes());
				obj.setRelReferencia(relPersistido.getRelReferencia());
			}
		}
		
		obj.setAnos(anos);
		
		Modelo mod = modeloService.buscar(dto.getModeloId());
		if(mod == null) {
			throw new EntityNotFoundException();
		}
		FonteDeDados fonte = fonteService.buscar(dto.getFonteDeDadosId());
		if(fonte == null) {
			throw new EntityNotFoundException();
		}
		obj.setModelo(mod);
		obj.setFonte(fonte);
		obj.setRelCodigo(dto.getCodigo());
		obj.setRelMensagem(dto.getMensagem());
		obj.setRelNome(dto.getNome());
		obj.setRelOrdenacao(dto.getOrdenacao());
		obj.setRmoPersonalizavel(false);
		
		List<Parametro> parametros = new ArrayList<Parametro>();
		List<Parametro> parametrosPersistidos = parametroService.obterParametrosDoRelatorio(id);
		
		for (ParametroFiltroDto p : dto.getParametros()) {	
			Parametro parametro = new Parametro();
			
			parametro.setParNome(p.getNome());
			parametro.setParOrdem(p.getOrdem());
			parametro.setParTipoControle(p.getTipoControle());
			parametro.setRelatorio(obj);
			
			if (p.getPropriedadeId() != null) {
				PropriedadeControle pco = pcoService.buscar(p.getPropriedadeId());
				if(pco == null) {
					throw new EntityNotFoundException();
				}
				parametro.setPropriedadeControle(pco);
				parametro.setParTipoPropriedade(p.getTipoPropriedade());
			}else {
				parametro.setParTipoPropriedade(0);
			}
			
			JasperReport report = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(mod.getModDados()));
			JRParameter[] parametrosModelo = report.getParameters();
			for(JRParameter par : parametrosModelo) {
				if (par.getName().equals(p.getNome())) {
					parametro.setParTipoClasse(par.getValueClassName());
				}
			}
			
			Controle controle = new Controle();
			controle.setConNome(p.getNome());
			controle.setConRotulo(p.getRotulo());
			controle.setConObrigatorio(p.isObrigatorio());
			controle.setConSomenteLeitura(p.isSomenteLeitura());
			controle.setConVisivel(p.isVisivel());
			controle.setConAtualizarOutrosParametros(true);
			
			List<Parametro> parsIguais = parametrosPersistidos.stream().filter(pp -> pp.getParNome().equals(p.getNome())).collect(Collectors.toList());
			if(parsIguais.size() > 0) {
				parametro.setParID(parsIguais.get(0).getParID());
				controle.setConID(parsIguais.get(0).getControle().getConID());
			}
			
			parametro.setControle(controle);
			
			parametros.add(parametro);
		}		
		obj.setParametros(parametros);
		
//		List<RelatoriosModelosImagens> rmis = new ArrayList<RelatoriosModelosImagens>();
//		for(Integer imgId : dto.getImagens()) {
//			Imagem img = imgService.buscar(imgId);
//			RelatoriosModelosImagens rmi = new RelatoriosModelosImagens();
//			rmi.setImagem(img);
//			rmi.setModelo(obj);
//			
//			rmis.add(rmi);
//		}
//		obj.setImagens(rmis);
		
		return obj;
	}

}