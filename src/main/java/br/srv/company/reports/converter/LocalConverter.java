package br.srv.company.reports.converter;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.LocalDto;
import br.srv.company.reports.model.Local;

@Component
public class LocalConverter extends BaseConverter<Local, LocalDto> {

	@Override
	public Local toObject(LocalDto dto) {
		if (dto == null)
			return null;
		Local local = new Local();
		local.setLocCodigo(dto.getCodigo());
		local.setLocID(dto.getId());
		local.setLocNome(dto.getNome());

		return local;
	}

	@Override
	public LocalDto toDto(Local entity) {
		LocalDto dto = new LocalDto();
		
		dto.setCodigo(entity.getLocCodigo());
		dto.setId(entity.getLocID());
		dto.setNome(entity.getLocNome());
		dto.setUtilizacao(entity.getRelatorios() != null ? entity.getRelatorios().size() : entity.getUtilizacao());
		
		return dto;
	}
}