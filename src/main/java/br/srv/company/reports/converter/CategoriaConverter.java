package br.srv.company.reports.converter;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.CategoriaDto;
import br.srv.company.reports.model.Categoria;

@Component
public class CategoriaConverter extends BaseConverter<Categoria, CategoriaDto> {

	@Override
	public Categoria toObject(CategoriaDto dto) {
		Categoria categoria = new Categoria();
		categoria.setCatCodigo(dto.getCodigo());
		categoria.setCatID(dto.getId());
		categoria.setCatNome(dto.getNome());
		categoria.setCatOrdenacao(dto.getOrdenacao());
		return categoria;
	}

	@Override
	public CategoriaDto toDto(Categoria entity) {
		CategoriaDto dto = new CategoriaDto();
		dto.setId(entity.getCatID());
		dto.setCodigo(entity.getCatCodigo());
		dto.setNome(entity.getCatNome());
		dto.setOrdenacao(entity.getCatOrdenacao());
		return dto;
	}
}