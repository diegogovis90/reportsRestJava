package br.srv.company.reports.converter;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.AnoRelatorioDto;
import br.srv.company.reports.model.AnoRelatorio;

@Component
public class AnoRelatorioConverter extends BaseConverter<AnoRelatorio, AnoRelatorioDto> {

  @Override
  public AnoRelatorio toObject(AnoRelatorioDto dto) {
    // TODO: Implementar
    throw new NotImplementedException();
  }

  @Override
  public AnoRelatorioDto toDto(AnoRelatorio entity) {
    AnoRelatorioDto dto = new AnoRelatorioDto();
    dto.setAno(entity.getAreAno());
    return dto;
  }

}