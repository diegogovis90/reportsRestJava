package br.srv.company.reports.converter;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ImagemDto;
import br.srv.company.reports.model.Imagem;

@Component
public class ImagemConverter extends BaseConverter<Imagem, ImagemDto> {

	@Override
	public Imagem toObject(ImagemDto dto) {
		Imagem img = new Imagem();

		img.setImaDados(dto.getDados());
		img.setImaFormato(dto.getFormato());
		img.setImaData(dto.getData());
		img.setImaID(dto.getId());
		img.setImaNome(dto.getNome());

		return img;
	}

	@Override
	public ImagemDto toDto(Imagem entity) {
		ImagemDto dto = new ImagemDto();
		dto.setId(entity.getImaID());
		dto.setNome(entity.getImaNome());
		dto.setFormato(entity.getImaFormato());
		dto.setUtilizacao(entity.getRelatoriosModelosImagens() != null ? entity.getRelatoriosModelosImagens().size() : entity.getUtilizacao());
		dto.setTamanhoArquivo(entity.getImaDados() != null ? (entity.getImaDados().length / 1024) + " KiB" : "");
		return dto;
	}

}