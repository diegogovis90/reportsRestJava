package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ItemDeListaDto;
import br.srv.company.reports.dto.ListaDeValoresDto;
import br.srv.company.reports.model.ItemDeLista;
import br.srv.company.reports.model.ListaDeValores;
import br.srv.company.reports.service.ListaDeValoresService;

@Component
public class ListaDeValoresConverter extends BaseConverter<ListaDeValores, ListaDeValoresDto> {
	@Autowired
	ItemDeListaConverter itemConverter;
	@Autowired
	ListaDeValoresService listaDeValoresService;
	
	@Override
	public ListaDeValores toObject(ListaDeValoresDto dto) {
		ListaDeValores listaDeValoresPersistido = listaDeValoresService.buscar(dto.getId());
		ListaDeValores lista = new ListaDeValores();
		lista.setPcoID(dto.getId());
		lista.setPcoNome(dto.getNome());
		lista.setPcoTipo("ListaDeValores");
		if(dto.getItens() != null) {
			List<ItemDeLista> itens = new ArrayList<ItemDeLista>();
			for(ItemDeListaDto itemDto : dto.getItens()) {
				ItemDeLista item = itemConverter.toObject(itemDto);
				item.setListaDeValores(lista);
				itens.add(item);
			}
			lista.setItens(itens);
		}else {
			lista.setItens(listaDeValoresPersistido.getItens());
		}
		
		return lista;
	}

	@Override
	public ListaDeValoresDto toDto(ListaDeValores entity) {
		ListaDeValoresDto dto = new ListaDeValoresDto();
		dto.setId(entity.getPcoID());
		dto.setNome(entity.getPcoNome());
		dto.setTipo(entity.getPcoTipo());
		dto.setTotalItens(entity.getTotalItens());
		return dto;
	}

}