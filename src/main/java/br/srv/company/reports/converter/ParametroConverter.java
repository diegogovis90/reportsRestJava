package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ParametroDto;
import br.srv.company.reports.dto.ParametroFiltroDto;
import br.srv.company.reports.model.Parametro;

@Component
public class ParametroConverter extends BaseConverter<Parametro, ParametroDto> {

	@Override
	public Parametro toObject(ParametroDto dto) {
		// TODO: Implementar
		throw new NotImplementedException();
	}

	@Override
	public ParametroDto toDto(Parametro entity) {
		ParametroDto dto = new ParametroDto();
		return dto;
	}

	public ParametroFiltroDto toFiltroDto(Parametro entity) {
		ParametroFiltroDto dto = new ParametroFiltroDto();
		dto.setId(entity.getParID());
		dto.setNome(entity.getParNome());
		dto.setNomeControle(entity.getPropriedadeControle().getPcoNome());
		dto.setObrigatorio(entity.getControle().getConObrigatorio());
		dto.setOrdem(entity.getParOrdem());
		dto.setPropriedadeId(entity.getPropriedadeControle().getPcoID());
		dto.setRotulo(entity.getControle().getConRotulo());
		dto.setSomenteLeitura(entity.getControle().getConSomenteLeitura());
		dto.setTipoControle(entity.getParTipoControle());
		dto.setTipoPropriedade(entity.getParTipoPropriedade());
		dto.setVisivel(entity.getControle().getConVisivel());
		return dto;
	}

	public List<ParametroFiltroDto> toFiltroDto(List<Parametro> lista) {
		List<ParametroFiltroDto> dtos = new ArrayList<ParametroFiltroDto>();
		for (Parametro entity : lista) {
			ParametroFiltroDto dto = new ParametroFiltroDto();
			dto.setId(entity.getParID());
			dto.setNome(entity.getParNome());
			if(entity.getPropriedadeControle() != null) {
				dto.setNomeControle(entity.getPropriedadeControle().getPcoNome());
				dto.setPropriedadeId(entity.getPropriedadeControle().getPcoID());
				dto.setTipoPropriedade(entity.getParTipoPropriedade());
			}else {
				dto.setTipoPropriedade(entity.getParTipoPropriedade());
			}
			dto.setObrigatorio(entity.getControle().getConObrigatorio());
			dto.setOrdem(entity.getParOrdem());
			dto.setRotulo(entity.getControle().getConRotulo());
			dto.setSomenteLeitura(entity.getControle().getConSomenteLeitura());
			dto.setTipoControle(entity.getParTipoControle());
			dto.setVisivel(entity.getControle().getConVisivel());
			
			dtos.add(dto);
		}
		return dtos;
	}

}