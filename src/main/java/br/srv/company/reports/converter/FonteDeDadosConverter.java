package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.FonteDeDadosDto;
import br.srv.company.reports.model.FonteDeDados;
import br.srv.company.reports.util.CryptoUtil;

@Component
public class FonteDeDadosConverter extends BaseConverter<FonteDeDados, FonteDeDadosDto> {

	@Override
	public FonteDeDados toObject(FonteDeDadosDto dto) {
		if (dto == null)
			return null;
		FonteDeDados fonte = new FonteDeDados();
		fonte.setDriver(dto.getDriver());
		fonte.setFddID(dto.getId());
		fonte.setFddNome(dto.getNome());
		fonte.setFddNomeBD(dto.getNomeBD());
		fonte.setFddOpcoes(dto.getOpcoes());
		try {
			fonte.setFddSenha(CryptoUtil.codificar(dto.getSenha()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		fonte.setFddServidor(dto.getServidor());
		fonte.setFddTipo(dto.getTipo());
		fonte.setFddUrl(dto.getUrl());
		fonte.setFddUsuario(dto.getUsuario());

		return fonte;
	}

	@Override
	public FonteDeDadosDto toDto(FonteDeDados entity) {
		FonteDeDadosDto dto = new FonteDeDadosDto();
		long utilizacao = (entity.getRelatorios() != null ? entity.getRelatorios().size() : 0)
				+ (entity.getRelatoriosUsu() != null ? entity.getRelatoriosUsu().size() : 0)
				+ (entity.getConsultas() != null ? entity.getConsultas().size() : 0);

		dto.setId(entity.getFddID());
		dto.setDriver(entity.getDriver());
		dto.setNome(entity.getFddNome());
		dto.setNomeBD(entity.getFddNomeBD());
		dto.setOpcoes(entity.getFddOpcoes());
		try {
			dto.setSenha(CryptoUtil.decodificar(entity.getFddSenha()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		dto.setServidor(entity.getFddServidor());
		dto.setTipo(entity.getFddTipo());
		dto.setUrl(entity.getFddUrl());
		dto.setUsuario(entity.getFddUsuario());
		dto.setTemSenha(entity.getFddSenha() != null);
		dto.setUtilizacao(utilizacao != 0 ? utilizacao : entity.getUtilizacao());
		return dto;
	}

	public List<FonteDeDadosDto> toBasicoDto(List<FonteDeDados> lista) {
		List<FonteDeDadosDto> dtos = new ArrayList<FonteDeDadosDto>();
		for (FonteDeDados fonte : lista) {
			FonteDeDadosDto dto = new FonteDeDadosDto();
			dto.setId(fonte.getFddID());
			dto.setNome(fonte.getFddNome());
			dto.setUtilizacao(fonte.getUtilizacao());
			dtos.add(dto);
		}
		return dtos;
	}

}