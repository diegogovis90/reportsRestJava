package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ConsultaDto;
import br.srv.company.reports.model.Consulta;
import br.srv.company.reports.service.FonteDeDadosService;

@Component
public class ConsultaConverter extends BaseConverter<Consulta, ConsultaDto> {
	@Autowired 
	private FonteDeDadosService fonteService;
	
	@Override
	public Consulta toObject(ConsultaDto dto) {
		Consulta consulta = new Consulta();
		consulta.setPcoID(dto.getId());
		consulta.setPcoTipo("Consulta");
		consulta.setPcoNome(dto.getNome());
		consulta.setFonte(fonteService.buscar(dto.getIdFonteDeDados()));
		consulta.setCosSql(dto.getSql());
		consulta.setCosColunaValor(dto.getColunaValor());
		String exibicao = "";
		for(String col : dto.getColunasExibicao()) {
			if(exibicao.equals("")) {
				exibicao = col;
			}else {
				exibicao += ";"+col;
			}
		}
		consulta.setCosColunasExibicao(exibicao);
		
		return consulta;
	}

	@Override
	public ConsultaDto toDto(Consulta entity) {
		ConsultaDto dto = new ConsultaDto();
		dto.setId(entity.getPcoID());
		dto.setNome(entity.getPcoNome());
		dto.setColunasExibicao(entity.getCosColunasExibicao().split(";"));
		dto.setColunaValor(entity.getCosColunaValor());
		dto.setSql(entity.getCosSql());
		dto.setTipo(entity.getPcoTipo());
		dto.setIdFonteDeDados(entity.getFonte().getFddID());
		return dto;
	}
	
	public List<ConsultaDto> toBasicoDto(List<Consulta> consultas) {
		List<ConsultaDto> dtos = new ArrayList<ConsultaDto>();
		for(Consulta consulta : consultas) {
			ConsultaDto dto = new ConsultaDto();
			dto.setId(consulta.getPcoID());
			dto.setNome(consulta.getPcoNome());
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	public ConsultaDto toBasicoDto(Consulta entity) {
		ConsultaDto dto = new ConsultaDto();
		dto.setId(entity.getPcoID());
		dto.setNome(entity.getPcoNome());
		return dto;
	}

}