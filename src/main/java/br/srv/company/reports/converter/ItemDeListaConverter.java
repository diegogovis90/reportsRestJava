package br.srv.company.reports.converter;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ItemDeListaDto;
import br.srv.company.reports.model.ItemDeLista;

@Component
public class ItemDeListaConverter extends BaseConverter<ItemDeLista, ItemDeListaDto> {

	@Override
	public ItemDeLista toObject(ItemDeListaDto dto) {
		ItemDeLista item = new ItemDeLista();
		item.setIdlID(dto.getId());
		item.setIdlNome(dto.getNome());
		item.setIdlValor(dto.getValor());
		item.setIdlPosicao(dto.getPosicao());
		return item;		
	}

	@Override
	public ItemDeListaDto toDto(ItemDeLista entity) {
		ItemDeListaDto dto = new ItemDeListaDto();
		dto.setId(entity.getIdlID());
		dto.setNome(entity.getIdlNome());
		dto.setValor(entity.getIdlValor());
		dto.setPosicao(entity.getIdlPosicao());
		return dto;
	}

}