package br.srv.company.reports.converter;

import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.ModeloDto;
import br.srv.company.reports.model.Modelo;

@Component
public class ModeloConverter extends BaseConverter<Modelo, ModeloDto> {

	@Override
	public Modelo toObject(ModeloDto dto) {
		if (dto == null)
			return null;
		Modelo modelo = new Modelo();
		modelo.setModCPFCriacao(dto.getCpfCriacao());
		modelo.setModCPFModificacao(dto.getCpfModificacao());
		modelo.setModCPFModificacaoArquivo(dto.getCpfModificacaoArquivo());
		modelo.setModCPFModificacaoArquivoTabulado(dto.getCpfModificacaoArquivoTabulado());
		modelo.setModDados(dto.getDados());
		modelo.setModDados2(dto.getDados2());
		modelo.setModData(dto.getData());
		modelo.setModDataEnvioArquivo(dto.getDataEnvioArquivo());
		modelo.setModDataEnvioArquivoTabulado(dto.getDataEnvioArquivoTabulado());
		modelo.setModDataModificacao(dto.getDataModificacao());
		modelo.setModDataModificacaoArquivo(dto.getDataModificacaoArquivo());
		modelo.setModDataModificacaoArquivoTabulado(dto.getDataModificacaoArquivoTabulado());
		modelo.setModID(dto.getId());
		modelo.setModNome(dto.getNome());
		modelo.setModNomeArquivo(dto.getNomeArquivo());
		modelo.setModNomeArquivoTabulado(dto.getNomeArquivoTabulado());

		return modelo;
	}

	@Override
	public ModeloDto toDto(Modelo entity) {
		ModeloDto dto = new ModeloDto();

		dto.setCpfCriacao(entity.getModCPFCriacao());
		dto.setCpfModificacao(entity.getModCPFModificacao());
		dto.setCpfModificacaoArquivo(entity.getModCPFModificacaoArquivo());
		dto.setCpfModificacaoArquivoTabulado(entity.getModCPFModificacaoArquivoTabulado());
		dto.setDados(entity.getModDados());
		dto.setDados2(entity.getModDados2());
		dto.setData(entity.getModData());
		dto.setDataEnvioArquivo(entity.getModDataEnvioArquivo());
		dto.setDataEnvioArquivoTabulado(entity.getModDataEnvioArquivoTabulado());
		dto.setDataModificacao(entity.getModDataModificacao());
		dto.setDataModificacaoArquivo(entity.getModDataModificacaoArquivo());
		dto.setDataModificacaoArquivoTabulado(entity.getModDataModificacaoArquivoTabulado());
		dto.setId(entity.getModID());
		dto.setNome(entity.getModNome());
		dto.setNomeArquivo(entity.getModNomeArquivo());
		dto.setNomeArquivoTabulado(entity.getModNomeArquivoTabulado());
		dto.setUtilizacao(entity.getRelatorios() != null ? entity.getRelatorios().size() : entity.getUtilizacao());
		dto.setTamanhoArquivo(entity.getModDados() != null ? (entity.getModDados().length / 1024) + " KiB" : "");
		dto.setTamanhoArquivoTabulado(entity.getModDados2() != null ? (entity.getModDados2().length / 1024) + " KiB" : "");

		return dto;
	}
}