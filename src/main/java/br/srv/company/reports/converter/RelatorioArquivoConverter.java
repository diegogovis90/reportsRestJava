package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.RelatorioArquivoDto;
import br.srv.company.reports.model.AnoRelatorio;
import br.srv.company.reports.model.AnosRelatorios;
import br.srv.company.reports.model.RelatorioArquivo;
import br.srv.company.reports.service.AnoRelatorioService;
import br.srv.company.reports.service.RelatorioArquivoService;

@Component
public class RelatorioArquivoConverter extends BaseConverter<RelatorioArquivo, RelatorioArquivoDto> {
	@Autowired
	AnosRelatoriosConverter arconverter;
	@Autowired
	AnoRelatorioService anoService;
	@Autowired
	RelatorioArquivoService relatorioArquivoService;
	
	@Override	
	public RelatorioArquivo toObject(RelatorioArquivoDto dto) {
		RelatorioArquivo relatorioArquivo = new RelatorioArquivo();
		RelatorioArquivo relPersistido = new RelatorioArquivo();
		if (dto.getId() != null) {
			relPersistido = relatorioArquivoService.buscar(dto.getId());
			if(relPersistido == null) {
				throw new EntityNotFoundException();
			} 
			
			relatorioArquivo.setRelID(relPersistido.getRelID());
			relatorioArquivo.setCategoria(relPersistido.getCategoria());
			relatorioArquivo.setLocais(relPersistido.getLocais());
			relatorioArquivo.setNotasExplicativas(relPersistido.getNotasExplicativas());
			relatorioArquivo.setPermissoes(relPersistido.getPermissoes());
			relatorioArquivo.setRelReferencia(relPersistido.getRelReferencia());
		}
		
		relatorioArquivo.setRelTipo("RelatorioArquivo");
		relatorioArquivo.setRelCodigo(dto.getCodigo());
		if(dto.getDados() != null) {
			relatorioArquivo.setRarDados(dto.getDados());
			relatorioArquivo.setRarFormato(dto.getFormato());
		}else {
			relatorioArquivo.setRarDados(relPersistido.getRarDados());
			relatorioArquivo.setRarFormato(relPersistido.getRarFormato());
		}
		
		relatorioArquivo.setRelNome(dto.getNome());
		relatorioArquivo.setRelOrdenacao(dto.getOrdenacao());
		
		List<AnosRelatorios> anos = new ArrayList<AnosRelatorios>();
		for (Integer anoInt : dto.getAnos()) {
			AnosRelatorios ar = new AnosRelatorios();
			AnoRelatorio ano = anoService.buscar(anoInt);
			ar.setAno(ano);
			ar.setRelatorio(relatorioArquivo);
			
			if (relPersistido.getAnos() != null) {
				for (AnosRelatorios arrel : relPersistido.getAnos()) {
					if (arrel.getAno().getAreAno().equals(anoInt)) {
						ar.setAreID(arrel.getAreID());
					}
				}
			}
			anos.add(ar);
		}
		relatorioArquivo.setAnos(anos);
		
		return relatorioArquivo;
	}

	@Override
	public RelatorioArquivoDto toDto(RelatorioArquivo entity) {
		RelatorioArquivoDto dto = new RelatorioArquivoDto();

		dto.setId(entity.getRelID());
		dto.setAnos(arconverter.toInt(entity.getAnos()));
		dto.setNome(entity.getRelNome());
		dto.setTipo(entity.getRelTipo());
		dto.setCodigo(entity.getRelCodigo());
		dto.setOrdenacao(entity.getRelOrdenacao());

		return dto;
	}
}