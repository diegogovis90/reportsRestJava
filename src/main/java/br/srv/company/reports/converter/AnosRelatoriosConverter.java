package br.srv.company.reports.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.srv.company.reports.dto.AnosRelatoriosDto;
import br.srv.company.reports.model.AnosRelatorios;

@Component
public class AnosRelatoriosConverter extends BaseConverter<AnosRelatorios, AnosRelatoriosDto> {
	@Autowired
	AnoRelatorioConverter arConverter;
	RelatorioConverter relConverter;

  @Override
  public AnosRelatorios toObject(AnosRelatoriosDto dto) {
    // TODO: Implementar
    throw new NotImplementedException();
  }

  @Override
  public AnosRelatoriosDto toDto(AnosRelatorios entity) {
    AnosRelatoriosDto dto = new AnosRelatoriosDto();
    dto.setAno(entity.getAno().getAreAno());
    return dto;
  }
  
  public List<Integer> toInt(List<AnosRelatorios> anos) {
	List<Integer> dtos = new ArrayList<Integer>();  
	for(AnosRelatorios are : anos) {
		Integer dto = are.getAno().getAreAno();
		dtos.add(dto);
	}
		
    return dtos;
  }

}