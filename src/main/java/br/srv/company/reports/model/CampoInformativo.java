package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CampoInformativo")
public class CampoInformativo implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cinID")
  private Integer cinID;

  @Column(name = "cinSequencia")
  private Integer cinSequencia;

  @Column(name = "cinTexto")
  private String cinTexto;

  @Column(name = "cinAtivo")
  private boolean cinAtivo;

  @ManyToOne()
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  public Integer getCinID() {
    return this.cinID;
  }

  public void setCinID(Integer cinID) {
    this.cinID = cinID;
  }

  public Integer getCinSequencia() {
    return this.cinSequencia;
  }

  public void setCinSequencia(Integer cinSequencia) {
    this.cinSequencia = cinSequencia;
  }

  public String getCinTexto() {
    return this.cinTexto;
  }

  public void setCinTexto(String cinTexto) {
    this.cinTexto = cinTexto;
  }

  public boolean isCinAtivo() {
    return this.cinAtivo;
  }

  public boolean getCinAtivo() {
    return this.cinAtivo;
  }

  public void setCinAtivo(boolean cinAtivo) {
    this.cinAtivo = cinAtivo;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

}