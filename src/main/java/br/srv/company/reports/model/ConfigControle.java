package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ConfigControle")
public class ConfigControle implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ccoClasse")
  private String ccoClasse;

  @Column(name = "ccoControle")
  private Integer ccoControle;

  public String getCcoClasse() {
    return this.ccoClasse;
  }

  public void setCcoClasse(String ccoClasse) {
    this.ccoClasse = ccoClasse;
  }

  public Integer getCcoControle() {
    return this.ccoControle;
  }

  public void setCcoControle(Integer ccoControle) {
    this.ccoControle = ccoControle;
  }

}