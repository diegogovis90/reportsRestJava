package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ColunaRelatorio")
public class ColunaRelatorio implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "fddID")
  private Integer fddID;

  @Column(name = "colNome")
  private String colNome;

  @Column(name = "colRotulo")
  private String colRotulo;

  @Column(name = "colRotuloPersonalizado")
  private String colRotuloPersonalizado;

  @Column(name = "colVisivel")
  private boolean colVisivel;

  @Column(name = "colPosicao")
  private int colPosicao;

  @Column(name = "colTipo")
  private String colTipo;

  @ManyToOne
  @JoinColumn(name = "relID", referencedColumnName = "relID")
  private RelatorioUsuario relatorioUsuario;

  public Integer getFddID() {
    return this.fddID;
  }

  public void setFddID(Integer fddID) {
    this.fddID = fddID;
  }

  public String getColNome() {
    return this.colNome;
  }

  public void setColNome(String colNome) {
    this.colNome = colNome;
  }

  public String getColRotulo() {
    return this.colRotulo;
  }

  public void setColRotulo(String colRotulo) {
    this.colRotulo = colRotulo;
  }

  public String getColRotuloPersonalizado() {
    return this.colRotuloPersonalizado;
  }

  public void setColRotuloPersonalizado(String colRotuloPersonalizado) {
    this.colRotuloPersonalizado = colRotuloPersonalizado;
  }

  public boolean isColVisivel() {
    return this.colVisivel;
  }

  public boolean getColVisivel() {
    return this.colVisivel;
  }

  public void setColVisivel(boolean colVisivel) {
    this.colVisivel = colVisivel;
  }

  public int getColPosicao() {
    return this.colPosicao;
  }

  public void setColPosicao(int colPosicao) {
    this.colPosicao = colPosicao;
  }

  public String getColTipo() {
    return this.colTipo;
  }

  public void setColTipo(String colTipo) {
    this.colTipo = colTipo;
  }

  public RelatorioUsuario getRelatorioUsuario() {
    return this.relatorioUsuario;
  }

  public void setRelatorioUsuario(RelatorioUsuario relatorioUsuario) {
    this.relatorioUsuario = relatorioUsuario;
  }

}