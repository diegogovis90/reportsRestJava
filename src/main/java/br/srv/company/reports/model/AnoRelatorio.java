package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AnoRelatorio")
public class AnoRelatorio implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "areAno")
  private Integer areAno;

  @OneToMany(mappedBy = "ano", fetch = FetchType.LAZY)
  private List<AnosRelatorios> relatorios;

  public List<AnosRelatorios> getRelatorios() {
	return relatorios;
  }

  public void setRelatorios(List<AnosRelatorios> relatorios) {
	this.relatorios = relatorios;
  }

  public Integer getAreAno() {
    return this.areAno;
  }

  public void setAreAno(Integer areAno) {
    this.areAno = areAno;
  }
}
