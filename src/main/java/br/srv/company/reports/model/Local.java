package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Local")
public class Local implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "locID")
  private Integer locID;

  @Column(name = "locCodigo")
  private String locCodigo;

  @Column(name = "locNome")
  private String locNome;

  @OneToMany(mappedBy = "local")
  private List<LocaisRelatorios> relatorios;
  
  @Transient
  private long utilizacao;
  
  public Local() {}
  
  public Local(String codigo, String nome) {
      this.locCodigo = codigo;
      this.locNome = nome;
  }
  
  public Local(Integer locID, String codigo, String nome, long utilizacao) {
	  this.locID = locID;
      this.locCodigo = codigo;
      this.locNome = nome;
      this.utilizacao = utilizacao;
  }

  public List<LocaisRelatorios> getRelatorios() {
    return this.relatorios;
  }

  public void setRelatorios(List<LocaisRelatorios> relatorios) {
    this.relatorios = relatorios;
  }

  public Integer getLocID() {
    return this.locID;
  }

  public void setLocID(Integer locID) {
    this.locID = locID;
  }

  public String getLocCodigo() {
    return this.locCodigo;
  }

  public void setLocCodigo(String locCodigo) {
    this.locCodigo = locCodigo;
  }

  public String getLocNome() {
    return this.locNome;
  }

  public void setLocNome(String locNome) {
    this.locNome = locNome;
  }

  public long getUtilizacao() {
	return utilizacao;
  }

  public void setUtilizacao(long utilizacao) {
	this.utilizacao = utilizacao;
  }
}