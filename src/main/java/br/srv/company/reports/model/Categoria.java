package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Categoria")
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "catID")
	private Integer catID;

	@Column(name = "catCodigo")
	private String catCodigo;

	@Column(name = "catNome")
	private String catNome;

	@Column(name = "catOrdenacao")
	private Integer catOrdenacao;

	@OneToMany(mappedBy = "categoria")
	private List<Relatorio> relatorios;

	public Integer getCatID() {
		return this.catID;
	}

	public void setCatID(Integer catID) {
		this.catID = catID;
	}

	public String getCatCodigo() {
		return this.catCodigo;
	}

	public void setCatCodigo(String catCodigo) {
		this.catCodigo = catCodigo;
	}

	public String getCatNome() {
		return this.catNome;
	}

	public void setCatNome(String catNome) {
		this.catNome = catNome;
	}

	public Integer getCatOrdenacao() {
		return this.catOrdenacao;
	}

	public void setCatOrdenacao(Integer catOrdenacao) {
		this.catOrdenacao = catOrdenacao;
	}

	public List<Relatorio> getRelatorios() {
		return relatorios;
	}

	public void setRelatorios(List<Relatorio> relatorios) {
		this.relatorios = relatorios;
	}

}