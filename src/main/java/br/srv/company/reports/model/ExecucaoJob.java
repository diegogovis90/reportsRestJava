package br.srv.company.reports.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ExecucaoJob")
public class ExecucaoJob implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ejoID")
  private Integer ejoID;

  @Column(name = "ejoStatus")
  private Integer ejoStatus;

  @Column(name = "ejoLog")
  private String ejoLog;

  @Column(name = "ejoDataInicio")
  private LocalDateTime ejoDataInicio;

  @Column(name = "ejoDataConclusao")
  private LocalDateTime ejoDataConclusao;

  @Column(name = "ejoCodigoExecucao")
  private String ejoCodigoExecucao;

  @ManyToOne
  @JoinColumn(name = "cjoID")
  private ConfiguracaoJob configuracaoJob;

  public ConfiguracaoJob getConfiguracaoJob() {
    return this.configuracaoJob;
  }

  public void setConfiguracaoJob(ConfiguracaoJob configuracaoJob) {
    this.configuracaoJob = configuracaoJob;
  }

  public Integer getEjoID() {
    return this.ejoID;
  }

  public void setEjoID(Integer ejoID) {
    this.ejoID = ejoID;
  }

  public Integer getEjoStatus() {
    return this.ejoStatus;
  }

  public void setEjoStatus(Integer ejoStatus) {
    this.ejoStatus = ejoStatus;
  }

  public String getEjoLog() {
    return this.ejoLog;
  }

  public void setEjoLog(String ejoLog) {
    this.ejoLog = ejoLog;
  }

  public LocalDateTime getEjoDataInicio() {
    return this.ejoDataInicio;
  }

  public void setEjoDataInicio(LocalDateTime ejoDataInicio) {
    this.ejoDataInicio = ejoDataInicio;
  }

  public LocalDateTime getEjoDataConclusao() {
    return this.ejoDataConclusao;
  }

  public void setEjoDataConclusao(LocalDateTime ejoDataConclusao) {
    this.ejoDataConclusao = ejoDataConclusao;
  }

  public String getEjoCodigoExecucao() {
    return this.ejoCodigoExecucao;
  }

  public void setEjoCodigoExecucao(String ejoCodigoExecucao) {
    this.ejoCodigoExecucao = ejoCodigoExecucao;
  }

}