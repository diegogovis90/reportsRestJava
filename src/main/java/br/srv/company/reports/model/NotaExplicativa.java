package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "NotaExplicativa")
public class NotaExplicativa implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "nteID")
  private Integer nteID;

  @Column(name = "nteSequencia")
  private int nteSequencia;

  @Column(name = "nteTexto")
  private String nteTexto;

  @Column(name = "nteAtiva")
  private boolean nteAtiva;

  @ManyToOne()
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  public Integer getNteID() {
    return this.nteID;
  }

  public void setNteID(Integer nteID) {
    this.nteID = nteID;
  }

  public int getNteSequencia() {
    return this.nteSequencia;
  }

  public void setNteSequencia(int nteSequencia) {
    this.nteSequencia = nteSequencia;
  }

  public String getNteTexto() {
    return this.nteTexto;
  }

  public void setNteTexto(String nteTexto) {
    this.nteTexto = nteTexto;
  }

  public boolean isNteAtiva() {
    return this.nteAtiva;
  }

  public boolean getNteAtiva() {
    return this.nteAtiva;
  }

  public void setNteAtiva(boolean nteAtiva) {
    this.nteAtiva = nteAtiva;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

}