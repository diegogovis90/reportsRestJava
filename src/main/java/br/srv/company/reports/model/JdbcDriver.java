package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "JdbcDriver")
public class JdbcDriver implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "jdrID")
  private Integer jdrID;

  @Column(name = "jdrNome")
  private String jdrNome;

  @Column(name = "jdrClasse")
  private boolean jdrClasse;

  public Integer getJdrID() {
    return this.jdrID;
  }

  public void setJdrID(Integer jdrID) {
    this.jdrID = jdrID;
  }

  public String getJdrNome() {
    return this.jdrNome;
  }

  public void setJdrNome(String jdrNome) {
    this.jdrNome = jdrNome;
  }

  public boolean isJdrClasse() {
    return this.jdrClasse;
  }

  public boolean getJdrClasse() {
    return this.jdrClasse;
  }

  public void setJdrClasse(boolean jdrClasse) {
    this.jdrClasse = jdrClasse;
  }

}