package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RelatoriosModelosImagens")
public class RelatoriosModelosImagens implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "rmiID")
  private Integer rmiID;

  @ManyToOne
  @JoinColumn(name = "relID")
  private RelatorioModelo relatorioModelo;

  @ManyToOne
  @JoinColumn(name = "imaID")
  private Imagem imagem;

  public Integer getRmiID() {
    return this.rmiID;
  }

  public void setRmiID(Integer rmiID) {
    this.rmiID = rmiID;
  }

  public RelatorioModelo getModelo() {
    return this.relatorioModelo;
  }

  public void setModelo(RelatorioModelo relatorioModelo) {
    this.relatorioModelo = relatorioModelo;
  }

  public Imagem getImagem() {
    return this.imagem;
  }

  public void setImagem(Imagem imagem) {
    this.imagem = imagem;
  }

}