package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "RelatorioArquivo")
@PrimaryKeyJoinColumn(name="relID", referencedColumnName="relID")
public class RelatorioArquivo extends Relatorio implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "rarDados")
  private byte[] rarDados;

  @Column(name = "rarFormato")
  private String rarFormato;

  public byte[] getRarDados() {
    return this.rarDados;
  }

  public void setRarDados(byte[] rarDados) {
    this.rarDados = rarDados;
  }

  public String getRarFormato() {
    return this.rarFormato;
  }

  public void setRarFormato(String rarFormato) {
    this.rarFormato = rarFormato;
  }
}