package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Consulta")
@PrimaryKeyJoinColumn(name = "pcoID", referencedColumnName = "pcoID")
public class Consulta extends PropriedadeControle implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "cosSql")
  private String cosSql;

  @Column(name = "cosColunasExibicao")
  private String cosColunasExibicao;

  @Column(name = "cosColunaValor")
  private String cosColunaValor;

  @ManyToOne
  @JoinColumn(name = "fddID")
  private FonteDeDados fonte;
  
  @OneToMany(mappedBy = "propriedadeControle")
  private List<Parametro> parametros;
  
  public Consulta() {}
  
  public Consulta(Integer id, String nome) {
	  this.setPcoID(id);
	  this.setPcoNome(nome);
  }

  public String getCosSql() {
    return this.cosSql;
  }

  public void setCosSql(String cosSql) {
    this.cosSql = cosSql;
  }

  public String getCosColunasExibicao() {
    return this.cosColunasExibicao;
  }

  public void setCosColunasExibicao(String cosColunasExibicao) {
    this.cosColunasExibicao = cosColunasExibicao;
  }

  public String getCosColunaValor() {
    return this.cosColunaValor;
  }

  public void setCosColunaValor(String cosColunaValor) {
    this.cosColunaValor = cosColunaValor;
  }

  public FonteDeDados getFonte() {
    return this.fonte;
  }

  public void setFonte(FonteDeDados fonte) {
    this.fonte = fonte;
  }

  public List<Parametro> getParametros() {
	return parametros;
  }

  public void setParametros(List<Parametro> parametros) {
	this.parametros = parametros;
  }

}