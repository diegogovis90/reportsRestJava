package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Relatorio")
@Inheritance(strategy = InheritanceType.JOINED)
public class Relatorio implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "relID")
  private Integer relID;

  @Column(name = "relNome")
  private String relNome;

  @Column(name = "relTipo")
  private String relTipo;

  @Column(name = "relMensagem")
  private String relMensagem;

  @Column(name = "relCodigo")
  private String relCodigo;

  @Column(name = "relOrdenacao")
  private int relOrdenacao;

  @ManyToOne()
  @JoinColumn(name = "catID")
  private Categoria categoria;

  @ManyToOne(cascade = { CascadeType.ALL })
  @JoinColumn(name = "relReferencia")
  private Relatorio relReferencia;

  @OneToMany(mappedBy = "relatorio")
  private List<RelatorioPermissao> permissoes;

  @OneToMany(mappedBy = "relatorio", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<AnosRelatorios> anos;

  @OneToMany(mappedBy = "relatorio")
  private List<LocaisRelatorios> locais;

  @OneToMany(mappedBy = "relatorio", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<NotaExplicativa> notasExplicativas;
  
  @Transient
  private Integer ano;
  
  @Transient
  private boolean marcado;
  
  @Transient
  private boolean temLocal;  
  
  @Transient
  private int acao;  
  
  public Relatorio() {}
  
  public Relatorio(Integer relID, String relNome, String relTipo, String relMensagem, String relCodigo,
		  int relOrdenacao, Categoria categoria, Integer ano) {
	  this.relID = relID;
	  this.relNome = relNome;
	  this.relTipo = relTipo;
	  this.relMensagem = relMensagem;
	  this.relCodigo = relCodigo;
	  this.relOrdenacao = relOrdenacao;
	  this.categoria = categoria;
	  this.ano = ano;
  }
  
  public Relatorio(Integer relID, String relNome, String relTipo, String relMensagem, String relCodigo,
		  int relOrdenacao, Categoria categoria, Integer ano, int marcado, int temLocal) {
	  this.relID = relID;
	  this.relNome = relNome;
	  this.relTipo = relTipo;
	  this.relMensagem = relMensagem;
	  this.relCodigo = relCodigo;
	  this.relOrdenacao = relOrdenacao;
	  this.categoria = categoria;
	  this.ano = ano;
	  this.marcado = marcado == 1;
	  this.temLocal = temLocal == 1;
  }
  
  public Relatorio(Integer relID, String relCodigo, String relNome, int acao) {
	  this.relID = relID;
	  this.relNome = relNome;
	  this.relCodigo = relCodigo;
	  this.acao = acao;
  }

  public Categoria getCategoria() {
    return this.categoria;
  }

  public void setCategoria(Categoria categoria) {
    this.categoria = categoria;
  }

  public List<LocaisRelatorios> getLocais() {
    return this.locais;
  }

  public void setLocais(List<LocaisRelatorios> locais) {
    this.locais = locais;
  }

  public List<AnosRelatorios> getAnos() {
	return anos;
  }

  public void setAnos(List<AnosRelatorios> anos) {
	this.anos = anos;
  }

  public List<RelatorioPermissao> getPermissoes() {
    return this.permissoes;
  }

  public void setPermissoes(List<RelatorioPermissao> permissoes) {
    this.permissoes = permissoes;
  }

  public List<NotaExplicativa> getNotasExplicativas() {
    return this.notasExplicativas;
  }

  public void setNotasExplicativas(List<NotaExplicativa> notasExplicativas) {
    this.notasExplicativas = notasExplicativas;
  }

  public Integer getRelID() {
    return this.relID;
  }

  public void setRelID(Integer relID) {
    this.relID = relID;
  }

  public String getRelNome() {
    return this.relNome;
  }

  public void setRelNome(String relNome) {
    this.relNome = relNome;
  }

  public String getRelTipo() {
    return this.relTipo;
  }

  public void setRelTipo(String relTipo) {
    this.relTipo = relTipo;
  }

  public String getRelMensagem() {
    return this.relMensagem;
  }

  public void setRelMensagem(String relMensagem) {
    this.relMensagem = relMensagem;
  }

  public String getRelCodigo() {
    return this.relCodigo;
  }

  public void setRelCodigo(String relCodigo) {
    this.relCodigo = relCodigo;
  }

  public int getRelOrdenacao() {
    return this.relOrdenacao;
  }

  public void setRelOrdenacao(int relOrdenacao) {
    this.relOrdenacao = relOrdenacao;
  }

  public Relatorio getRelReferencia() {
    return this.relReferencia;
  }

  public void setRelReferencia(Relatorio relReferencia) {
    this.relReferencia = relReferencia;
  }

  public Integer getAno() {
	return ano;
  }

  public void setAno(Integer ano) {
	this.ano = ano;
  }

  public boolean isMarcado() {
	return marcado;
  }

  public void setMarcado(boolean marcado) {
	this.marcado = marcado;
  }

  public boolean isTemLocal() {
	return temLocal;
  }

  public void setTemLocal(boolean temLocal) {
	this.temLocal = temLocal;
  }

  public int getAcao() {
	return acao;
  }

  public void setAcao(int acao) {
	this.acao = acao;
  }
}