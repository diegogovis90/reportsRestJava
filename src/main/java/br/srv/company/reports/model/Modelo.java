package br.srv.company.reports.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Modelo")
public class Modelo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "modID")
	private Integer modID;

	@Column(name = "modNome")
	private String modNome;

	@Column(name = "modNomeArquivo")
	private String modNomeArquivo;

	@Column(name = "modNomeArquivoTabulado")
	private String modNomeArquivoTabulado;

	@Column(name = "modDados")
	private byte[] modDados;

	@Column(name = "modDataEnvioArquivo")
	private LocalDateTime modDataEnvioArquivo;

	@Column(name = "modDataModificacaoArquivo")
	private LocalDateTime modDataModificacaoArquivo;

	@Column(name = "modCPFModificacaoArquivo")
	private String modCPFModificacaoArquivo;

	@Column(name = "modDados2")
	private byte[] modDados2;

	@Column(name = "modDataEnvioArquivoTabulado")
	private LocalDateTime modDataEnvioArquivoTabulado;

	@Column(name = "modDataModificacaoArquivoTabulado")
	private LocalDateTime modDataModificacaoArquivoTabulado;

	@Column(name = "modCPFModificacaoArquivoTabulado")
	private String modCPFModificacaoArquivoTabulado;

	@Column(name = "modCPFCriacao")
	private String modCPFCriacao;

	@Column(name = "modCPFModificacao")
	private String modCPFModificacao;

	@Column(name = "modData")
	private LocalDateTime modData;

	@Column(name = "modDataModificacao")
	private LocalDateTime modDataModificacao;

	@OneToMany(mappedBy = "modelo")
	private List<RelatorioModelo> relatorios;

	@Transient
	private long utilizacao;

	public Modelo() {
	}

	public Modelo(Integer id, String nome, long utilizacao) {
		this.modID = id;
		this.modNome = nome;
		this.utilizacao = utilizacao;
	}

	public Integer getModID() {
		return this.modID;
	}

	public void setModID(Integer modID) {
		this.modID = modID;
	}

	public String getModNome() {
		return this.modNome;
	}

	public void setModNome(String modNome) {
		this.modNome = modNome;
	}

	public String getModNomeArquivo() {
		return this.modNomeArquivo;
	}

	public void setModNomeArquivo(String modNomeArquivo) {
		this.modNomeArquivo = modNomeArquivo;
	}

	public String getModNomeArquivoTabulado() {
		return modNomeArquivoTabulado;
	}

	public void setModNomeArquivoTabulado(String modNomeArquivoTabulado) {
		this.modNomeArquivoTabulado = modNomeArquivoTabulado;
	}

	public byte[] getModDados() {
		return this.modDados;
	}

	public void setModDados(byte[] modDados) {
		this.modDados = modDados;
	}

	public LocalDateTime getModDataEnvioArquivo() {
		return this.modDataEnvioArquivo;
	}

	public void setModDataEnvioArquivo(LocalDateTime modDataEnvioArquivo) {
		this.modDataEnvioArquivo = modDataEnvioArquivo;
	}

	public LocalDateTime getModDataModificacaoArquivo() {
		return this.modDataModificacaoArquivo;
	}

	public void setModDataModificacaoArquivo(LocalDateTime modDataModificacaoArquivo) {
		this.modDataModificacaoArquivo = modDataModificacaoArquivo;
	}

	public String getModCPFModificacaoArquivo() {
		return this.modCPFModificacaoArquivo;
	}

	public void setModCPFModificacaoArquivo(String modCPFModificacaoArquivo) {
		this.modCPFModificacaoArquivo = modCPFModificacaoArquivo;
	}

	public byte[] getModDados2() {
		return this.modDados2;
	}

	public void setModDados2(byte[] modDados2) {
		this.modDados2 = modDados2;
	}

	public LocalDateTime getModDataEnvioArquivoTabulado() {
		return this.modDataEnvioArquivoTabulado;
	}

	public void setModDataEnvioArquivoTabulado(LocalDateTime modDataEnvioArquivoTabulado) {
		this.modDataEnvioArquivoTabulado = modDataEnvioArquivoTabulado;
	}

	public LocalDateTime getModDataModificacaoArquivoTabulado() {
		return this.modDataModificacaoArquivoTabulado;
	}

	public void setModDataModificacaoArquivoTabulado(LocalDateTime modDataModificacaoArquivoTabulado) {
		this.modDataModificacaoArquivoTabulado = modDataModificacaoArquivoTabulado;
	}

	public String getModCPFModificacaoArquivoTabulado() {
		return this.modCPFModificacaoArquivoTabulado;
	}

	public void setModCPFModificacaoArquivoTabulado(String modCPFModificacaoArquivoTabulado) {
		this.modCPFModificacaoArquivoTabulado = modCPFModificacaoArquivoTabulado;
	}

	public String getModCPFCriacao() {
		return this.modCPFCriacao;
	}

	public void setModCPFCriacao(String modCPFCriacao) {
		this.modCPFCriacao = modCPFCriacao;
	}

	public String getModCPFModificacao() {
		return this.modCPFModificacao;
	}

	public void setModCPFModificacao(String modCPFModificacao) {
		this.modCPFModificacao = modCPFModificacao;
	}

	public LocalDateTime getModData() {
		return this.modData;
	}

	public void setModData(LocalDateTime modData) {
		this.modData = modData;
	}

	public LocalDateTime getModDataModificacao() {
		return this.modDataModificacao;
	}

	public void setModDataModificacao(LocalDateTime modDataModificacao) {
		this.modDataModificacao = modDataModificacao;
	}

	public List<RelatorioModelo> getRelatorios() {
		return relatorios;
	}

	public void setRelatorios(List<RelatorioModelo> relatorios) {
		this.relatorios = relatorios;
	}

	public long getUtilizacao() {
		return utilizacao;
	}

	public void setUtilizacao(long utilizacao) {
		this.utilizacao = utilizacao;
	}
}