package br.srv.company.reports.model;

import java.util.ArrayList;
import java.util.List;

public class ErroRespostaValidacao extends ErroResposta {
	private List<ErroValidacao> erros = new ArrayList<>();
	
	public ErroRespostaValidacao(List<ErroValidacao> erros) {
		//super(99, "Encontrados erros na validação das informações enviadas.");
		super(99, "Foram encontrados um ou mais erros nos dados enviados.");
		this.erros = erros;
	}

	public List<ErroValidacao> getErros() {
		return erros;
	}

	public void setErros(List<ErroValidacao> erros) {
		this.erros = erros;
	}

}
