package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RelatorioPermissao")
public class RelatorioPermissao implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "repID")
  private Integer repID;

  @Column(name = "repUsuario")
  private String repusuario;

  @ManyToOne
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  public Integer getRepID() {
    return this.repID;
  }

  public void setRepID(Integer repID) {
    this.repID = repID;
  }

  public String getRepusuario() {
    return this.repusuario;
  }

  public void setRepusuario(String repusuario) {
    this.repusuario = repusuario;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

}