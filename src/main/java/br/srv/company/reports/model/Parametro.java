package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Parametro")
public class Parametro implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "parID")
  private Integer parID;

  @Column(name = "parNome")
  private String parNome;

  @Column(name = "parTipoControle")
  private int parTipoControle;

  @Column(name = "parTipoPropriedade")
  private int parTipoPropriedade;

  @Column(name = "parTipoClasse")
  private String parTipoClasse;

  @Column(name = "parOrdem")
  private short parOrdem;

  @Column(name = "parValor")
  private String parValor;

  @ManyToOne
  @JoinColumn(name = "pcoID")
  private PropriedadeControle propriedadeControle;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "conID")
  private Controle controle;

  @ManyToOne
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  public Integer getParID() {
    return this.parID;
  }

  public void setParID(Integer parID) {
    this.parID = parID;
  }

  public String getParNome() {
    return this.parNome;
  }

  public void setParNome(String parNome) {
    this.parNome = parNome;
  }

  public int getParTipoControle() {
    return this.parTipoControle;
  }

  public void setParTipoControle(int parTipoControle) {
    this.parTipoControle = parTipoControle;
  }

  public int getParTipoPropriedade() {
    return this.parTipoPropriedade;
  }

  public void setParTipoPropriedade(int parTipoPropriedade) {
    this.parTipoPropriedade = parTipoPropriedade;
  }

  public String getParTipoClasse() {
    return this.parTipoClasse;
  }

  public void setParTipoClasse(String parTipoClasse) {
    this.parTipoClasse = parTipoClasse;
  }

  public short getParOrdem() {
    return this.parOrdem;
  }

  public void setParOrdem(short parOrdem) {
    this.parOrdem = parOrdem;
  }

  public String getParValor() {
    return this.parValor;
  }

  public void setParValor(String parValor) {
    this.parValor = parValor;
  }

  public PropriedadeControle getPropriedadeControle() {
    return this.propriedadeControle;
  }

  public void setPropriedadeControle(PropriedadeControle propriedadeControle) {
    this.propriedadeControle = propriedadeControle;
  }

  public Controle getControle() {
    return this.controle;
  }

  public void setControle(Controle controle) {
    this.controle = controle;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

}