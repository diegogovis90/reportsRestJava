package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "PropriedadeControle")
@Inheritance(strategy = InheritanceType.JOINED)
public class PropriedadeControle implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "pcoID")
  private Integer pcoID;

  @Column(name = "pcoNome")
  private String pcoNome;

  @Column(name = "pcoTipo")
  private String pcoTipo;

  public Integer getPcoID() {
    return this.pcoID;
  }

  public void setPcoID(Integer pcoID) {
    this.pcoID = pcoID;
  }

  public String getPcoNome() {
    return this.pcoNome;
  }

  public void setPcoNome(String pcoNome) {
    this.pcoNome = pcoNome;
  }

  public String getPcoTipo() {
    return this.pcoTipo;
  }

  public void setPcoTipo(String pcoTipo) {
    this.pcoTipo = pcoTipo;
  }
}