package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AnosRelatorios")
public class AnosRelatorios implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "areID")
  private Integer areID;

  @ManyToOne
  @JoinColumn(name = "areAno")
  private AnoRelatorio ano;

  @ManyToOne
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  public Integer getAreID() {
    return this.areID;
  }

  public void setAreID(Integer areID) {
    this.areID = areID;
  }

  public AnoRelatorio getAno() {
    return this.ano;
  }

  public void setAno(AnoRelatorio ano) {
    this.ano = ano;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

}