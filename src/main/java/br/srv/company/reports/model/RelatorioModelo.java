package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "RelatorioModelo")
@PrimaryKeyJoinColumn(name = "relID", referencedColumnName = "relID")
public class RelatorioModelo extends Relatorio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "rmoPersonalizavel")
	private boolean rmoPersonalizavel;

	@ManyToOne
	@JoinColumn(name = "modID")
	private Modelo modelo;

	@ManyToOne
	@JoinColumn(name = "fddID")
	private FonteDeDados fonte;

	@OneToMany(mappedBy = "relatorio", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Parametro> parametros;

	@OneToMany(mappedBy = "relatorioModelo")
	private List<RelatoriosModelosImagens> imagens;

	public List<RelatoriosModelosImagens> getImagens() {
		return this.imagens;
	}

	public void setImagens(List<RelatoriosModelosImagens> imagens) {
		this.imagens = imagens;
	}

	public boolean isRmoPersonalizavel() {
		return this.rmoPersonalizavel;
	}

	public boolean getRmoPersonalizavel() {
		return this.rmoPersonalizavel;
	}

	public void setRmoPersonalizavel(boolean rmoPersonalizavel) {
		this.rmoPersonalizavel = rmoPersonalizavel;
	}

	public Modelo getModelo() {
		return this.modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public FonteDeDados getFonte() {
		return this.fonte;
	}

	public void setFonte(FonteDeDados fonte) {
		this.fonte = fonte;
	}

	public List<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}

}