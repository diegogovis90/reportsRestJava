package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Controle")
public class Controle implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "conID")
  private Integer conID;

  @Column(name = "conNome")
  private String conNome;

  @Column(name = "conRotulo")
  private String conRotulo;

  @Column(name = "conObrigatorio")
  private Boolean conObrigatorio;

  @Column(name = "conSomenteLeitura")
  private Boolean conSomenteLeitura;

  @Column(name = "conVisivel")
  private Boolean conVisivel;

  @Column(name = "conAtualizarOutrosParametros")
  private Boolean conAtualizarOutrosParametros;

  public Integer getConID() {
    return this.conID;
  }

  public void setConID(Integer conID) {
    this.conID = conID;
  }

  public String getConNome() {
    return this.conNome;
  }

  public void setConNome(String conNome) {
    this.conNome = conNome;
  }

  public String getConRotulo() {
    return this.conRotulo;
  }

  public void setConRotulo(String conRotulo) {
    this.conRotulo = conRotulo;
  }

  public Boolean isConObrigatorio() {
    return this.conObrigatorio;
  }

  public Boolean getConObrigatorio() {
    return this.conObrigatorio;
  }

  public void setConObrigatorio(Boolean conObrigatorio) {
    this.conObrigatorio = conObrigatorio;
  }

  public Boolean isConSomenteLeitura() {
    return this.conSomenteLeitura;
  }

  public Boolean getConSomenteLeitura() {
    return this.conSomenteLeitura;
  }

  public void setConSomenteLeitura(Boolean conSomenteLeitura) {
    this.conSomenteLeitura = conSomenteLeitura;
  }

  public Boolean isConVisivel() {
    return this.conVisivel;
  }

  public Boolean getConVisivel() {
    return this.conVisivel;
  }

  public void setConVisivel(Boolean conVisivel) {
    this.conVisivel = conVisivel;
  }

  public Boolean isConAtualizarOutrosParametros() {
    return this.conAtualizarOutrosParametros;
  }

  public Boolean getConAtualizarOutrosParametros() {
    return this.conAtualizarOutrosParametros;
  }

  public void setConAtualizarOutrosParametros(Boolean conAtualizarOutrosParametros) {
    this.conAtualizarOutrosParametros = conAtualizarOutrosParametros;
  }

}