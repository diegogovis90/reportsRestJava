package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ConfiguracaoJob")
public class ConfiguracaoJob implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "parID")
  private Integer parID;

  @Column(name = "cjoNome")
  private String cjoNome;

  @Column(name = "cjoDescricao")
  private String cjoDescricao;

  @Column(name = "cjoCaminhoBat")
  private String cjoCaminhoBat;

  @Column(name = "cjoServicoAtualizacacoJob")
  private String cjoServicoAtualizacacoJob;

  public Integer getParID() {
    return this.parID;
  }

  public void setParID(Integer parID) {
    this.parID = parID;
  }

  public String getCjoNome() {
    return this.cjoNome;
  }

  public void setCjoNome(String cjoNome) {
    this.cjoNome = cjoNome;
  }

  public String getCjoDescricao() {
    return this.cjoDescricao;
  }

  public void setCjoDescricao(String cjoDescricao) {
    this.cjoDescricao = cjoDescricao;
  }

  public String getCjoCaminhoBat() {
    return this.cjoCaminhoBat;
  }

  public void setCjoCaminhoBat(String cjoCaminhoBat) {
    this.cjoCaminhoBat = cjoCaminhoBat;
  }

  public String getCjoServicoAtualizacacoJob() {
    return this.cjoServicoAtualizacacoJob;
  }

  public void setCjoServicoAtualizacacoJob(String cjoServicoAtualizacacoJob) {
    this.cjoServicoAtualizacacoJob = cjoServicoAtualizacacoJob;
  }

}