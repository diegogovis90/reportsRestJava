package br.srv.company.reports.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Imagem")
public class Imagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "imaID")
	private Integer imaID;

	@Column(name = "imaNome")
	private String imaNome;

	@Column(name = "imaFormato")
	private String imaFormato;

	@Column(name = "imaDados")
	private byte[] imaDados;

	@Column(name = "imaData")
	private LocalDateTime imaData;

	@OneToMany(mappedBy = "imagem")
	private List<RelatoriosModelosImagens> relatoriosModelosImagens;

	@Transient
	private long utilizacao;

	public Imagem() {
	}

	public Imagem(Integer imaID, String imaNome, String imaFormato, LocalDateTime imaData, long utilizacao) {
		this.imaID = imaID;
		this.imaNome = imaNome;
		this.imaFormato = imaFormato;
		this.imaData = imaData;
		this.utilizacao = utilizacao;
	}

	public Integer getImaID() {
		return this.imaID;
	}

	public void setImaID(Integer imaID) {
		this.imaID = imaID;
	}

	public String getImaNome() {
		return this.imaNome;
	}

	public void setImaNome(String imaNome) {
		this.imaNome = imaNome;
	}

	public String getImaFormato() {
		return this.imaFormato;
	}

	public void setImaFormato(String imaFormato) {
		this.imaFormato = imaFormato;
	}

	public byte[] getImaDados() {
		return this.imaDados;
	}

	public void setImaDados(byte[] imaDados) {
		this.imaDados = imaDados;
	}

	public LocalDateTime getImaData() {
		return this.imaData;
	}

	public void setImaData(LocalDateTime imaData) {
		this.imaData = imaData;
	}

	public List<RelatoriosModelosImagens> getRelatoriosModelosImagens() {
		return relatoriosModelosImagens;
	}

	public void setRelatoriosModelosImagens(List<RelatoriosModelosImagens> relatoriosModelosImagens) {
		this.relatoriosModelosImagens = relatoriosModelosImagens;
	}
	
	public long getUtilizacao() {
		return utilizacao;
	}
	
	public void setUtilizacao(long utilizacao) {
		this.utilizacao = utilizacao;
	}

}