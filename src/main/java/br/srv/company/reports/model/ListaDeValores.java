package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ListaDeValores")
@PrimaryKeyJoinColumn(name = "pcoID", referencedColumnName = "pcoID")
public class ListaDeValores extends PropriedadeControle implements Serializable {
	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy = "listaDeValores", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<ItemDeLista> itens;
	
	@OneToMany(mappedBy = "propriedadeControle")
	private List<Parametro> parametros;
	
	@Transient
	private long totalItens;
	
	public ListaDeValores() {}
	
	public ListaDeValores(Integer id, String nome, long totalItens) {
		this.setPcoID(id);
		this.setPcoNome(nome);
		this.setTotalItens(totalItens);
	}

	public List<ItemDeLista> getItens() {
		return itens;
	}

	public void setItens(List<ItemDeLista> itens) {
		this.itens = itens;
	}
	
	public void adicionarItem(ItemDeLista item) {
		this.itens.add(item);
	}

	public List<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}

	public long getTotalItens() {
		return totalItens;
	}

	public void setTotalItens(long totalItens) {
		this.totalItens = totalItens;
	}
}