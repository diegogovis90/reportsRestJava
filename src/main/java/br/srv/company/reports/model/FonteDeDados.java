package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FonteDeDados")
public class FonteDeDados implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "fddID", nullable=false)
  private Integer fddID;

  @Column(name = "fddTipo", nullable=false)
  private String fddTipo;

  @Column(name = "fddNome", nullable=false)
  private String fddNome;

  @Column(name = "fddServidor", nullable=false)
  private String fddServidor;

  @Column(name = "fddNomeBD", nullable=false)
  private String fddNomeBD;

  @Column(name = "fddUrl", nullable=false)
  private String fddUrl;

  @Column(name = "fddUsuario", nullable=false)
  private String fddUsuario;

  @Column(name = "fddSenha", nullable=false)
  private String fddSenha;

  @Column(name = "fddOpcoes")
  private String fddOpcoes;

  @ManyToOne
  @JoinColumn(name = "jdrID", nullable=false)
  private JdbcDriver driver;
  
  @OneToMany(mappedBy = "fonte")
  private List<RelatorioModelo> relatorios;
  
  @OneToMany(mappedBy = "fonte")
  private List<Consulta> consultas;
  
  @OneToMany(mappedBy = "fonte")
  private List<RelatorioUsuario> relatoriosUsu;
  
  @Transient
  private long utilizacao;
  
  public FonteDeDados() {}
    
  public FonteDeDados(Integer fddID, String fddNome, long utilizacao) {
	this.fddID = fddID;
	this.fddNome = fddNome;
	this.utilizacao = utilizacao;
  }

  public Integer getFddID() {
    return this.fddID;
  }

  public void setFddID(Integer fddID) {
    this.fddID = fddID;
  }

  public String getFddTipo() {
    return this.fddTipo;
  }

  public void setFddTipo(String fddTipo) {
    this.fddTipo = fddTipo;
  }

  public String getFddNome() {
    return this.fddNome;
  }

  public void setFddNome(String fddNome) {
    this.fddNome = fddNome;
  }

  public String getFddServidor() {
    return this.fddServidor;
  }

  public void setFddServidor(String fddServidor) {
    this.fddServidor = fddServidor;
  }

  public String getFddNomeBD() {
    return this.fddNomeBD;
  }

  public void setFddNomeBD(String fddNomeBD) {
    this.fddNomeBD = fddNomeBD;
  }

  public String getFddUrl() {
    return this.fddUrl;
  }

  public void setFddUrl(String fddUrl) {
    this.fddUrl = fddUrl;
  }

  public String getFddUsuario() {
    return this.fddUsuario;
  }

  public void setFddUsuario(String fddUsuario) {
    this.fddUsuario = fddUsuario;
  }

  public String getFddSenha() {
    return this.fddSenha;
  }

  public void setFddSenha(String fddSenha) {
    this.fddSenha = fddSenha;
  }

  public String getFddOpcoes() {
    return this.fddOpcoes;
  }

  public void setFddOpcoes(String fddOpcoes) {
    this.fddOpcoes = fddOpcoes;
  }

  public JdbcDriver getDriver() {
    return this.driver;
  }

  public void setDriver(JdbcDriver driver) {
    this.driver = driver;
  }

  public List<RelatorioModelo> getRelatorios() {
	return relatorios;
  }

  public void setRelatorios(List<RelatorioModelo> relatorios) {
	this.relatorios = relatorios;
  }

  public List<Consulta> getConsultas() {
	return consultas;
  }

  public void setConsultas(List<Consulta> consultas) {
	this.consultas = consultas;
  }

  public List<RelatorioUsuario> getRelatoriosUsu() {
	return relatoriosUsu;
  }

  public void setRelatoriosUsu(List<RelatorioUsuario> relatoriosUsu) {
	this.relatoriosUsu = relatoriosUsu;
  }

  public long getUtilizacao() {
	return utilizacao;
  }

  public void setUtilizacao(long utilizacao) {
	this.utilizacao = utilizacao;
  }
}