package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ItemDeLista")
public class ItemDeLista implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idlID")
  private Integer idlID;

  @Column(name = "idlNome")
  private String idlNome;

  @Column(name = "idlValor")
  private String idlValor;

  @Column(name = "idlPosicao")
  private Integer idlPosicao;

  @ManyToOne
  @JoinColumn(name = "pcoID")
  private ListaDeValores listaDeValores;

  public Integer getIdlID() {
    return this.idlID;
  }

  public void setIdlID(Integer idlID) {
    this.idlID = idlID;
  }

  public String getIdlNome() {
    return this.idlNome;
  }

  public void setIdlNome(String idlNome) {
    this.idlNome = idlNome;
  }

  public String getIdlValor() {
    return this.idlValor;
  }

  public void setIdlValor(String idlValor) {
    this.idlValor = idlValor;
  }

  public Integer getIdlPosicao() {
    return this.idlPosicao;
  }

  public void setIdlPosicao(Integer idlPosicao) {
    this.idlPosicao = idlPosicao;
  }

  public ListaDeValores getListaDeValores() {
    return this.listaDeValores;
  }

  public void setListaDeValores(ListaDeValores listaDeValores) {
    this.listaDeValores = listaDeValores;
  }

}