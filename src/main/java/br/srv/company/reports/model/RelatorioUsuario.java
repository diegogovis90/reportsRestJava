package br.srv.company.reports.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Relatoriousuario")
public class RelatorioUsuario implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @ManyToOne
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  @Column(name = "rusExercicio")
  private Integer rusExercicio;

  @Column(name = "rusUsuario")
  private String rusUsuario;

  @Column(name = "rusSql")
  private String rusSql;

  @ManyToOne
  @JoinColumn(name = "fddID")
  private FonteDeDados fonte;

  @OneToMany(mappedBy = "relatorioUsuario")
  private List<ColunaRelatorio> colunas;

  public List<ColunaRelatorio> getColunas() {
    return this.colunas;
  }

  public void setColunas(List<ColunaRelatorio> colunas) {
    this.colunas = colunas;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

  public Integer getRusExercicio() {
    return this.rusExercicio;
  }

  public void setRusExercicio(Integer rusExercicio) {
    this.rusExercicio = rusExercicio;
  }

  public String getRusUsuario() {
    return this.rusUsuario;
  }

  public void setRusUsuario(String rusUsuario) {
    this.rusUsuario = rusUsuario;
  }

  public String getRusSql() {
    return this.rusSql;
  }

  public void setRusSql(String rusSql) {
    this.rusSql = rusSql;
  }

  public FonteDeDados getFonte() {
    return this.fonte;
  }

  public void setFonte(FonteDeDados fonte) {
    this.fonte = fonte;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (!(o instanceof RelatorioUsuario)) {
      return false;
    }
    RelatorioUsuario relatorioUsuario = (RelatorioUsuario) o;
    return Objects.equals(relatorio, relatorioUsuario.relatorio) && rusExercicio == relatorioUsuario.rusExercicio
        && Objects.equals(rusUsuario, relatorioUsuario.rusUsuario) && Objects.equals(rusSql, relatorioUsuario.rusSql)
        && Objects.equals(fonte, relatorioUsuario.fonte) && Objects.equals(colunas, relatorioUsuario.colunas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(relatorio, rusExercicio, rusUsuario, rusSql, fonte, colunas);
  }

}