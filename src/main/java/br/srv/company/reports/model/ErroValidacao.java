package br.srv.company.reports.model;

public class ErroValidacao {
	private String parametro;
	private String descricao;
	private String campoLista;
	private Integer indiceLista;
	
	public ErroValidacao(String parametro, String descricao) {
		this.parametro = parametro;
		this.descricao = descricao;
	}
	
	public ErroValidacao(String parametro, String descricao, Integer indiceLista, String campoLista) {
		this.parametro = parametro;
		this.descricao = descricao;
		this.campoLista = campoLista;
		this.indiceLista = indiceLista;
	}
	
	public String getParametro() {
		return parametro;
	}
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIndiceLista() {
		return indiceLista;
	}

	public void setIndiceLista(Integer indiceLista) {
		this.indiceLista = indiceLista;
	}

	public String getCampoLista() {
		return campoLista;
	}

	public void setCampoLista(String campoLista) {
		this.campoLista = campoLista;
	}
	
}
