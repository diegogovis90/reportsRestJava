package br.srv.company.reports.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LocaisRelatorios")
public class LocaisRelatorios implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "lreID")
  private Integer lreID;

  @ManyToOne
  @JoinColumn(name = "relID")
  private Relatorio relatorio;

  @ManyToOne
  @JoinColumn(name = "locID")
  private Local local;

  public Integer getLreID() {
    return this.lreID;
  }

  public void setLreID(Integer lreID) {
    this.lreID = lreID;
  }

  public Relatorio getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(Relatorio relatorio) {
    this.relatorio = relatorio;
  }

  public Local getLocal() {
    return this.local;
  }

  public void setLocal(Local local) {
    this.local = local;
  }

}