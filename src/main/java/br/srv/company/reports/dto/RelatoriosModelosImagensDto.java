package br.srv.company.reports.dto;

public class RelatoriosModelosImagensDto {

  private Integer id;
  private RelatorioModeloDto relatorioModelo;
  private ImagemDto imagem;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public RelatorioModeloDto getModelo() {
    return this.relatorioModelo;
  }

  public void setModelo(RelatorioModeloDto relatorioModelo) {
    this.relatorioModelo = relatorioModelo;
  }

  public ImagemDto getImagem() {
    return this.imagem;
  }

  public void setImagem(ImagemDto imagem) {
    this.imagem = imagem;
  }

}