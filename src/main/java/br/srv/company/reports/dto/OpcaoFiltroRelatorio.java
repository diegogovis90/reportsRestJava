package br.srv.company.reports.dto;

public class OpcaoFiltroRelatorio {
  private String nome;
  private String valor;

  public OpcaoFiltroRelatorio() {
    super();
  }

  public OpcaoFiltroRelatorio(String nome, String valor) {
    this.nome = nome;
    this.valor = valor;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getValor() {
    return this.valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

}