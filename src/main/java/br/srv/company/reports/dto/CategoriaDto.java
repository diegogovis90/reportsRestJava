package br.srv.company.reports.dto;

import java.util.List;

public class CategoriaDto {

	private Integer id;
	private String codigo;
	private String nome;
	private Integer ordenacao;
	List<RelatorioAcaoDto> relatorios;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getOrdenacao() {
		return this.ordenacao;
	}

	public void setOrdenacao(Integer ordenacaco) {
		this.ordenacao = ordenacaco;
	}

	public List<RelatorioAcaoDto> getRelatorios() {
		return relatorios;
	}

	public void setRelatorios(List<RelatorioAcaoDto> relatorios) {
		this.relatorios = relatorios;
	}
}