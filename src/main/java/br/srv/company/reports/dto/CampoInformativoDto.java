package br.srv.company.reports.dto;

public class CampoInformativoDto {

  private Integer id;
  private Integer sequencia;
  private String texto;
  private boolean ativo;
  private RelatorioDto relatorio;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getSequencia() {
    return this.sequencia;
  }

  public void setSequencia(Integer sequencia) {
    this.sequencia = sequencia;
  }

  public String getTexto() {
    return this.texto;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }
  
  public boolean getAtivo() {
    return this.ativo;
  }

  public void setAtivo(boolean ativo) {
    this.ativo = ativo;
  }

  public RelatorioDto getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(RelatorioDto relatorio) {
    this.relatorio = relatorio;
  }

}