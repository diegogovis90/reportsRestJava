package br.srv.company.reports.dto;

import java.io.Serializable;
import java.util.List;

public class ListaDeValoresDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nome;
	private String tipo = "ListaDeValores";
	private List<ItemDeListaDto> itens;
	private long totalItens;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<ItemDeListaDto> getItens() {
		return itens;
	}

	public void setItens(List<ItemDeListaDto> itens) {
		this.itens = itens;
	}

	public long getTotalItens() {
		return totalItens;
	}

	public void setTotalItens(long totalItens) {
		this.totalItens = totalItens;
	}	
}