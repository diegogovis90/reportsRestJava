package br.srv.company.reports.dto;

import java.io.Serializable;

public class RelatorioModeloBasicoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long id;
	private int modeloId;
	private int fonteDeDadosId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getModeloId() {
		return modeloId;
	}

	public void setModeloId(int modeloId) {
		this.modeloId = modeloId;
	}

	public int getFonteDeDadosId() {
		return fonteDeDadosId;
	}

	public void setFonteDeDadosId(int fonteDeDadosId) {
		this.fonteDeDadosId = fonteDeDadosId;
	}

}