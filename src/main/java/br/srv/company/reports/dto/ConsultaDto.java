package br.srv.company.reports.dto;

import java.io.Serializable;

public class ConsultaDto implements Serializable {
  private static final long serialVersionUID = 1L;

  private Integer id;
  private String nome;
  private String tipo = "Consulta";
  private String sql;
  private String[] colunasExibicao;
  private String colunaValor;
  private Integer idFonteDeDados;

  public Integer getId() {
	return id;
  }

  public void setId(Integer id) {
	this.id = id;
  }

  public String getNome() {
	return nome;
  }

  public void setNome(String nome) {
	this.nome = nome;
  }

  public String getTipo() {
	return tipo;
  }

  public void setTipo(String tipo) {
	this.tipo = tipo;
  }

  public String getSql() {
    return this.sql;
  }

  public void setSql(String sql) {
    this.sql = sql;
  }

  public String[] getColunasExibicao() {
    return this.colunasExibicao;
  }

  public void setColunasExibicao(String[] colunasExibicao) {
    this.colunasExibicao = colunasExibicao;
  }

  public String getColunaValor() {
    return this.colunaValor;
  }

  public void setColunaValor(String colunaValor) {
    this.colunaValor = colunaValor;
  }

  public Integer getIdFonteDeDados() {
	return idFonteDeDados;
  }

  public void setIdFonteDeDados(Integer idFonteDeDados) {
	this.idFonteDeDados = idFonteDeDados;
  }
}