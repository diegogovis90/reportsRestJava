package br.srv.company.reports.dto;

public class ItemDeListaDto {

  private Integer id;
  private String nome;
  private String valor;
  private Integer posicao;
  private ListaDeValoresDto listaDeValores;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getValor() {
    return this.valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

  public Integer getPosicao() {
    return this.posicao;
  }

  public void setPosicao(Integer posicao) {
    this.posicao = posicao;
  }

  public ListaDeValoresDto getPropriedadeControle() {
    return this.listaDeValores;
  }

  public void setListaDeValores(ListaDeValoresDto listaDeValores) {
    this.listaDeValores = listaDeValores;
  }

}