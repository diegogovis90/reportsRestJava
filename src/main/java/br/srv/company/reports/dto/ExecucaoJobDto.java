package br.srv.company.reports.dto;

import java.time.LocalDateTime;

public class ExecucaoJobDto {

  private Integer id;
  private Integer status;
  private String log;
  private LocalDateTime dataInicio;
  private LocalDateTime dataConclusao;
  private String codigoExecucao;
  private ConfiguracaoJobDto configuracaoJob;

  public ConfiguracaoJobDto getConfiguracaoJob() {
    return this.configuracaoJob;
  }

  public void setConfiguracaoJob(ConfiguracaoJobDto configuracaoJob) {
    this.configuracaoJob = configuracaoJob;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getStatus() {
    return this.status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getLog() {
    return this.log;
  }

  public void setLog(String log) {
    this.log = log;
  }

  public LocalDateTime getDataInicio() {
    return this.dataInicio;
  }

  public void setDataInicio(LocalDateTime dataInicio) {
    this.dataInicio = dataInicio;
  }

  public LocalDateTime getDataConclusao() {
    return this.dataConclusao;
  }

  public void setDataConclusao(LocalDateTime dataConclusao) {
    this.dataConclusao = dataConclusao;
  }

  public String getCodigoExecucao() {
    return this.codigoExecucao;
  }

  public void setCodigoExecucao(String codigoExecucao) {
    this.codigoExecucao = codigoExecucao;
  }

}