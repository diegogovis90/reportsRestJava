package br.srv.company.reports.dto;

import java.util.List;

public class AnoRelatorioDto {
  private Integer ano;
  private List<AnosRelatoriosDto> relatorios;
  
  public Integer getAno() {
	  return this.ano;
  }

  public void setAno(Integer areAno) {
	  this.ano = areAno;
  }

  public List<AnosRelatoriosDto> getRelatorios() {
	  return this.relatorios;
  }

  public void setRelatorios(List<AnosRelatoriosDto> relatorios) {
	  this.relatorios = relatorios;
  }
}
