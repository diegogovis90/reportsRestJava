package br.srv.company.reports.dto;

public class ControleDto {

  private Integer id;
  private String nome;
  private String rotulo;
  private boolean obrigatorio;
  private boolean somenteLeitura;
  private boolean visivel;
  private boolean atualizarOutrosParametros;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getRotulo() {
    return this.rotulo;
  }

  public void setRotulo(String rotulo) {
    this.rotulo = rotulo;
  }

  public boolean getObrigatorio() {
    return this.obrigatorio;
  }

  public void setObrigatorio(boolean obrigatorio) {
    this.obrigatorio = obrigatorio;
  }

  public boolean getSomenteLeitura() {
    return this.somenteLeitura;
  }

  public void setSomenteLeitura(boolean somenteLeitura) {
    this.somenteLeitura = somenteLeitura;
  }

  public boolean getVisivel() {
    return this.visivel;
  }

  public void setVisivel(boolean visivel) {
    this.visivel = visivel;
  }

  public boolean getAtualizarOutrosParametros() {
    return this.atualizarOutrosParametros;
  }

  public void setAtualizarOutrosParametros(boolean atualizarOutrosParametros) {
    this.atualizarOutrosParametros = atualizarOutrosParametros;
  }

}