package br.srv.company.reports.dto;

import java.util.List;

public class FiltroRelatorioDto {

  private String nome;
  private String rotulo;
  private String tipo;
  private Integer controle;
  private boolean obrigatorio;
  private boolean visivel;
  private List<OpcaoFiltroRelatorio> opcoes;
  private Object valor;

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getRotulo() {
    return this.rotulo;
  }

  public void setRotulo(String rotulo) {
    this.rotulo = rotulo;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public Integer getControle() {
    return this.controle;
  }

  public void setControle(Integer controle) {
    this.controle = controle;
  }

  public boolean isObrigatorio() {
    return this.obrigatorio;
  }

  public boolean getObrigatorio() {
    return this.obrigatorio;
  }

  public void setObrigatorio(boolean obrigatorio) {
    this.obrigatorio = obrigatorio;
  }

  public boolean isVisivel() {
    return this.visivel;
  }

  public boolean getVisivel() {
    return this.visivel;
  }

  public void setVisivel(boolean visivel) {
    this.visivel = visivel;
  }

  public List<OpcaoFiltroRelatorio> getOpcoes() {
    return this.opcoes;
  }

  public void setOpcoes(List<OpcaoFiltroRelatorio> opcoes) {
    this.opcoes = opcoes;
  }

  public Object getValor() {
    return this.valor;
  }

  public void setValor(Object valor) {
    this.valor = valor;
  }

}
