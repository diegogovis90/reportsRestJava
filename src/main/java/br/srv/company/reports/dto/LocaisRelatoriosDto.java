package br.srv.company.reports.dto;

public class LocaisRelatoriosDto {

  private Integer id;
  private RelatorioDto relatorio;
  private LocalDto local;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public RelatorioDto getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(RelatorioDto relatorio) {
    this.relatorio = relatorio;
  }

  public LocalDto getLocal() {
    return this.local;
  }

  public void setLocal(LocalDto local) {
    this.local = local;
  }

}