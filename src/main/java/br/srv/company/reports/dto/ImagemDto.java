package br.srv.company.reports.dto;

import java.time.LocalDateTime;

public class ImagemDto {

	private Integer id;
	private String nome;
	private String formato;
	private byte[] dados;
	private LocalDateTime data;
	private long utilizacao;
	private String tamanhoArquivo;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getFormato() {
		return this.formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public byte[] getDados() {
		return this.dados;
	}

	public void setDados(byte[] dados) {
		this.dados = dados;
	}

	public LocalDateTime getData() {
		return this.data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public long getUtilizacao() {
		return utilizacao;
	}

	public void setUtilizacao(long utilizacao) {
		this.utilizacao = utilizacao;
	}

	public String getTamanhoArquivo() {
		return tamanhoArquivo;
	}

	public void setTamanhoArquivo(String tamanhoArquivo) {
		this.tamanhoArquivo = tamanhoArquivo;
	}

}