package br.srv.company.reports.dto;

public class ConfigControleDto {

  private String classe;
  private Integer controle;

  public String getClasse() {
    return this.classe;
  }

  public void setClasse(String classe) {
    this.classe = classe;
  }

  public Integer getControle() {
    return this.controle;
  }

  public void setControle(Integer controle) {
    this.controle = controle;
  }

}