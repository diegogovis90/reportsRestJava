package br.srv.company.reports.dto;

public class RelatorioBasicoDto {

	private Integer id;
	private String nome;
	private Integer categoriaId;
	private String categoria;
	private Integer ano;
	private String tipo;
	//campos usados na página de categoria e local
	private boolean marcado;
	private boolean temLocal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isMarcado() {
		return marcado;
	}

	public void setMarcado(boolean marcado) {
		this.marcado = marcado;
	}

	public boolean isTemLocal() {
		return temLocal;
	}

	public void setTemLocal(boolean temLocal) {
		this.temLocal = temLocal;
	}
}