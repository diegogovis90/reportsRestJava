package br.srv.company.reports.dto;

public class PropriedadeControleDto {

  private Integer id;
  private String nome;
  private String tipo;
  private long totalItens;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public long getTotalItens() {
	return totalItens;
  }

  public void setTotalItens(long totalItens) {
	this.totalItens = totalItens;
  }
}