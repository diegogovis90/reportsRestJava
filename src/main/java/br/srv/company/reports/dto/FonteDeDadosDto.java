package br.srv.company.reports.dto;

import br.srv.company.reports.model.JdbcDriver;

public class FonteDeDadosDto {

  private Integer id;
  private String tipo;
  private String nome;
  private String servidor;
  private String nomeBD;
  private String url;
  private String usuario;
  private String senha;
  private String opcoes;
  private JdbcDriver driver;
  private boolean temSenha;
  private long utilizacao;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getServidor() {
    return this.servidor;
  }

  public void setServidor(String servidor) {
    this.servidor = servidor;
  }

  public String getNomeBD() {
    return this.nomeBD;
  }

  public void setNomeBD(String nomeBD) {
    this.nomeBD = nomeBD;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUsuario() {
    return this.usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  public String getSenha() {
    return this.senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  public String getOpcoes() {
    return this.opcoes;
  }

  public void setOpcoes(String opcoes) {
    this.opcoes = opcoes;
  }

  public JdbcDriver getDriver() {
    return this.driver;
  }

  public void setDriver(JdbcDriver driver) {
    this.driver = driver;
  }

  public boolean getTemSenha() {
	return temSenha;
  }

  public void setTemSenha(boolean temSenha) {
	this.temSenha = temSenha;
  }

  public long getUtilizacao() {
	return utilizacao;
  }

  public void setUtilizacao(long utilizacao) {
	this.utilizacao = utilizacao;
  }
  
  public void montarURL() {
	  url = "jdbc:sqlserver://"+getServidor()+";user="+getUsuario()+";databaseName="+getNomeBD()+";"+getOpcoes();
  }
}