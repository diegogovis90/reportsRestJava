package br.srv.company.reports.dto;

public class ColunaRelatorioDto {

  private Integer id;
  private String nome;
  private String rotulo;
  private String rotuloPersonalizado;
  private boolean visivel;
  private Integer posicao;
  private String tipo;
  private RelatorioUsuarioDto relatorioUsuario;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getRotulo() {
    return this.rotulo;
  }

  public void setRotulo(String rotulo) {
    this.rotulo = rotulo;
  }

  public String getRotuloPersonalizado() {
    return this.rotuloPersonalizado;
  }

  public void setRotuloPersonalizado(String rotuloPersonalizado) {
    this.rotuloPersonalizado = rotuloPersonalizado;
  }

  public boolean getVisivel() {
    return this.visivel;
  }

  public void setVisivel(boolean visivel) {
    this.visivel = visivel;
  }

  public Integer getPosicao() {
    return this.posicao;
  }

  public void setPosicao(Integer posicao) {
    this.posicao = posicao;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public RelatorioUsuarioDto getRelatorioUsuario() {
    return this.relatorioUsuario;
  }

  public void setRelatorioUsuario(RelatorioUsuarioDto relatorioUsuario) {
    this.relatorioUsuario = relatorioUsuario;
  }

}