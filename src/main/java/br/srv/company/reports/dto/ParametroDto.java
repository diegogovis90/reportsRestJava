package br.srv.company.reports.dto;

public class ParametroDto {

  private Integer id;
  private String nome;
  private Integer tipoControle;
  private Integer tipoPropriedade;
  private String tipoClasse;
  private short ordem;
  private String valor;
  private PropriedadeControleDto propriedadeControle;
  private ControleDto controle;
  private RelatorioDto relatorio;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Integer getTipoControle() {
    return this.tipoControle;
  }

  public void setTipoControle(Integer tipoControle) {
    this.tipoControle = tipoControle;
  }

  public Integer getTipoPropriedade() {
    return this.tipoPropriedade;
  }

  public void setTipoPropriedade(Integer tipoPropriedade) {
    this.tipoPropriedade = tipoPropriedade;
  }

  public String getTipoClasse() {
    return this.tipoClasse;
  }

  public void setTipoClasse(String tipoClasse) {
    this.tipoClasse = tipoClasse;
  }

  public short getOrdem() {
    return this.ordem;
  }

  public void setOrdem(short ordem) {
    this.ordem = ordem;
  }

  public String getValor() {
    return this.valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

  public PropriedadeControleDto getPropriedadeControle() {
    return this.propriedadeControle;
  }

  public void setPropriedadeControle(PropriedadeControleDto propriedadeControle) {
    this.propriedadeControle = propriedadeControle;
  }

  public ControleDto getControle() {
    return this.controle;
  }

  public void setControle(ControleDto controle) {
    this.controle = controle;
  }

  public RelatorioDto getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(RelatorioDto relatorio) {
    this.relatorio = relatorio;
  }

}