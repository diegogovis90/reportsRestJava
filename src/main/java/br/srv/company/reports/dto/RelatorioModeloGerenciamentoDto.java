package br.srv.company.reports.dto;

import java.io.Serializable;
import java.util.List;

public class RelatorioModeloGerenciamentoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer ano;
	private String codigo;
	private String nome;
	private short ordenacao;
	private Integer fonteDeDadosId;
	private Integer modeloId;
	private List<ParametroFiltroDto> parametros;
	private String mensagem;
	private List<Integer> imagens;

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public short getOrdenacao() {
		return ordenacao;
	}

	public void setOrdenacao(short ordenacao) {
		this.ordenacao = ordenacao;
	}

	public Integer getFonteDeDadosId() {
		return fonteDeDadosId;
	}

	public void setFonteDeDadosId(Integer fonteDeDadosId) {
		this.fonteDeDadosId = fonteDeDadosId;
	}

	public Integer getModeloId() {
		return modeloId;
	}

	public void setModeloId(Integer modeloId) {
		this.modeloId = modeloId;
	}

	public List<ParametroFiltroDto> getParametros() {
		return parametros;
	}

	public void setParametros(List<ParametroFiltroDto> parametros) {
		this.parametros = parametros;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<Integer> getImagens() {
		return imagens;
	}

	public void setImagens(List<Integer> imagens) {
		this.imagens = imagens;
	}
}