package br.srv.company.reports.dto;

import java.util.List;

public class RelatorioDto {

  private Integer id;
  private String nome;
  private String tipo;
  private String mensagem;
  private String codigo;
  private Integer idCategoria;
  private String codigoCategoria;
  private String nomeCategoria;
  private Integer ordenacao;
  private RelatorioDto referencia;
  private List<RelatorioPermissaoDto> permissoes;
  private List<Integer> anos;
  private List<LocaisRelatoriosDto> locais;
  private List<NotaExplicativaDto> notasExplicativas;
  private List<ParametroDto> parametros;

  public Integer getIdCategoria() {
    return this.idCategoria;
  }

  public void setIdCategoria(Integer idCategoria) {
    this.idCategoria = idCategoria;
  }

  public String getCodigoCategoria() {
    return this.codigoCategoria;
  }

  public void setCodigoCategoria(String codigoCategoria) {
    this.codigoCategoria = codigoCategoria;
  }

  public String getNomeCategoria() {
    return this.nomeCategoria;
  }

  public void setNomeCategoria(String nomeCategoria) {
    this.nomeCategoria = nomeCategoria;
  }

  public List<ParametroDto> getParametros() {
    return this.parametros;
  }

  public void setParametros(List<ParametroDto> parametros) {
    this.parametros = parametros;
  }

  public List<LocaisRelatoriosDto> getLocais() {
    return this.locais;
  }

  public void setLocais(List<LocaisRelatoriosDto> locais) {
    this.locais = locais;
  }

  public List<Integer> getAnos() {
	return anos;
  }

  public void setAnos(List<Integer> anos) {
	this.anos = anos;
  }

  public List<LocaisRelatoriosDto> getRelatorios() {
    return this.locais;
  }

  public void setRelatorios(List<LocaisRelatoriosDto> locais) {
    this.locais = locais;
  }

  public List<RelatorioPermissaoDto> getPermissoes() {
    return this.permissoes;
  }

  public void setPermissoes(List<RelatorioPermissaoDto> permissoes) {
    this.permissoes = permissoes;
  }

  public List<NotaExplicativaDto> getNotasExplicativas() {
    return this.notasExplicativas;
  }

  public void setNotasExplicativas(List<NotaExplicativaDto> notasExplicativas) {
    this.notasExplicativas = notasExplicativas;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getMensagem() {
    return this.mensagem;
  }

  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }

  public String getCodigo() {
    return this.codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public Integer getOrdenacao() {
    return this.ordenacao;
  }

  public void setOrdenacao(Integer ordenacao) {
    this.ordenacao = ordenacao;
  }

  public RelatorioDto getReferencia() {
    return this.referencia;
  }

  public void setReferencia(RelatorioDto referencia) {
    this.referencia = referencia;
  }

}