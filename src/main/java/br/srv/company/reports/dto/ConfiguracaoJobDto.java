package br.srv.company.reports.dto;

public class ConfiguracaoJobDto {

  private Integer id;
  private String nome;
  private String descricao;
  private String caminhoBat;
  private String servicoAtualizacacoJob;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescricao() {
    return this.descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public String getCaminhoBat() {
    return this.caminhoBat;
  }

  public void setCaminhoBat(String caminhoBat) {
    this.caminhoBat = caminhoBat;
  }

  public String getServicoAtualizacacoJob() {
    return this.servicoAtualizacacoJob;
  }

  public void setServicoAtualizacacoJob(String servicoAtualizacacoJob) {
    this.servicoAtualizacacoJob = servicoAtualizacacoJob;
  }

}