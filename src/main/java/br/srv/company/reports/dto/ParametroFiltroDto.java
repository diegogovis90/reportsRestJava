package br.srv.company.reports.dto;

public class ParametroFiltroDto {

	private Integer id;
	private String nome;
	private Integer tipoControle;
	private Integer tipoPropriedade;
	private Integer propriedadeId;
	private String nomeControle;
	private String rotulo;
	private short ordem;
	private boolean obrigatorio;
	private boolean somenteLeitura;
	private boolean visivel;
	
	public ParametroFiltroDto(){}
	
	public ParametroFiltroDto(Integer id, String nome, Integer tipoControle, Integer tipoPropriedade, Integer propriedadeId, 
	String nomeControle, String rotulo, short ordem, boolean obrigatorio, boolean somenteLeitura, boolean visivel){
		this.id = id;
		this.nome = nome;
		this.tipoControle = tipoControle;
		this.tipoPropriedade = tipoPropriedade;
		this.propriedadeId = propriedadeId;
		this.nomeControle = nomeControle;
		this.rotulo = rotulo;
		this.ordem = ordem;
		this.obrigatorio = obrigatorio;
		this.somenteLeitura = somenteLeitura;
		this.visivel = visivel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getTipoControle() {
		return tipoControle;
	}

	public void setTipoControle(Integer tipoControle) {
		this.tipoControle = tipoControle;
	}

	public Integer getTipoPropriedade() {
		return tipoPropriedade;
	}

	public void setTipoPropriedade(Integer tipoPropriedade) {
		this.tipoPropriedade = tipoPropriedade;
	}

	public Integer getPropriedadeId() {
		return propriedadeId;
	}

	public void setPropriedadeId(Integer propriedadeId) {
		this.propriedadeId = propriedadeId;
	}

	public String getNomeControle() {
		return nomeControle;
	}

	public void setNomeControle(String nomeControle) {
		this.nomeControle = nomeControle;
	}

	public String getRotulo() {
		return rotulo;
	}

	public void setRotulo(String rotulo) {
		this.rotulo = rotulo;
	}

	public short getOrdem() {
		return ordem;
	}

	public void setOrdem(short ordem) {
		this.ordem = ordem;
	}

	public boolean isObrigatorio() {
		return obrigatorio;
	}

	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}

	public boolean isSomenteLeitura() {
		return somenteLeitura;
	}

	public void setSomenteLeitura(boolean somenteLeitura) {
		this.somenteLeitura = somenteLeitura;
	}

	public boolean isVisivel() {
		return visivel;
	}

	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}
}