package br.srv.company.reports.dto;

public class JdbcDriverDto {

  private Integer id;
  private String nome;
  private boolean classe;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public boolean getClasse() {
    return this.classe;
  }

  public void setClasse(boolean classe) {
    this.classe = classe;
  }

}