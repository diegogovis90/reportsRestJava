package br.srv.company.reports.dto;

import java.util.List;

public class RelatorioParametrosDto {

  private Integer id;
  private String nome;
  private String categoria;
  private boolean temNotaExplicativa;
  private List<FiltroRelatorioDto> parametros;
  private String mensagem;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCategoria() {
    return this.categoria;
  }

  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }

  public boolean isTemNotaExplicativa() {
    return this.temNotaExplicativa;
  }

  public boolean getTemNotaExplicativa() {
    return this.temNotaExplicativa;
  }

  public void setTemNotaExplicativa(boolean temNotaExplicativa) {
    this.temNotaExplicativa = temNotaExplicativa;
  }

  public List<FiltroRelatorioDto> getParametros() {
    return this.parametros;
  }

  public void setParametros(List<FiltroRelatorioDto> parametros) {
    this.parametros = parametros;
  }

  public String getMensagem() {
    return this.mensagem;
  }

  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }

}