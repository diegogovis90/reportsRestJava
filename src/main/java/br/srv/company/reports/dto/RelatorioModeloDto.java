package br.srv.company.reports.dto;

import java.io.Serializable;
import java.util.List;

public class RelatorioModeloDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean personalizavel;
	private ModeloDto modelo;
	private FonteDeDadosDto fonte;
	private List<RelatoriosModelosImagensDto> imagens;

	public boolean getPersonalizavel() {
		return this.personalizavel;
	}

	public void setPersonalizavel(boolean personalizavel) {
		this.personalizavel = personalizavel;
	}

	public ModeloDto getModelo() {
		return this.modelo;
	}

	public void setModelo(ModeloDto modelo) {
		this.modelo = modelo;
	}

	public FonteDeDadosDto getFonte() {
		return this.fonte;
	}

	public void setFonte(FonteDeDadosDto fonte) {
		this.fonte = fonte;
	}

	public List<RelatoriosModelosImagensDto> getImagens() {
		return imagens;
	}

	public void setImagens(List<RelatoriosModelosImagensDto> imagens) {
		this.imagens = imagens;
	}

}