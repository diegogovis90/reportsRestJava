package br.srv.company.reports.dto;

public class NotaExplicativaDto {

  private Integer id;
  private Integer sequencia;
  private String texto;
  private boolean ativa;
  private RelatorioDto relatorio;

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getSequencia() {
    return this.sequencia;
  }

  public void setSequencia(Integer sequencia) {
    this.sequencia = sequencia;
  }

  public String gettexto() {
    return this.texto;
  }

  public void settexto(String texto) {
    this.texto = texto;
  }

  public boolean isAtiva() {
    return this.ativa;
  }

  public boolean getAtiva() {
    return this.ativa;
  }

  public void setAtiva(boolean ativa) {
    this.ativa = ativa;
  }

  public RelatorioDto getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(RelatorioDto relatorio) {
    this.relatorio = relatorio;
  }

}