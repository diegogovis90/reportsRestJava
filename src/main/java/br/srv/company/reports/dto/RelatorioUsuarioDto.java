package br.srv.company.reports.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class RelatorioUsuarioDto implements Serializable {
  private static final long serialVersionUID = 1L;

  private RelatorioDto relatorio;
  private Integer exercicio;
  private String usuario;
  private String sql;
  private FonteDeDadosDto fonte;
  private List<ColunaRelatorioDto> colunas;

  public List<ColunaRelatorioDto> getColunas() {
    return this.colunas;
  }

  public void setColunas(List<ColunaRelatorioDto> colunas) {
    this.colunas = colunas;
  }

  public RelatorioDto getRelatorio() {
    return this.relatorio;
  }

  public void setRelatorio(RelatorioDto relatorio) {
    this.relatorio = relatorio;
  }

  public Integer getExercicio() {
    return this.exercicio;
  }

  public void setExercicio(Integer exercicio) {
    this.exercicio = exercicio;
  }

  public String getUsuario() {
    return this.usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  public String getSql() {
    return this.sql;
  }

  public void setSql(String sql) {
    this.sql = sql;
  }

  public FonteDeDadosDto getFonte() {
    return this.fonte;
  }

  public void setFonte(FonteDeDadosDto fonte) {
    this.fonte = fonte;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (!(o instanceof RelatorioUsuarioDto)) {
      return false;
    }
    RelatorioUsuarioDto relatorioUsuario = (RelatorioUsuarioDto) o;
    return Objects.equals(relatorio, relatorioUsuario.relatorio) && exercicio == relatorioUsuario.exercicio
        && Objects.equals(usuario, relatorioUsuario.usuario) && Objects.equals(sql, relatorioUsuario.sql)
        && Objects.equals(fonte, relatorioUsuario.fonte) && Objects.equals(colunas, relatorioUsuario.colunas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(relatorio, exercicio, usuario, sql, fonte, colunas);
  }

}