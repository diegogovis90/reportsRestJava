package br.srv.company.reports.dto;

import java.time.LocalDateTime;

public class ModeloDto {

  private Integer id;
  private String nome;
  private String nomeArquivo;
  private String nomeArquivoTabulado;
  private byte[] dados;
  private LocalDateTime dataEnvioArquivo;
  private LocalDateTime dataModificacaoArquivo;
  private String cpfModificacaoArquivo;
  private byte[] dados2;
  private LocalDateTime dataEnvioArquivoTabulado;
  private LocalDateTime dataModificacaoArquivoTabulado;
  private String cpfModificacaoArquivoTabulado;
  private String cpfCriacao;
  private String cpfModificacao;
  private LocalDateTime data;
  private LocalDateTime dataModificacao;
  private long utilizacao;
  private String tamanhoArquivo;
  private String tamanhoArquivoTabulado;

  public ModeloDto() {}
  
  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNomeArquivo() {
    return this.nomeArquivo;
  }

  public void setNomeArquivo(String nomeArquivo) {
    this.nomeArquivo = nomeArquivo;
  }

  public String getNomeArquivoTabulado() {
	return nomeArquivoTabulado;
  }

  public void setNomeArquivoTabulado(String nomeArquivoTabulado) {
	this.nomeArquivoTabulado = nomeArquivoTabulado;
  }

  public byte[] getDados() {
    return this.dados;
  }

  public void setDados(byte[] dados) {
    this.dados = dados;
  }

  public LocalDateTime getDataEnvioArquivo() {
    return this.dataEnvioArquivo;
  }

  public void setDataEnvioArquivo(LocalDateTime dataEnvioArquivo) {
    this.dataEnvioArquivo = dataEnvioArquivo;
  }

  public LocalDateTime getDataModificacaoArquivo() {
    return this.dataModificacaoArquivo;
  }

  public void setDataModificacaoArquivo(LocalDateTime dataModificacaoArquivo) {
    this.dataModificacaoArquivo = dataModificacaoArquivo;
  }

  public String getCpfModificacaoArquivo() {
    return this.cpfModificacaoArquivo;
  }

  public void setCpfModificacaoArquivo(String cpfModificacaoArquivo) {
    this.cpfModificacaoArquivo = cpfModificacaoArquivo;
  }

  public byte[] getDados2() {
    return this.dados2;
  }

  public void setDados2(byte[] dados2) {
    this.dados2 = dados2;
  }

  public LocalDateTime getDataEnvioArquivoTabulado() {
    return this.dataEnvioArquivoTabulado;
  }

  public void setDataEnvioArquivoTabulado(LocalDateTime dataEnvioArquivoTabulado) {
    this.dataEnvioArquivoTabulado = dataEnvioArquivoTabulado;
  }

  public LocalDateTime getDataModificacaoArquivoTabulado() {
    return this.dataModificacaoArquivoTabulado;
  }

  public void setDataModificacaoArquivoTabulado(LocalDateTime dataModificacaoArquivoTabulado) {
    this.dataModificacaoArquivoTabulado = dataModificacaoArquivoTabulado;
  }

  public String getCpfModificacaoArquivoTabulado() {
    return this.cpfModificacaoArquivoTabulado;
  }

  public void setCpfModificacaoArquivoTabulado(String cpfModificacaoArquivoTabulado) {
    this.cpfModificacaoArquivoTabulado = cpfModificacaoArquivoTabulado;
  }

  public String getCpfCriacao() {
    return this.cpfCriacao;
  }

  public void setCpfCriacao(String cpfCriacao) {
    this.cpfCriacao = cpfCriacao;
  }

  public String getCpfModificacao() {
    return this.cpfModificacao;
  }

  public void setCpfModificacao(String cpfModificacao) {
    this.cpfModificacao = cpfModificacao;
  }

  public LocalDateTime getData() {
    return this.data;
  }

  public void setData(LocalDateTime data) {
    this.data = data;
  }

  public LocalDateTime getDataModificacao() {
    return this.dataModificacao;
  }

  public void setDataModificacao(LocalDateTime dataModificacao) {
    this.dataModificacao = dataModificacao;
  }

  public long getUtilizacao() {
	return utilizacao;
  }

  public void setUtilizacao(long utilizacao) {
	this.utilizacao = utilizacao;
  }

  public String getTamanhoArquivo() {
	return tamanhoArquivo;
  }

  public void setTamanhoArquivo(String tamanhoArquivo) {
	this.tamanhoArquivo = tamanhoArquivo;
  }

  public String getTamanhoArquivoTabulado() {
	return tamanhoArquivoTabulado;
  }

  public void setTamanhoArquivoTabulado(String tamanhoArquivoTabulado) {
	this.tamanhoArquivoTabulado = tamanhoArquivoTabulado;
  }
}