package br.srv.company.reports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import br.srv.company.reports.util.ConfiguracaoUtil;

@SpringBootApplication
public class ReportsApplication extends SpringBootServletInitializer {

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        if(!ConfiguracaoUtil.temConfigInterna())
        	application.properties(ConfiguracaoUtil.carregaConfigAcesso());
		
        return application.sources(ReportsApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ReportsApplication.class);
		if(!ConfiguracaoUtil.temParametroComConfigExterna(args) && 
				!ConfiguracaoUtil.temConfigInterna())
			application.setDefaultProperties(ConfiguracaoUtil.carregaConfigAcesso());
		
		application.run(args);
	}
}
