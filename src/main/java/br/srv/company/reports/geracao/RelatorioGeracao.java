package br.srv.company.reports.geracao;

public class RelatorioGeracao {
  private ParametroGeracao[] parametros;
  private String[] campos;
  private String sql;

  public ParametroGeracao[] getParametros() {
    return parametros;
  }

  public void setParametros(ParametroGeracao[] parametros) {
    this.parametros = parametros;
  }

  public String[] getCampos() {
    return campos;
  }

  public void setCampos(String[] campos) {
    this.campos = campos;
  }

  public String getSql() {
    return sql;
  }

  public void setSql(String sql) {
    this.sql = sql;
  }

}