package br.srv.company.reports.geracao;

import java.io.File;

public class RelatorioExportado {

  private File arquivo;
  private String nomeRel;

  public RelatorioExportado(File arquivo, String nomeRel) {
    this.arquivo = arquivo;
    this.nomeRel = nomeRel;
  }

  public File getArquivo() {
    return this.arquivo;
  }

  public String getNomeRel() {
    return this.nomeRel;
  }

}