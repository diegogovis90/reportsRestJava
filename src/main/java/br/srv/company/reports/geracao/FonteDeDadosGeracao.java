package br.srv.company.reports.geracao;

public class FonteDeDadosGeracao {
  private int id;
  private String url;
  private String usuario;
  private String senha;

  public FonteDeDadosGeracao(String url, String usuario, String senha) {
    this.url = url;
    this.usuario = usuario;
    this.senha = senha;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUsuario() {
    return usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

}