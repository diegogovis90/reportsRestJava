package br.srv.company.reports.geracao;

public class ParametroGeracao {
  private String nome;
  private String tipo;
  private boolean visivel;
  private Object valor;
  private String valorPadrao;

  public ParametroGeracao(String nome, String tipo, boolean visivel, Object valor, String valorPadrao) {
    this.nome = nome;
    this.tipo = tipo;
    this.visivel = visivel;
    this.valor = valor;
    this.valorPadrao = valorPadrao;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public boolean isVisivel() {
    return visivel;
  }

  public void setVisivel(boolean visivel) {
    this.visivel = visivel;
  }

  public Object getValor() {
    return valor;
  }

  public void setValor(Object valor) {
    this.valor = valor;
  }

  public String getValorPadrao() {
    return valorPadrao;
  }

  public void setValorPadrao(String valorPadrao) {
    this.valorPadrao = valorPadrao;
  }

}