package br.srv.company.reports.geracao;

import java.io.ByteArrayInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.ExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleTextExporterConfiguration;
import net.sf.jasperreports.export.SimpleTextReportConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

public class GeracaoHelper {
  private static final Logger logger = LoggerFactory.getLogger(GeracaoHelper.class);

  public static JasperReport obterJasperReport(byte[] modelo) {
    try {
      return (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modelo));
    } catch (JRException ex) {
      logger.error(ex.getMessage(), ex);
    }
    return null;
  }

  public static RelatorioGeracao obterRelatorio(JasperReport jasperReport) {
    RelatorioGeracao relatorio = new RelatorioGeracao();
    JRParameter[] jrParameters = jasperReport.getParameters();
    List<ParametroGeracao> parametros = new ArrayList<>();
    for (JRParameter jrParameter : jrParameters) {
      if (jrParameter.isSystemDefined()) {
        continue;
      }
      parametros.add(new ParametroGeracao(jrParameter.getName(), jrParameter.getValueClassName(),
          jrParameter.isForPrompting(), null, ""));
    }
    relatorio.setParametros(parametros.toArray(new ParametroGeracao[parametros.size()]));

    JRField[] jrFields = jasperReport.getFields();
    String[] campos = new String[jrFields.length];
    for (int i = 0; i < jrFields.length; i++) {
      campos[i] = jrFields[i].getName();
    }
    relatorio.setCampos(campos);
    relatorio.setSql(jasperReport.getMainDataset().getQuery().getText());
    return relatorio;
  }

  public static RelatorioGeracao obterRelatorio(byte[] modelo) {
    JasperReport jasperReport = obterJasperReport(modelo);
    if (jasperReport == null) {
      return null;
    }
    return obterRelatorio(jasperReport);
  }

  public static JasperPrint obterJasperPrint(byte[] modelo, FonteDeDadosGeracao fdd, List<byte[]> imagens,
      List<ParametroGeracao> parametros) {
    JasperReport jasperReport = obterJasperReport(modelo);

    if (jasperReport == null) {
      return null;
    }
    Map<String, Object> params = new HashMap<>();
    params.put("IMAGEM", imagens);
    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
    loopJRParameters: for (JRParameter jrParam : jasperReport.getParameters()) {
      if (jrParam.isSystemDefined() || jrParam.getName().equals("IMAGEM")) {
        continue;
      }
      for (ParametroGeracao p : parametros) {
        if (p.getNome().equalsIgnoreCase(jrParam.getName()) && jrParam.isForPrompting()) {
          params.put(jrParam.getName(), p.getValor());
          continue loopJRParameters;
        }
      }
    }

    try {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      try (Connection conexaoBD = DriverManager.getConnection(fdd.getUrl(), fdd.getUsuario(), fdd.getSenha())) {
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conexaoBD);
        return jasperPrint;
      }
    } catch (ClassNotFoundException | SQLException | JRException ex) {
      logger.error(ex.getMessage(), ex);
    }
    return null;
  }

  public static String export(JasperPrint jasperPrint, String type, String dir) throws Exception {
    Path path = Paths.get(dir, RandomStringUtils.randomAlphanumeric(12) + "." + type);
    ExporterOutput output = new SimpleOutputStreamExporterOutput(path.toFile());
    JRAbstractExporter exporter;
    try {
      switch (type) {
        case "pdf":
          exporter = new JRPdfExporter();
          break;
        case "xlsx":
          exporter = new JRXlsxExporter();
          agregarConfiguracoesPlanilha(exporter);
          break;
        case "xls":
          exporter = new JRXlsExporter();
          agregarConfiguracoesPlanilha(exporter);
          break;
        case "csv":
          exporter = new JRCsvExporter();
          output = new SimpleWriterExporterOutput(path.toFile());
          break;
        case "ods":
          exporter = new JROdsExporter();
          break;
        case "docx":
          exporter = new JRDocxExporter();
          break;
        case "odt":
          exporter = new JROdtExporter();
          break;
        case "txt":
          exporter = new JRTextExporter();
          SimpleTextExporterConfiguration config = new SimpleTextExporterConfiguration();
          config.setPageSeparator("");
          exporter.setConfiguration(config);
          SimpleTextReportConfiguration reportConfig = new SimpleTextReportConfiguration();
          reportConfig.setCharWidth(Float.valueOf(20));
          reportConfig.setCharHeight(Float.valueOf(20));
          exporter.setConfiguration(reportConfig);
          output = new SimpleWriterExporterOutput(path.toFile());
          break;
        default:
          return "";
      }
      exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
      exporter.setExporterOutput(output);
      exporter.exportReport();
    } catch (JRException ex) {
      logger.error(ex.getMessage(), ex);
      throw ex;
    }
    return path.toString();
  }

  private static void agregarConfiguracoesPlanilha(JRAbstractExporter exporter) {
    SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
    configuration.setRemoveEmptySpaceBetweenRows(true);
    configuration.setRemoveEmptySpaceBetweenColumns(true);
    configuration.setWhitePageBackground(false);
    configuration.setDetectCellType(true);
    exporter.setConfiguration(configuration);
  }
}
