package br.srv.company.reports.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.srv.company.reports.model.ErroRespostaValidacao;
import br.srv.company.reports.model.ErroValidacao;
import br.srv.company.reports.util.ValidacaoUtil;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOG.error("", ex);
		List<ErroValidacao> erros = ValidacaoUtil.processarErros(ex.getBindingResult());

		return handleExceptionInternal(ex, new ErroRespostaValidacao(erros), headers, HttpStatus.BAD_REQUEST, request);
	}

}
