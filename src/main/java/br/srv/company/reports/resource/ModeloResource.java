package br.srv.company.reports.resource;

import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.srv.company.reports.converter.ModeloConverter;
import br.srv.company.reports.dto.ModeloDto;
import br.srv.company.reports.model.Modelo;
import br.srv.company.reports.service.ModeloService;
import br.srv.company.reports.dao.JdbcDriverDao;
import net.bytebuddy.implementation.bind.annotation.Empty;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Transactional
@RestController
@RequestMapping("/modelos")
public class ModeloResource {

	@Autowired
	ModeloService modeloService;
	@Autowired
	JdbcDriverDao jdbcDriverDao;
	@Autowired
	private ModeloConverter modeloConverter;

	@GetMapping("")
	public ResponseEntity<Modelos> listar(@RequestParam("termo") String termo,
			@RequestParam(value = "indice", required = false) @Empty Integer indice,
			@RequestParam(value = "limite", required = false) @Empty Integer limite) {
		List<Modelo> modelos = (modeloService.listar(termo));
		List<ModeloDto> itens = modeloConverter.toDto(modelos);
		Modelos resposta = new Modelos();
		if(indice != null && limite != null) {
			int fim = (indice + limite);
			int lastIndex = (itens.size());
			resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		}else {
			resposta.setItens(itens);
		}
		resposta.setTotal(itens.size());
		
		return ResponseEntity.ok(resposta);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ModeloDto> obter(@PathVariable("id") int id) {
		Modelo modelo = modeloService.buscar(id);
		if (modelo == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(modeloConverter.toDto(modelo));
	}
	
	@GetMapping("/{id}/parametros")
	public ResponseEntity<List<ParametroGerenciamento>> obterParametros(@PathVariable("id") int id) throws JRException{
		List<ParametroGerenciamento> parametros = new ArrayList<ParametroGerenciamento>();
		Modelo modelo = modeloService.buscar(id);
		JasperReport report = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modelo.getModDados()));
		JRParameter[] parametrosModelo = report.getParameters();
		for(JRParameter par : parametrosModelo) {
			if (!par.isSystemDefined() && !par.getName().equals("IMAGEM")) {
				ParametroGerenciamento p = new ParametroGerenciamento();
				p.setNome(par.getName());
				p.setRotulo("");
				p.setObrigatorio(false);
				p.setVisivel(true);
				p.setControle(0);
				parametros.add(p);
			}
		}
		return ResponseEntity.ok(parametros);
	}
	
	@GetMapping("/{id}/download")
	public ResponseEntity<byte[]> download(@PathVariable("id") int id, @RequestParam("tipo") int tipo) {
		Modelo modelo = modeloService.buscar(id);
		if (modelo == null) {
			return ResponseEntity.notFound().build();
		}	
		String nomeArquivo;
		byte[] dados;
		if(tipo == 0) {
			nomeArquivo = modelo.getModNomeArquivo()+".jasper";
			dados = modelo.getModDados();
		}else if(tipo == 1) {
			nomeArquivo = modelo.getModNomeArquivo()+"_excel.jasper";
			dados = modelo.getModDados2();
		}else {
			return ResponseEntity.notFound().build();
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
		headers.setContentDisposition(ContentDisposition.parse("attachment; filename=\""+nomeArquivo+"\""));
		return new ResponseEntity<byte[]>(dados, headers, HttpStatus.OK);
	}

	@PostMapping(consumes = "multipart/form-data")
	public ResponseEntity<?> criar(@Valid @RequestParam String dados, 
			@RequestParam MultipartFile arquivo,
			@RequestParam MultipartFile arquivoTabulado) throws Exception {
		JSONObject jsonObj = new JSONObject(dados);
		Modelo modelo = new Modelo();

		// TODO: Pegar CPF do usuário logado
		modelo.setModCPFCriacao("11111111111");
		modelo.setModCPFModificacao("11111111111");
		modelo.setModCPFModificacaoArquivo("11111111111");
		modelo.setModCPFModificacaoArquivoTabulado("11111111111");
		modelo.setModDados(arquivo.getBytes());
		modelo.setModDados2(arquivoTabulado.getBytes());
		// TODO: Verificar timezone
		modelo.setModData(LocalDateTime.now());
		modelo.setModDataEnvioArquivo(LocalDateTime.now());
		modelo.setModDataEnvioArquivoTabulado(LocalDateTime.now());
		modelo.setModDataModificacao(LocalDateTime.now());
		modelo.setModDataModificacaoArquivo(LocalDateTime.now());
		modelo.setModDataModificacaoArquivoTabulado(LocalDateTime.now());
		modelo.setModNome(jsonObj.getString("nome"));
		modelo.setModNomeArquivo(arquivo.getOriginalFilename().replace(".jasper", ""));
		modelo.setModNomeArquivoTabulado(arquivoTabulado.getOriginalFilename().replace(".jasper", ""));
		
		modeloService.salvar(modelo);
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping(path = "/{id}/validar", consumes = "multipart/form-data")
	public ResponseEntity<?> validar(@PathVariable("id") int id,
			@Valid @RequestParam String dados,
			@RequestParam(required = false) @Empty MultipartFile arquivo,
			@RequestParam(required = false) @Empty MultipartFile arquivoTabulado
			) throws Exception {
		RespostaValidacao res = new RespostaValidacao();
		res.setPadrao_valido(true);
		res.setTabulado_valido(true);
		
		if(arquivo == null && arquivoTabulado == null) {
			return ResponseEntity.ok(res);
		}
		
		Modelo modeloPersistido = modeloService.buscar(id);
		
		if (arquivo != null && modeloPersistido.getModDados() != null) {
			JasperReport modelReportPersistido = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modeloPersistido.getModDados()));
			JasperReport modelReport = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(arquivo.getBytes()));
			
			JRParameter[] modelParamPersistido = modelReportPersistido.getParameters();
			JRParameter[] modelParam = modelReport.getParameters();
			
			String modelValorPersistido = "";
			String modelValor = "";

			for (JRParameter jr : modelParamPersistido) {
				modelValorPersistido += jr.getName();
			}
			for (JRParameter jr : modelParam) {
				modelValor += jr.getName();
			}

			if (!modelValorPersistido.equals(modelValor)) {
				res.setPadrao_valido(false);
			}
		}
		
		if (arquivoTabulado != null && modeloPersistido.getModDados2() != null) {
			JasperReport model2ReportPersistido = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modeloPersistido.getModDados2()));
			JasperReport model2Report = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(arquivoTabulado.getBytes()));

			JRParameter[] model2ParamPersistido = model2ReportPersistido.getParameters();
			JRParameter[] model2Param = model2Report.getParameters();

			String model2ValorPersistido = "";
			String model2Valor = "";

			for (JRParameter jr : model2ParamPersistido) {
				model2ValorPersistido += jr.getName();
			}
			for (JRParameter jr : model2Param) {
				model2Valor += jr.getName();
			}

			if (!model2ValorPersistido.equals(model2Valor)) {
				res.setTabulado_valido(false);
			}
		}

		return ResponseEntity.ok(res);
	}
	
	@PutMapping(path = "/{id}", consumes = "multipart/form-data")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestParam String dados,
			@RequestParam(required = false) @Empty MultipartFile arquivo,
			@RequestParam(required = false) @Empty MultipartFile arquivoTabulado) throws Exception {
		Modelo modeloPersistido = modeloService.buscar(id);
		if (modeloPersistido == null) {
			return ResponseEntity.notFound().build();
		}
		JSONObject jsonObj = new JSONObject(dados);
		Modelo modelo = new Modelo();
		modelo.setModID(id);
		modelo.setModCPFCriacao(modeloPersistido.getModCPFCriacao());
		modelo.setModData(modeloPersistido.getModData());
		modelo.setModDataEnvioArquivo(modeloPersistido.getModDataEnvioArquivo());
		modelo.setModDataEnvioArquivoTabulado(modeloPersistido.getModDataEnvioArquivoTabulado());

		// TODO: Pegar CPF do usuário logado
		// TODO: Verificar timezone
		modelo.setModCPFModificacao("11111111111");
		modelo.setModDataModificacao(LocalDateTime.now());

		if (arquivo != null && !arquivo.getBytes().equals(modeloPersistido.getModDados())) {
			modelo.setModCPFModificacaoArquivo("11111111111");
			modelo.setModDados(arquivo.getBytes());
			modelo.setModNomeArquivo(arquivo.getOriginalFilename().replace(".jasper", ""));
			modelo.setModDataModificacaoArquivo(LocalDateTime.now());
		} else {
			modelo.setModCPFModificacaoArquivo(modeloPersistido.getModCPFModificacaoArquivo());
			modelo.setModDados(modeloPersistido.getModDados());
			modelo.setModNomeArquivo(modeloPersistido.getModNomeArquivo());
			modelo.setModDataModificacaoArquivo(modeloPersistido.getModDataModificacaoArquivo());
		}

		if (arquivoTabulado != null && !arquivoTabulado.getBytes().equals(modeloPersistido.getModDados2())) {
			modelo.setModDados2(arquivoTabulado.getBytes());
			modelo.setModNomeArquivoTabulado(arquivoTabulado.getOriginalFilename().replace(".jasper", ""));
			modelo.setModCPFModificacaoArquivoTabulado("11111111111");
			modelo.setModDataModificacaoArquivoTabulado(LocalDateTime.now());
			if (modeloPersistido.getModDados2() == null) {
				modelo.setModDataEnvioArquivoTabulado(LocalDateTime.now());
			}
		} else {
			if (jsonObj.has("nomeArquivoTabulado") && ("").equals(jsonObj.getString("nomeArquivoTabulado"))) {
				modelo.setModCPFModificacaoArquivoTabulado("11111111111");
				modelo.setModDataModificacaoArquivoTabulado(LocalDateTime.now());
				modelo.setModDataEnvioArquivoTabulado(null);
			} else {
				modelo.setModDados2(modeloPersistido.getModDados2());
				modelo.setModNomeArquivoTabulado(modeloPersistido.getModNomeArquivoTabulado());
				modelo.setModCPFModificacaoArquivoTabulado(modeloPersistido.getModCPFModificacaoArquivoTabulado());
				modelo.setModDataModificacaoArquivoTabulado(modeloPersistido.getModDataModificacaoArquivoTabulado());
			}
		}

		modelo.setModNome(jsonObj.getString("nome"));

		modeloService.atualizar(modelo);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		Modelo modelo = modeloService.buscar(id);
		if (modelo == null) {
			return ResponseEntity.notFound().build();
		}
		modeloService.excluir(modelo);
		return ResponseEntity.noContent().build();
	}
	
	@SuppressWarnings("unused")
	private static class ParametroGerenciamento {
		String nome;
		String rotulo;
		String tipo;
		Integer controle;
		boolean obrigatorio;
		boolean visivel;
		String opcoes;
		
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getRotulo() {
			return rotulo;
		}
		public void setRotulo(String rotulo) {
			this.rotulo = rotulo;
		}
		public String getTipo() {
			return tipo;
		}
		public void setTipo(String tipo) {
			this.tipo = tipo;
		}
		public Integer getControle() {
			return controle;
		}
		public void setControle(Integer controle) {
			this.controle = controle;
		}
		public boolean isObrigatorio() {
			return obrigatorio;
		}
		public void setObrigatorio(boolean obrigatorio) {
			this.obrigatorio = obrigatorio;
		}
		public boolean isVisivel() {
			return visivel;
		}
		public void setVisivel(boolean visivel) {
			this.visivel = visivel;
		}
		public String getOpcoes() {
			return opcoes;
		}
		public void setOpcoes(String opcoes) {
			this.opcoes = opcoes;
		}
	}

	@SuppressWarnings("unused")
	private static class Modelos {
		int total;
		List<ModeloDto> itens;
		
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public List<ModeloDto> getItens() {
			return itens;
		}
		public void setItens(List<ModeloDto> itens) {
			this.itens = itens;
		}
	}
	
	@SuppressWarnings("unused")
	private static class RespostaValidacao {
		boolean padrao_valido;
		boolean tabulado_valido;
		
		public boolean isPadrao_valido() {
			return padrao_valido;
		}
		public void setPadrao_valido(boolean padrao_valido) {
			this.padrao_valido = padrao_valido;
		}
		public boolean isTabulado_valido() {
			return tabulado_valido;
		}
		public void setTabulado_valido(boolean tabulado_valido) {
			this.tabulado_valido = tabulado_valido;
		}
	}
}