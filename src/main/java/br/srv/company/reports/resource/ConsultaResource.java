package br.srv.company.reports.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.ConsultaConverter;
import br.srv.company.reports.dto.ConsultaDto;
import br.srv.company.reports.model.Consulta;
import br.srv.company.reports.service.ConsultaService;
import net.bytebuddy.implementation.bind.annotation.Empty;

@Transactional
@RestController
@RequestMapping("/listas-por-consulta")
public class ConsultaResource {
	
	@Autowired
	ConsultaService consultaService;
	@Autowired
	ConsultaConverter consultaConverter;

	@GetMapping("")
	public ResponseEntity<Consultas> listar(@RequestParam("termo") String termo,
			@RequestParam(value = "indice", required = false) @Empty Integer indice,
			@RequestParam(value = "limite", required = false) @Empty Integer limite) {
		List<Consulta> consultas = consultaService.listar(termo);
		List<ConsultaDto> itens = consultaConverter.toBasicoDto(consultas);
		Consultas resposta = new Consultas();
		if (indice != null && limite != null) {
			int fim = (indice + limite);
			int lastIndex = (itens.size());
			resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		} else {
			resposta.setItens(itens);
		}
		resposta.setTotal(itens.size());

		return ResponseEntity.ok(resposta);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> obter(@PathVariable("id") int id) {
		Consulta consulta = consultaService.buscar(id);
		if (consulta == null) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(consultaConverter.toBasicoDto(consulta));
	}
	
	@GetMapping("/{id}/consulta")
	public ResponseEntity<?> obterConsulta(@PathVariable("id") int id) {
		Consulta consulta = consultaService.buscar(id);
		if (consulta == null) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(consultaConverter.toDto(consulta));
	}
	
	@PostMapping
	public ResponseEntity<?> salvar(@Valid @RequestBody ConsultaDto dto) throws Exception {
		Consulta consulta = consultaConverter.toObject(dto);
		consultaService.salvar(consulta);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody ConsultaDto dto) throws Exception {
		Consulta consultaPersistido = consultaService.buscar(id);
		if (consultaPersistido == null) {
			return ResponseEntity.notFound().build();
		}
		
		dto.setId(id);
		Consulta consulta = consultaConverter.toObject(dto);
		consultaService.atualizar(consulta);

		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		Consulta consulta = consultaService.buscar(id);
		if (consulta == null) {
			return ResponseEntity.notFound().build();
		}
		consultaService.excluir(consulta);
		return ResponseEntity.noContent().build();
	}
	
	@SuppressWarnings("unused")
	private static class Consultas {
		int total;
		List<ConsultaDto> itens;
		
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public List<ConsultaDto> getItens() {
			return itens;
		}
		public void setItens(List<ConsultaDto> itens) {
			this.itens = itens;
		}
	}
}