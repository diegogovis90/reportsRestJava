package br.srv.company.reports.resource;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.CategoriaConverter;
import br.srv.company.reports.converter.RelatorioConverter;
import br.srv.company.reports.dto.CategoriaDto;
import br.srv.company.reports.dto.RelatorioAcaoDto;
import br.srv.company.reports.dto.RelatorioBasicoDto;
import br.srv.company.reports.model.Categoria;
import br.srv.company.reports.model.Relatorio;
import br.srv.company.reports.service.CategoriaService;
import br.srv.company.reports.service.RelatorioService;

@Transactional
@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	CategoriaService catService;
	@Autowired
	CategoriaConverter catConverter;
	@Autowired
	RelatorioService relService;
	@Autowired
	RelatorioConverter relConverter;

	@GetMapping("")
	public ResponseEntity<List<CategoriaDto>> listar() {
		List<Categoria> categorias = (catService.buscarTodos());
		List<CategoriaDto> dtos = catConverter.toDto(categorias);

		return ResponseEntity.ok(dtos);
	}

	@GetMapping("/{id}")
	public ResponseEntity<CategoriaDto> buscar(@PathVariable("id") int id) {
		Categoria categoria = catService.buscar(id);
		CategoriaDto dto = catConverter.toDto(categoria);

		return ResponseEntity.ok(dto);
	}

	@PutMapping("/{id}/relatorio")
	public ResponseEntity<?> listarRelatorios(@PathVariable("id") int id, @RequestParam("ano") int ano,
			@RequestParam("termo") String termo, @RequestParam("indice") int indice,
			@RequestParam("limite") int limite, @RequestParam("tipo_marcacao") int tipoMarcacao) {
		//tipoMarcacao 2 = marcar todos, tipoMarcacao 1 = desmarcar todos, tipoMarcacao 0 = default 
		
		List<Relatorio> rels = relService.listarPorCategoria(ano, id, termo, tipoMarcacao);
		List<RelatorioBasicoDto> itens = relConverter.toDtoBasico(rels);

		RelatoriosCategoria resposta = new RelatoriosCategoria();
		int fim = (indice + limite);
		int lastIndex = (itens.size());
		resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		resposta.setTotal(itens.size());
		resposta.setTotalMarcado(itens.stream().filter(rel -> rel.isMarcado()).count());
		
		//problema quando tipoMarcacao era 1/2 anteriormente, e há troca de página
		if (tipoMarcacao != 0) {
			List<Relatorio> relsMarcados = relService.listarAlteracaoMarcacao(ano, id, termo, tipoMarcacao);
			HashMap<Integer,RelatorioAcaoDto> itensMarcados = new HashMap<Integer,RelatorioAcaoDto>();
			for(var marcado : relsMarcados) {
				RelatorioAcaoDto dto = relConverter.toDtoRelatorioAcao(marcado);
				itensMarcados.put(marcado.getRelID(), dto);
			}
			resposta.setItensMarcados(itensMarcados);
		}
		return ResponseEntity.ok(resposta);
	}

	@PostMapping
	public ResponseEntity<?> salvar(@Valid @RequestBody CategoriaDto dto) throws Exception {
		Categoria categoria = catConverter.toObject(dto);
		catService.salvar(categoria);

		for (RelatorioAcaoDto rel : dto.getRelatorios()) {
			if (rel.getAcao() == 0) {
				catService.removerRelatorio(categoria.getCatID(), rel.getId());
			}
			if (rel.getAcao() == 1) {
				catService.adicionarRelatorio(categoria.getCatID(), rel.getId());
			}
		}

		return ResponseEntity.noContent().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody CategoriaDto dto)
			throws Exception {
		Categoria categoriaPersistida = catService.buscar(id);
		if (categoriaPersistida == null) {
			return ResponseEntity.notFound().build();
		}
		dto.setId(id);
		Categoria categoria = catConverter.toObject(dto);
		catService.atualizar(categoria);

		for (RelatorioAcaoDto rel : dto.getRelatorios()) {
			if (rel.getAcao() == 0) {
				catService.removerRelatorio(id, rel.getId());
			}
			if (rel.getAcao() == 1) {
				catService.adicionarRelatorio(id, rel.getId());
			}
		}

		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		Categoria categoria = catService.buscar(id);
		if (categoria == null) {
			return ResponseEntity.notFound().build();
		}
		catService.excluir(categoria);
		return ResponseEntity.noContent().build();
	}

	@SuppressWarnings("unused")
	private static class RelatoriosCategoria {
		int total;
		long totalMarcado;
		List<RelatorioBasicoDto> itens;
		HashMap<Integer,RelatorioAcaoDto> itensMarcados;
		
		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public long getTotalMarcado() {
			return totalMarcado;
		}

		public void setTotalMarcado(long totalMarcado) {
			this.totalMarcado = totalMarcado;
		}

		public List<RelatorioBasicoDto> getItens() {
			return itens;
		}

		public void setItens(List<RelatorioBasicoDto> itens) {
			this.itens = itens;
		}

		public HashMap<Integer, RelatorioAcaoDto> getItensMarcados() {
			return itensMarcados;
		}

		public void setItensMarcados(HashMap<Integer, RelatorioAcaoDto> itensMarcados) {
			this.itensMarcados = itensMarcados;
		}
	}
}