package br.srv.company.reports.resource;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.LocalConverter;
import br.srv.company.reports.converter.RelatorioConverter;
import br.srv.company.reports.dto.LocalDto;
import br.srv.company.reports.dto.RelatorioAcaoDto;
import br.srv.company.reports.dto.RelatorioBasicoDto;
import br.srv.company.reports.model.Local;
import br.srv.company.reports.model.Relatorio;
import br.srv.company.reports.service.LocalService;
import br.srv.company.reports.service.RelatorioService;
import net.bytebuddy.implementation.bind.annotation.Empty;

@Transactional
@RestController
@RequestMapping("/locais")
public class LocalResource {

	@Autowired
	LocalService localService;
	@Autowired
	private LocalConverter localConverter;
	@Autowired
	RelatorioService relService;
	@Autowired
	RelatorioConverter relConverter;

	@GetMapping("")
	public ResponseEntity<Locais> listar(@RequestParam("termo") String termo,
			@RequestParam(value = "indice", required = false) @Empty Integer indice,
			@RequestParam(value = "limite", required = false) @Empty Integer limite) {
		List<Local> locais = (localService.listar(termo));
		List<LocalDto> itens = localConverter.toDto(locais);
		Locais resposta = new Locais();
		if(indice != null && limite != null) {
			int fim = (indice + limite);
			int lastIndex = (itens.size());
			resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		}else {
			resposta.setItens(itens);
		}
		resposta.setTotal(itens.size());
		
		return ResponseEntity.ok(resposta);
	}

	@GetMapping("/{id}")
	public ResponseEntity<LocalDto> obter(@PathVariable("id") int id) {
		Local local = localService.buscar(id);
		if (local == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(localConverter.toDto(local));
	}
	
	@PutMapping("/{id}/relatorios")
	public ResponseEntity<?> listarRelatorios(@PathVariable("id") int id, @RequestParam("ano") int ano,
			@RequestParam("termo") String termo, @RequestParam("indice") int indice,
			@RequestParam("limite") int limite, @RequestParam("tipo_marcacao") int tipoMarcacao) {
		//tipoMarcacao 2 = marcar todos, tipoMarcacao 1 = desmarcar todos, tipoMarcacao 0 = default 
		
		List<Relatorio> rels = relService.listarPorLocal(ano, id, termo, tipoMarcacao);
		List<RelatorioBasicoDto> itens = relConverter.toDtoBasico(rels);

		RelatoriosLocal resposta = new RelatoriosLocal();
		int fim = (indice + limite);
		int lastIndex = (itens.size());
		resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		resposta.setTotal(itens.size());
		resposta.setTotalMarcado(itens.stream().filter(rel -> rel.isMarcado()).count());
		
		//problema quando tipoMarcacao era 1/2 anteriormente, e há troca de página
		if (tipoMarcacao != 0) {
			List<Relatorio> relsMarcados = relService.listarAlteracaoMarcacao(ano, id, termo, tipoMarcacao);
			HashMap<Integer,RelatorioAcaoDto> itensMarcados = new HashMap<Integer,RelatorioAcaoDto>();
			for(var marcado : relsMarcados) {
				RelatorioAcaoDto dto = relConverter.toDtoRelatorioAcao(marcado);
				itensMarcados.put(marcado.getRelID(), dto);
			}
			resposta.setItensMarcados(itensMarcados);
		}
		return ResponseEntity.ok(resposta);
	}
	
	@PostMapping
	public ResponseEntity<?> salvar(@Valid @RequestBody LocalDto dto) throws Exception {
		Local local = localConverter.toObject(dto);
		localService.salvar(local);

		for (RelatorioAcaoDto rel : dto.getRelatorios()) {
			if (rel.getAcao() == 0) {
				localService.removerRelatorio(local.getLocID(), rel.getId());
			}
			if (rel.getAcao() == 1) {
				localService.adicionarRelatorio(local.getLocID(), rel.getId());
			}
		}

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody LocalDto dto)
			throws Exception {
		Local localPersistida = localService.buscar(id);
		if (localPersistida == null) {
			return ResponseEntity.notFound().build();
		}
		dto.setId(id);
		Local local = localConverter.toObject(dto);
		localService.atualizar(local);

		for (RelatorioAcaoDto rel : dto.getRelatorios()) {
			if (rel.getAcao() == 0) {
				localService.removerRelatorio(id, rel.getId());
			}
			if (rel.getAcao() == 1) {
				localService.adicionarRelatorio(id, rel.getId());
			}
		}

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		Local local = localService.buscar(id);
		if (local == null) {
			return ResponseEntity.notFound().build();
		}
		localService.excluir(local);
		return ResponseEntity.noContent().build();
	}

	@SuppressWarnings("unused")
	private static class Locais {
		int total;
		List<LocalDto> itens;
		
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public List<LocalDto> getItens() {
			return itens;
		}
		public void setItens(List<LocalDto> itens) {
			this.itens = itens;
		}
	}
	
	@SuppressWarnings("unused")
	private static class RelatoriosLocal {
		int total;
		long totalMarcado;
		List<RelatorioBasicoDto> itens;
		HashMap<Integer,RelatorioAcaoDto> itensMarcados;
		
		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public long getTotalMarcado() {
			return totalMarcado;
		}

		public void setTotalMarcado(long totalMarcado) {
			this.totalMarcado = totalMarcado;
		}

		public List<RelatorioBasicoDto> getItens() {
			return itens;
		}

		public void setItens(List<RelatorioBasicoDto> itens) {
			this.itens = itens;
		}

		public HashMap<Integer, RelatorioAcaoDto> getItensMarcados() {
			return itensMarcados;
		}

		public void setItensMarcados(HashMap<Integer, RelatorioAcaoDto> itensMarcados) {
			this.itensMarcados = itensMarcados;
		}
	}
}