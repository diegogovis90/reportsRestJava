package br.srv.company.reports.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.RelatorioConverter;
import br.srv.company.reports.dto.RelatorioDto;
import br.srv.company.reports.service.RelatorioService;

@RestController
@RequestMapping("/gerenciamento")
public class GerenciamentoResource {

  @Autowired
  RelatorioService relService;
  @Autowired
  private RelatorioConverter relatorioConverter;

  @GetMapping("/relatorios/{id}")
  public ResponseEntity<RelatorioDto> obter(@PathVariable("id") int id) {
    return ResponseEntity.ok(relatorioConverter.toDto(relService.buscar(id)));
  }
  
}