package br.srv.company.reports.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.ImagemConverter;
import br.srv.company.reports.converter.ParametroConverter;
import br.srv.company.reports.converter.RelatorioModeloConverter;
import br.srv.company.reports.dto.ImagemDto;
import br.srv.company.reports.dto.ParametroFiltroDto;
import br.srv.company.reports.dto.RelatorioModeloBasicoDto;
import br.srv.company.reports.dto.RelatorioModeloGerenciamentoDto;
import br.srv.company.reports.model.Imagem;
import br.srv.company.reports.model.RelatorioModelo;
import br.srv.company.reports.service.RelatorioModeloService;

@Transactional
@RestController
@RequestMapping("/relatorios-modelos")
public class RelatorioModeloResource {

	@Autowired
	RelatorioModeloService relatorioModeloService;
	@Autowired
	RelatorioModeloConverter relatorioModeloConverter;
	@Autowired
	ParametroConverter parametroConverter;
	@Autowired
	ImagemConverter imgConverter;

	@GetMapping("/{id}")
	public ResponseEntity<RelatorioModeloBasicoDto> obter(@PathVariable("id") int id) {
		RelatorioModelo rel = relatorioModeloService.buscar(id);
		RelatorioModeloBasicoDto dto = relatorioModeloConverter.toBasicoDto(rel);
		return ResponseEntity.ok(dto);
	}

	@GetMapping("/{id}/parametros")
	public ResponseEntity<List<ParametroFiltroDto>> obterParametros(@PathVariable("id") int id) {
		RelatorioModelo rel = relatorioModeloService.buscar(id);
		List<ParametroFiltroDto> parametros = parametroConverter.toFiltroDto(rel.getParametros());
		return ResponseEntity.ok(parametros);
	}

	@GetMapping("/{id}/imagens")
	public ResponseEntity<List<ImagemDto>> obterImagens(@PathVariable("id") int id) {
		List<Imagem> imgs = relatorioModeloService.obterImagens(id);
		List<ImagemDto> dtos = imgConverter.toDto(imgs);
		return ResponseEntity.ok(dtos);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody RelatorioModeloGerenciamentoDto dto)
			throws Exception {
		RelatorioModelo relatorioModeloPersistido = relatorioModeloService.buscar(id);
		if (relatorioModeloPersistido == null) {
			return ResponseEntity.notFound().build();
		}
		RelatorioModelo relatorioModelo = relatorioModeloConverter.toObject(id, dto);
		relatorioModeloService.atualizar(relatorioModelo);
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping
	public ResponseEntity<?> salvar(@Valid @RequestBody RelatorioModeloGerenciamentoDto dto)
			throws Exception {
		RelatorioModelo relatorioModelo = relatorioModeloConverter.toObject(null, dto);
		relatorioModeloService.salvar(relatorioModelo);
		return ResponseEntity.noContent().build();
	}
}