package br.srv.company.reports.resource;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.srv.company.reports.converter.RelatorioConverter;
import br.srv.company.reports.dto.CategoriaDto;
import br.srv.company.reports.dto.FiltroRelatorioDto;
import br.srv.company.reports.dto.RelatorioBasicoDto;
import br.srv.company.reports.dto.RelatorioDto;
import br.srv.company.reports.dto.RelatorioParametrosDto;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.geracao.RelatorioExportado;
import br.srv.company.reports.model.Relatorio;
import br.srv.company.reports.service.CategoriaService;
import br.srv.company.reports.service.GeracaoRelatorioService;
import br.srv.company.reports.service.RelatorioService;

@RestController
@RequestMapping("/relatorios")
public class RelatorioResource {

	@Autowired
	GeracaoRelatorioService gerService;
	@Autowired
	RelatorioService relService;
	@Autowired
	CategoriaService catService;
	@Autowired
	private RelatorioConverter relatorioConverter;

	@GetMapping("")
	public ResponseEntity<List<RelatorioDto>> listar(
			@RequestParam(name = "ano", required = false, defaultValue = "0") Integer ano,
			@RequestParam(name = "categoria", required = false, defaultValue = "0") Integer categoria) {
		// TODO: Na versão em Go esse endpoint também permite listar os relatórios
		// personalizados do usuário
		return ResponseEntity.ok(relService.listar(ano, categoria));
	}

	@PostMapping("/{id}/{tipo}")
	public ResponseEntity<?> exportarRelatorio(HttpServletResponse response,
			@RequestBody List<FiltroRelatorioDto> params, @PathVariable("id") Integer id,
			@PathVariable("tipo") String tipo, @RequestParam(value = "visualizar", required = false) boolean visualizar)
			throws BusinessException {

		if (tipo.equals("pdf") && visualizar) {
			RelatorioExportado exp = gerService.gerarArquivo(id, tipo, params);
			try {
				return ResponseEntity.ok(new ObjectMapper().writeValueAsString(exp.getArquivo().getName()));
			} catch (JsonProcessingException e) {
				// TODO: Log de erro
				throw new BusinessException("Erro inesperado");
			}
		}
		gerService.gerarParaDownload(response, id, tipo, params);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<RelatorioDto> obter(@PathVariable("id") Integer id,
			@RequestParam(name = "ano", required = false, defaultValue = "0") Integer ano) {
		RelatorioDto dto = relService.obterComAcessoPorAno(id, ano);
		return ResponseEntity.ok(dto);
	}

	@GetMapping("/ano")
	public ResponseEntity<List<Integer>> obterTodosAnos() {
		return ResponseEntity.ok(relService.listarAnos());
	}

	@GetMapping("/categoria")
	public ResponseEntity<List<CategoriaDto>> listarCategorias(
			@RequestParam(name = "ano", required = false, defaultValue = "0") Integer ano) {
		return ResponseEntity.ok(catService.listarPorAno(ano));
	}

	@GetMapping("/ver/{arquivo}")
	public ResponseEntity<?> baixarTemporario(HttpServletResponse response, @PathVariable("arquivo") String arquivo)
			throws BusinessException {

		gerService.baixarTemporario(response, arquivo);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/{id}/parametro")
	public ResponseEntity<RelatorioParametrosDto> obterParametros(HttpServletRequest request,
			@PathVariable("id") Integer id) throws BusinessException {
		return ResponseEntity.ok(relService.obterParametros(id, request.getParameterMap()));
	}

	@GetMapping("/basico")
	public ResponseEntity<RelatoriosBasicos> listarTodos(@RequestParam("ano") int ano,
			@RequestParam("termo") String termo,
			@RequestParam(name = "indice", required = false, defaultValue = "0") int indice,
			@RequestParam("limite") int limite) throws BusinessException {
		List<Relatorio> rels = (relService.listarTodos(ano, termo));
		List<RelatorioBasicoDto> itens = relatorioConverter.toDtoBasico(rels);
		RelatoriosBasicos resposta = new RelatoriosBasicos();
		int fim = (indice + limite);
		int lastIndex = (itens.size());
		resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		resposta.setTotal(itens.size());
		return ResponseEntity.ok(resposta);
	}

	@SuppressWarnings("unused")
	private static class RelatoriosBasicos {
		int total;
		List<RelatorioBasicoDto> itens;

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}

		public List<RelatorioBasicoDto> getItens() {
			return itens;
		}

		public void setItens(List<RelatorioBasicoDto> itens) {
			this.itens = itens;
		}
	}

}