package br.srv.company.reports.resource;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.srv.company.reports.converter.ImagemConverter;
import br.srv.company.reports.dto.ImagemDto;
import br.srv.company.reports.model.Imagem;
import br.srv.company.reports.service.ImagemService;
import br.srv.company.reports.util.ImagemUtil;
import net.bytebuddy.implementation.bind.annotation.Empty;

@Transactional
@RestController
@RequestMapping("/imagens")
public class ImagemResource {

	@Autowired
	ImagemService imgService;
	@Autowired
	ImagemConverter imgConverter;

	@GetMapping("")
	public ResponseEntity<List<ImagemDto>> listar(@RequestParam(value = "termo", required = false) @Empty String termo,
			@RequestParam(value = "excl", required = false) @Empty List<Integer> excl) {
		List<Imagem> imgs = imgService.listar(termo, excl);
		List<ImagemDto> dtos = imgConverter.toDto(imgs);
		return ResponseEntity.ok(dtos);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> obter(@PathVariable("id") int id,
			@RequestParam(value = "ver", required = false) @Empty Integer ver) {
		Imagem img = imgService.buscar(id);
		if (ver == null) {
			return ResponseEntity.ok(imgConverter.toDto(img));
		} else if (ver == 1) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType(ImagemUtil.obterTipoConteudo(img.getImaFormato())));
			headers.setContentDispositionFormData(img.getImaNome(), img.getImaNome() + "." + img.getImaFormato());
			return new ResponseEntity<byte[]>(img.getImaDados(), headers, HttpStatus.OK);
		}
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping
	public ResponseEntity<?> criar(@Valid @RequestParam String dados,
			@RequestParam(required = false) @Empty MultipartFile arquivo) throws Exception {
		
		JSONObject jsonObj = new JSONObject(dados);
		ImagemDto dto = new ImagemDto();
		dto.setNome(jsonObj.getString("nome"));
		if (arquivo != null) {
			dto.setDados(arquivo.getBytes());
			String[] splitFormato = arquivo.getOriginalFilename().split("\\.");
			dto.setFormato(splitFormato[splitFormato.length - 1]);
		}
		dto.setData(LocalDateTime.now());

		imgService.salvar(imgConverter.toObject(dto));
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestParam String dados,
			@RequestParam(required = false) @Empty MultipartFile arquivo) throws Exception {
		Imagem imgPersistida = imgService.buscar(id);
		if (imgPersistida == null) {
			return ResponseEntity.notFound().build();
		}

		JSONObject jsonObj = new JSONObject(dados);
		ImagemDto dto = new ImagemDto();
		dto.setId(id);
		dto.setNome(jsonObj.getString("nome"));
		dto.setData(LocalDateTime.now());
		
		if (arquivo != null) {
			dto.setDados(arquivo.getBytes());
			String[] splitFormato = arquivo.getOriginalFilename().split("\\.");
			dto.setFormato(splitFormato[splitFormato.length - 1]);
		}else {
			dto.setDados(imgPersistida.getImaDados());
			dto.setFormato(imgPersistida.getImaFormato());
		}

		imgService.atualizar(imgConverter.toObject(dto));
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		Imagem img = imgService.buscar(id);
		if (img == null) {
			return ResponseEntity.notFound().build();
		}
		imgService.excluir(img);
		return ResponseEntity.noContent().build();
	}
}