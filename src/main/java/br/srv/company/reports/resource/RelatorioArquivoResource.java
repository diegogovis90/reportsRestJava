package br.srv.company.reports.resource;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.srv.company.reports.converter.RelatorioArquivoConverter;
import br.srv.company.reports.dto.RelatorioArquivoDto;
import br.srv.company.reports.model.RelatorioArquivo;
import br.srv.company.reports.service.RelatorioArquivoService;
import net.bytebuddy.implementation.bind.annotation.Empty;

@Transactional
@RestController
@RequestMapping("/relatorios-arquivos")
public class RelatorioArquivoResource {

	@Autowired
	RelatorioArquivoService relatorioArquivoService;
	@Autowired
	RelatorioArquivoConverter relatorioArquivoConverter;

	@PostMapping(consumes = "multipart/form-data")
	public ResponseEntity<?> salvar(@Valid @RequestParam String dados, @RequestParam MultipartFile arquivo)
			throws Exception {
		JSONObject jsonObj = new JSONObject(dados);
		RelatorioArquivoDto dto = new RelatorioArquivoDto();
		String[] splitFormato = arquivo.getOriginalFilename().split("\\.");
		
		dto.setDados(arquivo.getBytes());
		dto.setFormato(splitFormato[splitFormato.length-1]);
		dto.setAnos(converterLista(jsonObj.getJSONArray("anos")));
		dto.setCodigo(jsonObj.getString("codigo"));
		dto.setNome(jsonObj.getString("nome"));
		dto.setOrdenacao(jsonObj.getInt("ordenacao"));
		relatorioArquivoService.salvar(relatorioArquivoConverter.toObject(dto));
		return ResponseEntity.noContent().build();
	}

	@PutMapping(path = "/{id}", consumes = "multipart/form-data")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestParam String dados,
			@RequestParam(required = false) @Empty MultipartFile arquivo) throws Exception {
		RelatorioArquivo relatorioArquivoPersistido = relatorioArquivoService.buscar(id);
		if (relatorioArquivoPersistido == null) {
			return ResponseEntity.notFound().build();
		} 
		
		JSONObject jsonObj = new JSONObject(dados);
		RelatorioArquivoDto dto = new RelatorioArquivoDto();
		dto.setId(id);
		
		if(arquivo != null) {
			String[] splitFormato = arquivo.getOriginalFilename().split("\\.");
			dto.setDados(arquivo.getBytes());
			dto.setFormato(splitFormato[splitFormato.length-1]);
		}		
		
		dto.setAnos(converterLista(jsonObj.getJSONArray("anos")));
		dto.setCodigo(jsonObj.getString("codigo"));
		dto.setNome(jsonObj.getString("nome"));
		dto.setOrdenacao(jsonObj.getInt("ordenacao"));
		
		relatorioArquivoService.atualizar(relatorioArquivoConverter.toObject(dto));
		return ResponseEntity.noContent().build();
	}
	
	private List<Integer> converterLista(JSONArray array){
		List<Integer> lista = new ArrayList<Integer>();
		
		for(Object obj : array.toList()) {
			lista.add((Integer)obj);
		}
		return lista;
	}
}