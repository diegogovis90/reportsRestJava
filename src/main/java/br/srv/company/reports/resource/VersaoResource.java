package br.srv.company.reports.resource;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/versao")
public class VersaoResource {

  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> obterVersao() {
    // TODO: Implementar endpoint de versão
    return ResponseEntity.ok("5.0");
  }

}