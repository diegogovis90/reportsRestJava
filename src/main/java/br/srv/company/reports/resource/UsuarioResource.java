package br.srv.company.reports.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

  // FIXME: Refatorar quando for implementar a autenticação
  @GetMapping("/mim")
  public ResponseEntity<UsuarioAuth> obterUsuario() {
    return ResponseEntity.ok(new UsuarioAuth("Usuario teste", true));
  }

  // FIXME: Refatorar quando for implementar a autenticação
  public class UsuarioAuth {
    private String nome;
    private boolean master;

    public UsuarioAuth(String nome, boolean master) {
      this.nome = nome;
      this.master = master;
    }

    public String getNome() {
      return this.nome;
    }

    public void setNome(String nome) {
      this.nome = nome;
    }

    public boolean isMaster() {
      return this.master;
    }

    public boolean getMaster() {
      return this.master;
    }

    public void setMaster(boolean master) {
      this.master = master;
    }

  }
}