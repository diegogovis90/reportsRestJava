package br.srv.company.reports.resource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.FonteDeDadosConverter;
import br.srv.company.reports.dao.JdbcDriverDao;
import br.srv.company.reports.dto.FonteDeDadosDto;
import br.srv.company.reports.model.FonteDeDados;
import br.srv.company.reports.model.JdbcDriver;
import br.srv.company.reports.service.FonteDeDadosService;
import br.srv.company.reports.util.CryptoUtil;

@Transactional
@RestController
@RequestMapping("/fontes-de-dados")
public class FonteDeDadosResource {

	@Autowired
	FonteDeDadosService fonteDeDadosService;
	@Autowired
	JdbcDriverDao jdbcDriverDao;
	@Autowired
	private FonteDeDadosConverter fonteDeDadosConverter;

	@GetMapping("")
	public ResponseEntity<List<FonteDeDadosDto>> listar() {
		List<FonteDeDados> fontes = (fonteDeDadosService.listar());
		return ResponseEntity.ok(fonteDeDadosConverter.toBasicoDto(fontes));
	}

	@GetMapping("/{id}")
	public ResponseEntity<FonteDeDadosDto> obter(@PathVariable("id") int id) {
		FonteDeDados fonte = fonteDeDadosService.buscar(id);
		if (fonte == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(fonteDeDadosConverter.toDto(fonte));
	}

	@PostMapping
	public ResponseEntity<?> criar(@Valid @RequestBody FonteDeDadosDto dto) throws Exception {
		JdbcDriver driver = jdbcDriverDao.buscar(1);
		dto.setDriver(driver);
		dto.montarURL();
		FonteDeDados fonte = fonteDeDadosConverter.toObject(dto);
		fonteDeDadosService.salvar(fonte);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody FonteDeDadosDto dto)
			throws Exception {
		FonteDeDados fontePersistido = fonteDeDadosService.buscar(id);
		if (fontePersistido == null) {
			return ResponseEntity.notFound().build();
		}

		dto.setId(id);
		dto.setDriver(fontePersistido.getDriver());
		if (dto.getSenha() == null) {
			dto.setSenha(CryptoUtil.decodificar(fontePersistido.getFddSenha()));
		}
		dto.montarURL();
		FonteDeDados fonte = fonteDeDadosConverter.toObject(dto);
		fonteDeDadosService.atualizar(fonte);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		FonteDeDados fonte = fonteDeDadosService.buscar(id);
		if (fonte == null) {
			return ResponseEntity.notFound().build();
		}
		fonteDeDadosService.excluir(fonte);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/teste")
	public ResponseEntity<RespostaTeste> testar(@Valid @RequestBody FonteDeDadosTeste teste) throws Exception {
		RespostaTeste resposta = new RespostaTeste();
		String senha = "";
		if (!teste.alterarSenha) {
			FonteDeDados fonte = fonteDeDadosService.buscar(teste.id);
			if (fonte == null) {
				ResponseEntity.notFound().build();
			}
			if (fonte.getFddSenha() != null) {
				senha = CryptoUtil.decodificar(fonte.getFddSenha());
			}
		} else if (teste.senha != null) {
			senha = teste.getSenha();
		}
		Connection con = null;
		String url = "jdbc:sqlserver://" + teste.getServidor() + ";databaseName=" + teste.getNomeBD() + ";"
				+ teste.getOpcoes();
		try {
			DriverManager.setLoginTimeout(5);
			con = DriverManager.getConnection(url, teste.getUsuario(), senha);
		} catch (SQLException e) {
			resposta.situacao = false;
			resposta.mensagem = e.getMessage();
			return ResponseEntity.ok(resposta);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					resposta.situacao = false;
					resposta.mensagem = ex.getMessage();
					return ResponseEntity.ok(resposta);
				}
			}
		}

		resposta.situacao = true;
		resposta.mensagem = "Tudo certo.";
		return ResponseEntity.ok(resposta);
	}

	@SuppressWarnings("unused")
	private static class RespostaTeste {
		boolean situacao;
		String mensagem;

		public boolean isSituacao() {
			return situacao;
		}

		public void setSituacao(boolean situacao) {
			this.situacao = situacao;
		}

		public String getMensagem() {
			return mensagem;
		}

		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
	}

	@SuppressWarnings("unused")
	private static class FonteDeDadosTeste {
		int id;
		String tipo;
		String servidor;
		String nomeBD;
		String usuario;
		String senha;
		boolean alterarSenha;
		String opcoes;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public String getServidor() {
			return servidor;
		}

		public void setServidor(String servidor) {
			this.servidor = servidor;
		}

		public String getNomeBD() {
			return nomeBD;
		}

		public void setNomeBD(String nomeBD) {
			this.nomeBD = nomeBD;
		}

		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		public String getSenha() {
			return senha;
		}

		public void setSenha(String senha) {
			this.senha = senha;
		}

		public boolean isAlterarSenha() {
			return alterarSenha;
		}

		public void setAlterarSenha(boolean alterarSenha) {
			this.alterarSenha = alterarSenha;
		}

		public String getOpcoes() {
			return opcoes;
		}

		public void setOpcoes(String opcoes) {
			this.opcoes = opcoes;
		}
	}
}