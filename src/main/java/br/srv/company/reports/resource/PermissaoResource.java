package br.srv.company.reports.resource;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/permissoes", produces = MediaType.APPLICATION_JSON_VALUE)
public class PermissaoResource {

  // FIXME: Refatorar quando for implementar a autenticação
  @GetMapping
  public ResponseEntity<List<String>> obterPermissoes() {
    return ResponseEntity.ok().body(List.of(""));
  }
}