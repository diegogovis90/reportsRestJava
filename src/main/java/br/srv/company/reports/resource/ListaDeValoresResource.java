package br.srv.company.reports.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.ItemDeListaConverter;
import br.srv.company.reports.converter.ListaDeValoresConverter;
import br.srv.company.reports.dto.ItemDeListaDto;
import br.srv.company.reports.dto.ListaDeValoresDto;
import br.srv.company.reports.model.ItemDeLista;
import br.srv.company.reports.model.ListaDeValores;
import br.srv.company.reports.service.ItemDeListaService;
import br.srv.company.reports.service.ListaDeValoresService;
import net.bytebuddy.implementation.bind.annotation.Empty;

@Transactional
@RestController
@RequestMapping("/listas-predefinidas")
public class ListaDeValoresResource {

	@Autowired
	ListaDeValoresService listaDeValoresService;
	@Autowired
	ListaDeValoresConverter listaDeValoresConverter;
	@Autowired
	ItemDeListaConverter itemDeListaConverter;
	@Autowired
	ItemDeListaService itemDeListaService;

	@GetMapping("")
	public ResponseEntity<ListasDeValores> listar(@RequestParam(value = "termo", required = false, defaultValue = "") @Empty String termo,
			@RequestParam(value = "indice", required = false) @Empty Integer indice,
			@RequestParam(value = "limite", required = false) @Empty Integer limite) {
		List<ListaDeValores> listas = listaDeValoresService.listar(termo);
		List<ListaDeValoresDto> itens = listaDeValoresConverter.toDto(listas);
		ListasDeValores resposta = new ListasDeValores();
		if (indice != null && limite != null) {
			int fim = (indice + limite);
			int lastIndex = (itens.size());
			resposta.setItens(itens.subList(indice, fim <= lastIndex ? fim : lastIndex));
		} else {
			resposta.setItens(itens);
		}
		resposta.setTotal(itens.size());

		return ResponseEntity.ok(resposta);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> obter(@PathVariable("id") int id) {
		ListaDeValores listaDeValores = listaDeValoresService.buscarComTotalItens(id);
		if (listaDeValores == null) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(listaDeValoresConverter.toDto(listaDeValores));
	}
	
	@GetMapping("/{id}/itens")
	public ResponseEntity<?> obterItens(@PathVariable("id") int id) {
		ListaDeValores listaDeValores = listaDeValoresService.buscar(id);
		if (listaDeValores == null) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(itemDeListaConverter.toDto(listaDeValores.getItens()));
	}
	
	@PostMapping
	public ResponseEntity<?> salvar(@Valid @RequestBody ListaDeValoresDto dto) throws Exception {
		ListaDeValores listaDeValores = listaDeValoresConverter.toObject(dto);
		listaDeValoresService.salvar(listaDeValores);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable("id") int id, @Valid @RequestBody ObjAtualizado obj) throws Exception {
		ListaDeValores listaDeValoresPersistido = listaDeValoresService.buscar(id);
		if (listaDeValoresPersistido == null) {
			return ResponseEntity.notFound().build();
		}
		ListaDeValoresDto dto = new ListaDeValoresDto();
		dto.setId(id);
		dto.setNome(obj.getNome());
		ListaDeValores listaDeValores = listaDeValoresConverter.toObject(dto);
		listaDeValoresService.atualizar(listaDeValores);
		
		for (Map.Entry<String, ItemDeListaDto> entrada : obj.getItens().entrySet()) {
			if(Integer.parseInt(entrada.getKey()) <= 0) {
				//adicionar item
				ItemDeLista item = itemDeListaConverter.toObject(entrada.getValue());
				Integer ultimaPosicao = itemDeListaService.ultimaPosicao(id);
				item.setIdlPosicao((ultimaPosicao == null ? 0 : ultimaPosicao+1));
				item.setListaDeValores(listaDeValores);
				itemDeListaService.salvar(item);
			}else if(entrada.getValue() != null && (entrada.getValue().getNome() != null || entrada.getValue().getValor() != null)) {
				//alterar item
				ItemDeLista itemPersistido = itemDeListaService.buscar(Integer.parseInt(entrada.getKey()));
				ItemDeLista itemAlteracao = new ItemDeLista();
				itemAlteracao.setIdlID(itemPersistido.getIdlID());
				itemAlteracao.setIdlPosicao(itemPersistido.getIdlPosicao());
				itemAlteracao.setListaDeValores(itemPersistido.getListaDeValores());
				itemAlteracao.setIdlNome(entrada.getValue().getNome() != null ? entrada.getValue().getNome() : itemPersistido.getIdlNome());
				itemAlteracao.setIdlValor(entrada.getValue().getValor() != null ? entrada.getValue().getValor() : itemPersistido.getIdlValor());
				itemDeListaService.atualizar(itemAlteracao);
			}else {
				//excluir item
				ItemDeLista itemPersistido = itemDeListaService.buscar(Integer.parseInt(entrada.getKey()));
				if (itemPersistido == null) {
					return ResponseEntity.notFound().build();
				}
				
				int posicao = itemPersistido.getIdlPosicao();
				itemDeListaService.excluir(itemPersistido);
				itemDeListaService.atualizarPosicoes(id, posicao);
			}
		}
	
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> apagar(@PathVariable("id") int id) throws Exception {
		ListaDeValores listaDeValores = listaDeValoresService.buscar(id);
		if (listaDeValores == null) {
			return ResponseEntity.notFound().build();
		}
		listaDeValoresService.excluir(listaDeValores);
		return ResponseEntity.noContent().build();
	}
	
	@SuppressWarnings("unused")
	private static class ListasDeValores {
		int total;
		List<ListaDeValoresDto> itens;
		
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public List<ListaDeValoresDto> getItens() {
			return itens;
		}
		public void setItens(List<ListaDeValoresDto> itens) {
			this.itens = itens;
		}
	}
	
	@SuppressWarnings("unused")
	private static class ObjAtualizado {
		String nome;
		HashMap<String, ItemDeListaDto> itens = new HashMap<String, ItemDeListaDto>();
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public HashMap<String, ItemDeListaDto> getItens() {
			return itens;
		}
		public void setItens(HashMap<String, ItemDeListaDto> itens) {
			this.itens = itens;
		}
		
	}
}