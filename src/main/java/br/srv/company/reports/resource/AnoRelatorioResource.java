package br.srv.company.reports.resource;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.srv.company.reports.converter.AnoRelatorioConverter;
import br.srv.company.reports.dao.JdbcDriverDao;
import br.srv.company.reports.model.AnoRelatorio;
import br.srv.company.reports.service.AnoRelatorioService;

@Transactional
@RestController
@RequestMapping("/anos")
public class AnoRelatorioResource {

	@Autowired
	AnoRelatorioService anoRelatorioService;
	@Autowired
	JdbcDriverDao jdbcDriverDao;
	@Autowired
	private AnoRelatorioConverter anoRelatorioConverter;

	@GetMapping("")
	public ResponseEntity<List<Integer>> listar() {
		List<AnoRelatorio> anos = (anoRelatorioService.buscarTodos());
		List<Integer> anosInt = anoRelatorioConverter.toDto(anos).stream().map(obj -> obj.getAno()).collect(Collectors.toList());
		return ResponseEntity.ok(anosInt);
	}
}