package br.srv.company.reports.conf;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import br.srv.company.reports.filter.CorsFilter;

@Configuration
public class ConfiguracaoBase {

	@Bean
	@ConditionalOnProperty("cors.enable")
	public FilterRegistrationBean<CorsFilter> corsFilter(){
	    FilterRegistrationBean<CorsFilter> registrationBean = new FilterRegistrationBean<>();
	    registrationBean.setFilter(new CorsFilter());
	    registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
	    return registrationBean;
	}
}
