package br.srv.company.reports.conf.oauth;

import static br.srv.company.reports.conf.oauth.Oauth2Constantes.RESOURCE_ID;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

// @Configuration
// @EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {
  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private ClientDetailsService clientDetailsService;
  @Value("${jwt.signingkey}")
  private String jwtSigningKey;
  @Value("${jwt.validadeToken}")
  private Integer validadeToken;

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    DefaultOAuth2RequestFactory requestFactory = new DefaultOAuth2RequestFactory(clientDetailsService);

    TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
    tokenEnhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter(), new UserInfoTokenEnhancer()));

    endpoints.authenticationManager(authenticationManager);
    endpoints.tokenEnhancer(tokenEnhancerChain);
    endpoints.requestFactory(requestFactory);
    endpoints.tokenStore(jwtTokenStore());
    endpoints.accessTokenConverter(accessTokenConverter());
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    String idCliente = "cliente-app";
    String senhaCliente = "{noop}123456";
    clients.inMemory().withClient(idCliente).secret(senhaCliente).authorizedGrantTypes("password")
        .scopes("read", "write").accessTokenValiditySeconds(validadeToken).resourceIds(RESOURCE_ID);
  }

  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    converter.setSigningKey(jwtSigningKey);
    return converter;
  }

  @Bean
  public JwtTokenStore jwtTokenStore() {
    return new JwtTokenStore(accessTokenConverter());
  }

  @Bean
  @Primary
  public DefaultTokenServices tokenServices() {
    DefaultTokenServices tokenServices = new DefaultTokenServices();
    tokenServices.setTokenStore(jwtTokenStore());
    tokenServices.setSupportRefreshToken(true);
    return tokenServices;
  }

  private static class UserInfoTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
      final Map<String, Object> additionalInfo = new HashMap<>();

      ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

      return accessToken;
    }

  }

}
