package br.srv.company.reports.conf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ResourceOwner implements UserDetails {
	private static final long serialVersionUID = 1L;
	private String cpf;
	private String nome;
	private String nomeReduzido;
	private String cargo;
	private String urlacesso;
	private String senha = "{noop}reports";
	private Collection<String> roles;
	Collection<GrantedAuthority> authorities = new ArrayList<>();

	public ResourceOwner(String cpf, String nome,  String nomeReduzido, String cargo, String urlacesso, Collection<String> roles) {
		this.nome = nome;
		this.nomeReduzido = nomeReduzido;
		this.cpf = cpf;
		this.cargo = cargo;
		this.urlacesso = urlacesso;
		this.roles = Collections.unmodifiableCollection(roles);
		this.roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.toString())));
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return cpf;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	public Collection<String> getRoles() {
		return roles;
	}

	public String getNome() {
		return nome;
	}

	public String getCargo() {
		return cargo;
	}

	public String getUrlacesso() {
		return urlacesso;
	}

	public String getNomeReduzido() {
		return nomeReduzido;
	}
	
}
