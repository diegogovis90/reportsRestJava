package br.srv.company.reports.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.srv.company.reports.dao.GenericJPADao;
import br.srv.company.reports.dao.RelatorioModeloDao;
import br.srv.company.reports.model.Imagem;
import br.srv.company.reports.model.RelatorioModelo;

@Service
public class RelatorioModeloService extends BaseService<RelatorioModelo> {

  @Autowired
  private RelatorioModeloDao relatorioDao;

  @Override
  public GenericJPADao<RelatorioModelo> getDao() {
    return relatorioDao;
  }

  public List<Imagem> obterImagens(int relID){
	  return relatorioDao.obterImagens(relID);
  }
}
