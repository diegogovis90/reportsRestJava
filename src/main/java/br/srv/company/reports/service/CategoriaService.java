package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.converter.CategoriaConverter;
import br.srv.company.reports.dao.CategoriaDao;
import br.srv.company.reports.dto.CategoriaDto;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.Categoria;

@Service
@Transactional
public class CategoriaService extends BaseService<Categoria> {

	@Autowired
	private CategoriaDao categoriaDao;
	@Autowired
	private CategoriaConverter catConverter;

	@Override
	public CategoriaDao getDao() {
		return categoriaDao;
	}

	@Override
	public void excluir(Categoria categoria) throws Exception {
		Objects.requireNonNull(categoria);
		if (categoria.getRelatorios().size() > 0) {
			throw new BusinessException(
					"Categoria está sendo utilizada por " + categoria.getRelatorios().size() + " relatório(s)");
		}
		getDao().excluir(categoria);
	}

	public List<CategoriaDto> listarPorAno(Integer ano) {
		// TODO: Somente retornar as categorias que possuem relatórios aos quais o
		// usuário tem
		// acesso (pelo local e pela tabela de permissões)
		List<CategoriaDto> dtos = catConverter.toDto(getDao().listarPorAno(ano));
		if (getDao().existeSemCategoria(ano)) {
			CategoriaDto semCat = new CategoriaDto();
			semCat.setNome("Sem Categoria");
			semCat.setId(0);
			semCat.setOrdenacao(0);
			dtos.add(semCat);
		}
		return dtos;

	}

	public void removerRelatorio(Integer catID, Integer relID) {
		categoriaDao.removerRelatorio(catID, relID);
	}

	public void adicionarRelatorio(Integer catID, Integer relID) {
		categoriaDao.adicionarRelatorio(catID, relID);
	}
}