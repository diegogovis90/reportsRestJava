package br.srv.company.reports.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;

import br.srv.company.reports.dao.RelatorioDao;
import br.srv.company.reports.dao.RelatorioModeloDao;
import br.srv.company.reports.util.CryptoUtil;
import br.srv.company.reports.util.ResponseUtil;
import br.srv.company.reports.dto.FiltroRelatorioDto;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.geracao.FonteDeDadosGeracao;
import br.srv.company.reports.geracao.GeracaoHelper;
import br.srv.company.reports.geracao.ParametroGeracao;
import br.srv.company.reports.geracao.RelatorioExportado;
import br.srv.company.reports.model.Relatorio;
import br.srv.company.reports.model.RelatorioModelo;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class GeracaoRelatorioService {
  private final String PASTA_TEMPORARIA = "tmp";

  @Autowired
  private RelatorioDao relatorioDao;
  @Autowired
  private RelatorioModeloDao relModeloDao;

  public RelatorioExportado gerarArquivo(Integer id, String tipo, List<FiltroRelatorioDto> params)
      throws BusinessException {
    try {

      Relatorio rel = relatorioDao.buscar(id);
      RelatorioModelo relModelo = relModeloDao.buscar(id);
      byte[] dadosModelo;

      switch (tipo) {
        case "xlsx":
        case "xls":
        case "ods":
          dadosModelo = relModelo.getModelo().getModDados2();
          break;
        default:
          dadosModelo = relModelo.getModelo().getModDados();
      }
      String senha = CryptoUtil.decodificar(relModelo.getFonte().getFddSenha());

      FonteDeDadosGeracao fonte = new FonteDeDadosGeracao(relModelo.getFonte().getFddUrl(),
          relModelo.getFonte().getFddUsuario(), senha);

      List<byte[]> imagens = relModelo.getImagens().stream().map(img -> img.getImagem().getImaDados())
          .collect(Collectors.toList());
      List<ParametroGeracao> parametros = new ArrayList<>();
      for (FiltroRelatorioDto filtro : params) {
        parametros
            .add(new ParametroGeracao(filtro.getNome(), filtro.getTipo(), filtro.getVisivel(), 
            		!filtro.getTipo().equals("java.lang.Boolean") ? filtro.getValor() : !filtro.getValor().equals("")
            		, ""));
      }

      JasperPrint print = GeracaoHelper.obterJasperPrint(dadosModelo, fonte, imagens, parametros);
      String tempFile = GeracaoHelper.export(print, tipo, PASTA_TEMPORARIA); // TODO: Implementar configuração e limpeza
                                                                             // da pasta temporária

      return new RelatorioExportado(new File(tempFile), rel.getRelNome());
    } catch (Exception e) {
      // FIXME: Refatorar exceções com a estrutura dos erros do projeto em Go
      e.printStackTrace();
      throw new BusinessException("Erro inesperado");
    }

  }

  public void gerarParaDownload(HttpServletResponse response, Integer id, String tipo, List<FiltroRelatorioDto> params)
      throws BusinessException {
    RelatorioExportado exp = gerarArquivo(id, tipo, params);
    prepararDownload(response, exp.getArquivo(), exp.getNomeRel(), tipo);
  }

  public void baixarTemporario(HttpServletResponse response, String arquivo) throws BusinessException {
    File f = new File(PASTA_TEMPORARIA + "/" + arquivo);
    String ext = Files.getFileExtension(arquivo);
    prepararDownload(response, f, arquivo, ext);
  }

  private void prepararDownload(HttpServletResponse response, File arquivo, String nome, String tipo)
      throws BusinessException {
    try (BufferedInputStream buf = new BufferedInputStream(new FileInputStream(arquivo))) {
      response.setHeader("Content-Type", ResponseUtil.obterContentType(tipo));
      response.setHeader("Content-Disposition", "attachment; filename=\"" + nome + "." + tipo + "\"");
      // Caso haja problema com caracteres especiais, usar também:
      // filename*=utf-8''" + URLEncoder.encode(nome,
      // StandardCharsets.UTF_8.toString()) + "." + tipo);

      IOUtils.copy(buf, response.getOutputStream());
      response.flushBuffer();
    } catch (FileNotFoundException e) {
      throw new BusinessException("Arquivo não encontrado");
    } catch (ClientAbortException e) {
    } catch (Exception e) {
      // FIXME: Refatorar exceções com a estrutura dos erros do projeto em Go
      e.printStackTrace();
      throw new BusinessException("Erro ao gerar o download");
    }
  }
}