package br.srv.company.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.PropriedadeControleDao;
import br.srv.company.reports.model.PropriedadeControle;

@Service
@Transactional
public class PropriedadeControleService extends BaseService<PropriedadeControle> {

  @Autowired
  private PropriedadeControleDao pcoDao;

  @Override
  public PropriedadeControleDao getDao() {
    return pcoDao;
  }
}