package br.srv.company.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.srv.company.reports.dao.GenericJPADao;
import br.srv.company.reports.dao.RelatoriosModelosImagensDao;
import br.srv.company.reports.model.RelatoriosModelosImagens;

@Service
public class RelatoriosModelosImagensService extends BaseService<RelatoriosModelosImagens> {

  @Autowired
  private RelatoriosModelosImagensDao relatorioDao;

  @Override
  public GenericJPADao<RelatoriosModelosImagens> getDao() {
    return relatorioDao;
  }
}
