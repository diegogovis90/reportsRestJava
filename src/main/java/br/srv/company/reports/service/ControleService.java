package br.srv.company.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ControleDao;
import br.srv.company.reports.model.Controle;

@Service
@Transactional
public class ControleService extends BaseService<Controle> {

  @Autowired
  private ControleDao controleDao;

  @Override
  public ControleDao getDao() {
    return controleDao;
  }

}