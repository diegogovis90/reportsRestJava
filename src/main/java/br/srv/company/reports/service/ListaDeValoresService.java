package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ListaDeValoresDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.ListaDeValores;

@Service
@Transactional
public class ListaDeValoresService extends BaseService<ListaDeValores> {

	@Autowired
	private ListaDeValoresDao listaDeValoresDao;

	@Override
	public ListaDeValoresDao getDao() {
		return listaDeValoresDao;
	}

	@Override
	public void excluir(ListaDeValores lista) throws Exception {
		Objects.requireNonNull(lista);
		if (lista.getParametros().size() > 0) {
			throw new BusinessException("Lista de Valores está sendo utilizada por " + lista.getParametros().size() + " parâmetros");
		}
		getDao().excluir(lista);
	}

	public List<ListaDeValores> listar(String termo) {
		return listaDeValoresDao.listar(termo);
	}

	public ListaDeValores buscarComTotalItens(int id) {
		return listaDeValoresDao.buscarComTotalItens(id);
	}
}