package br.srv.company.reports.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.AnosRelatoriosDao;
import br.srv.company.reports.model.AnosRelatorios;

@Service
@Transactional
public class AnosRelatoriosService extends BaseService<AnosRelatorios> {

  @Autowired
  private AnosRelatoriosDao anoDao;

  @Override
  public AnosRelatoriosDao getDao() {
    return anoDao;
  }
  
  public List<AnosRelatorios> obterAnos(int relID){
	  return anoDao.obterAnos(relID);
  }
}