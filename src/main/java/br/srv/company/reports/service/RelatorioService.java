package br.srv.company.reports.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.srv.company.reports.converter.RelatorioConverter;
import br.srv.company.reports.dao.AnosRelatoriosDao;
import br.srv.company.reports.dao.ConsultaDao;
import br.srv.company.reports.dao.GenericJPADao;
import br.srv.company.reports.dao.ItemDeListaDao;
import br.srv.company.reports.dao.ParametroDao;
import br.srv.company.reports.dao.RelatorioDao;
import br.srv.company.reports.dto.FiltroRelatorioDto;
import br.srv.company.reports.dto.RelatorioDto;
import br.srv.company.reports.dto.RelatorioParametrosDto;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.AnosRelatorios;
import br.srv.company.reports.model.Parametro;
import br.srv.company.reports.model.Relatorio;
import br.srv.company.reports.util.ParametroUtil;

@Service
public class RelatorioService extends BaseService<Relatorio> {

  @Autowired
  private RelatorioDao relatorioDao;
  @Autowired
  private RelatorioConverter relatorioConverter;
  @Autowired
  private ParametroDao parametroDao;
  @Autowired
  private ItemDeListaDao itemListaDao;
  @Autowired
  private AnosRelatoriosDao anosRelatoriosDao;
  @Autowired
  private ConsultaDao consultaDao;

  @Override
  public GenericJPADao<Relatorio> getDao() {
    return relatorioDao;
  }

  public List<RelatorioDto> listar(Integer ano, Integer categoria) {
    // TODO: Somente retornar relatórios aos quais o usuário tem acesso (pelo local
    // e pela tabela de permissões)

    return relatorioConverter.toDto(relatorioDao.listarPorAnoCategoria(ano, categoria));
  }
  
  public List<Relatorio> listarTodos(int ano, String termo) {
	  return relatorioDao.listarTodos(ano, termo);
  }

  public RelatorioDto obterComAcessoPorAno(Integer id, Integer ano) {
    // FIXME: Refatorar para verificar permissão do usuário ao relatório através do
    // local
    return relatorioConverter.toDto(relatorioDao.obterPorAno(id, ano));
  }

  public RelatorioParametrosDto obterParametros(Integer id, Map<String, String[]> valores) throws BusinessException {
    try {

      Relatorio rel = relatorioDao.buscar(id);
      List<Parametro> parametrosBanco = parametroDao.obterParametrosDoRelatorio(id);

      List<FiltroRelatorioDto> parametrosRelatorio = new ArrayList<FiltroRelatorioDto>();
      for (Parametro par : parametrosBanco) {
        FiltroRelatorioDto rp = new FiltroRelatorioDto();
        rp.setNome(par.getParNome());
        rp.setObrigatorio(par.getControle().getConObrigatorio());
        rp.setRotulo(par.getControle().getConRotulo());
        rp.setTipo(par.getParTipoClasse());
        rp.setControle(par.getParTipoControle());
        rp.setVisivel(par.getControle().getConVisivel());

        if (valores.containsKey(par.getParNome())) {
          rp.setValor(ParametroUtil.obterValorFiltro(par, valores.get(par.getParNome())));
        } else if (par.getParValor() != null) {
          rp.setValor(ParametroUtil.obterValorFiltro(par, new String[] { par.getParValor() }));
        }
        parametrosRelatorio.add(rp);
      }

      for (int i = 0; i < parametrosBanco.size(); i++) {
        Parametro pb = parametrosBanco.get(i);
        switch (pb.getParTipoPropriedade()) {
          case 1:
            parametrosRelatorio.get(i)
                .setOpcoes(itemListaDao.obterOpcoesPreDefinidas(pb.getPropriedadeControle().getPcoID()));
            break;
          case 2:
            String consulta = ParametroUtil.substituirLocais(consultaDao.buscar(pb.getPropriedadeControle().getPcoID()).getCosSql(), pb);
            consulta = ParametroUtil.substituirParametros(consulta, parametrosRelatorio);
            parametrosRelatorio.get(i).setOpcoes(ParametroUtil.opcoesPorConsulta(pb, consulta));
            break;
        }
      }

      RelatorioParametrosDto resp = new RelatorioParametrosDto();
      resp.setId(rel.getRelID());
      resp.setNome(rel.getRelCodigo() + " - " + rel.getRelNome());
      resp.setCategoria(rel.getCategoria() == null ? "Sem Categoria"
          : rel.getCategoria().getCatCodigo() + " - " + rel.getCategoria().getCatNome());
      resp.setParametros(parametrosRelatorio);
      resp.setTemNotaExplicativa(false); // TODO: Nota explicativa
      resp.setMensagem(rel.getRelMensagem());
      return resp;
    } catch (Exception e) {
      // TODO: Log de erro
      e.printStackTrace();
      throw new BusinessException("Erro inesperado");
    }
  }

  public List<Integer> listarAnos() {
    // TODO: Somente retornar os anos que possuem relatórios aos quais o usuário tem
    // acesso (pelo local e pela tabela de permissões)
    List<AnosRelatorios> anosBanco = anosRelatoriosDao.buscarTodos();
    return anosBanco.stream().map(a -> a.getAno().getAreAno()).distinct().collect(Collectors.toList());
  }
  
  public List<Relatorio> listarPorCategoria(int ano, int catID, String termo, int tipoMarcacao) {
	  return relatorioDao.listarPorCategoria(ano, catID, termo, tipoMarcacao);
  }
  
  public List<Relatorio> listarAlteracaoMarcacao(int ano, int catID, String termo, int tipoMarcacao) {
	  return relatorioDao.listarAlteracaoMarcacao(ano, catID, termo, tipoMarcacao);
  }
  
  public List<Relatorio> listarPorLocal(int ano, int locID, String termo, int tipoMarcacao) {
	  return relatorioDao.listarPorLocal(ano, locID, termo, tipoMarcacao);
  }
}
