package br.srv.company.reports.service;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import br.srv.company.reports.dao.GenericJPADao;
import br.srv.company.reports.pesquisa.Pesquisa;

public abstract class BaseService<T extends Serializable> {

  public abstract GenericJPADao<T> getDao();

  public T buscar(int id) {
    return getDao().buscar(id);
  }

  public List<T> buscarTodos() {
    return getDao().buscarTodos();
  }

  public void salvar(T entity) throws Exception {
    Objects.requireNonNull(entity);
    getDao().salvar(entity);
  }

  public T atualizar(T entity) throws Exception {
    Objects.requireNonNull(entity);
    return getDao().atualizar(entity);
  }

  public void excluir(T entity) throws Exception {
    Objects.requireNonNull(entity);
    getDao().excluir(entity);
  }

  public List<T> pesquisar(Pesquisa pesquisa) {
    Objects.requireNonNull(pesquisa);
    return getDao().pesquisar(pesquisa);
  }

  public Long totalRegistros(Pesquisa pesquisa) {
    Objects.requireNonNull(pesquisa);
    return getDao().totalRegistros(pesquisa.getFiltros(), pesquisa.isDistinct());
  }
}
