package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ConsultaDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.Consulta;

@Service
@Transactional
public class ConsultaService extends BaseService<Consulta> {

	@Autowired
	private ConsultaDao consultaDao;

	@Override
	public ConsultaDao getDao() {
		return consultaDao;
	}

	@Override
	public void excluir(Consulta consulta) throws Exception {
		Objects.requireNonNull(consulta);
		if (consulta.getParametros().size() > 0) {
			throw new BusinessException(
					"Consulta está sendo utilizada por " + consulta.getParametros().size() + " parâmetros");
		}
		getDao().excluir(consulta);
	}

	public List<Consulta> listar(String termo) {
		return consultaDao.listar(termo);
	}
}