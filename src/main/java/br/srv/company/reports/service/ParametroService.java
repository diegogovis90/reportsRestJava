package br.srv.company.reports.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ParametroDao;
import br.srv.company.reports.model.Parametro;

@Service
@Transactional
public class ParametroService extends BaseService<Parametro> {

  @Autowired
  private ParametroDao parametroDao;

  @Override
  public ParametroDao getDao() {
    return parametroDao;
  }

  public List<Parametro> obterParametrosDoRelatorio(Integer relID){
	  return parametroDao.obterParametrosDoRelatorio(relID);
  }
}