package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.LocalDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.Local;

@Service
@Transactional
public class LocalService extends BaseService<Local> {

	@Autowired
	private LocalDao localDao;

	@Override
	public LocalDao getDao() {
		return localDao;
	}

	@Override
	public void salvar(Local local) throws Exception {
		Objects.requireNonNull(local);
		getDao().salvar(local);
	}

	@Override
	public Local atualizar(Local local) throws Exception {
		Objects.requireNonNull(local);
		return getDao().atualizar(local);
	}

	@Override
	public void excluir(Local local) throws Exception {
		Objects.requireNonNull(local);
		if (local.getRelatorios().size() > 0) {
			throw new BusinessException(
					"Local está sendo utilizado por " + local.getRelatorios().size() + " relatório(s)");
		}
		getDao().excluir(local);
	}

	public List<Local> listar(String termo) {
		List<Local> locals = getDao().listar(termo);
		return locals;
	}

	public void removerRelatorio(Integer locID, Integer relID) {
		localDao.removerRelatorio(locID, relID);
	}

	public void adicionarRelatorio(Integer locID, Integer relID) {
		localDao.adicionarRelatorio(locID, relID);
	}
}