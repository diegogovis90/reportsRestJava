package br.srv.company.reports.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ItemDeListaDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.ItemDeLista;

@Service
@Transactional
public class ItemDeListaService extends BaseService<ItemDeLista> {

	@Autowired
	private ItemDeListaDao itemDeListaDao;

	@Override
	public ItemDeListaDao getDao() {
		return itemDeListaDao;
	}

	@Override
	public ItemDeLista atualizar(ItemDeLista itemDeLista) throws Exception {
		Objects.requireNonNull(itemDeLista);
		if(itemDeListaDao.existeNome(itemDeLista.getListaDeValores().getPcoID(), itemDeLista.getIdlNome(), itemDeLista.getIdlID())) {
			throw new BusinessException("Já existe um item com o nome "+itemDeLista.getIdlNome());
		}
	    return getDao().atualizar(itemDeLista);
	}

	public Integer ultimaPosicao(int pcoID) {
		return itemDeListaDao.ultimaPosicao(pcoID);
	}

	public void atualizarPosicoes(int pcoID, int posicao) {
		itemDeListaDao.atualizarPosicoes(pcoID, posicao);
	}
}