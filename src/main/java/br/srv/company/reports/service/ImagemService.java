package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ImagemDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.Imagem;

@Service
@Transactional
public class ImagemService extends BaseService<Imagem> {

	@Autowired
	private ImagemDao imgDao;

	@Override
	public ImagemDao getDao() {
		return imgDao;
	}
	
	@Override
	public void excluir(Imagem img) throws BusinessException {
		Objects.requireNonNull(img);
		if(img.getRelatoriosModelosImagens().size() > 0) {
			throw new BusinessException("Imagem está sendo utilizada por "+img.getRelatoriosModelosImagens().size()+" relatórios");
		}
		imgDao.excluir(img);
	}
	
	public List<Imagem> listar(String termo, List<Integer> exclusoes) {
		return imgDao.listar(termo, exclusoes);
	}
}