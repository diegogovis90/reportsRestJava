package br.srv.company.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.srv.company.reports.dao.GenericJPADao;
import br.srv.company.reports.dao.RelatorioArquivoDao;
import br.srv.company.reports.model.RelatorioArquivo;

@Service
public class RelatorioArquivoService extends BaseService<RelatorioArquivo> {

  @Autowired
  private RelatorioArquivoDao relatorioDao;

  @Override
  public GenericJPADao<RelatorioArquivo> getDao() {
    return relatorioDao;
  }
}
