package br.srv.company.reports.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.FonteDeDadosDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.FonteDeDados;
import br.srv.company.reports.util.CryptoUtil;

@Service
@Transactional
public class FonteDeDadosService extends BaseService<FonteDeDados> {

	@Autowired
	private FonteDeDadosDao fonteDeDadosDao;

	@Override
	public FonteDeDadosDao getDao() {
		return fonteDeDadosDao;
	}

	@Override
	public void salvar(FonteDeDados fonte) throws Exception {
		Objects.requireNonNull(fonte);
		validar(fonte);
		getDao().salvar(fonte);
	}

	@Override
	public FonteDeDados atualizar(FonteDeDados fonte) throws Exception {
		Objects.requireNonNull(fonte);
		validar(fonte);
		return getDao().atualizar(fonte);
	}
	
	@Override
	public void excluir(FonteDeDados fonte) throws Exception {
		Objects.requireNonNull(fonte);
		long utilizacao = fonte.getConsultas().size() + fonte.getRelatorios().size() + fonte.getRelatoriosUsu().size();
		if (utilizacao > 0) {
			throw new BusinessException("Fonte de dados está sendo utilizada por " + utilizacao + " entidades");
		}
		getDao().excluir(fonte);
	}

	public List<FonteDeDados> listar() {
		List<FonteDeDados> fontes = getDao().listar();
		return fontes;
	}

	private void validar(FonteDeDados fonte) throws Exception {
		if (fonte.getFddServidor() == null || fonte.getFddServidor().trim().equals("")) {
			throw new BusinessException("Campo Servidor é obrigatório");
		}
		if (fonte.getFddNomeBD() == null || fonte.getFddNomeBD().trim().equals("")) {
			throw new BusinessException("Campo Banco de dados é obrigatório");
		}
		if (fonte.getFddUsuario() == null || fonte.getFddUsuario().trim().equals("")) {
			throw new BusinessException("Campo Usuário é obrigatório");
		}
		if (fonte.getFddSenha() == null || CryptoUtil.decodificar(fonte.getFddSenha()).trim().equals("")) {
			throw new BusinessException("Campo Senha é obrigatório");
		}
	}

}