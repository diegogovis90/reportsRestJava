package br.srv.company.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.AnoRelatorioDao;
import br.srv.company.reports.model.AnoRelatorio;

@Service
@Transactional
public class AnoRelatorioService extends BaseService<AnoRelatorio> {

  @Autowired
  private AnoRelatorioDao anoDao;

  @Override
  public AnoRelatorioDao getDao() {
    return anoDao;
  }
}