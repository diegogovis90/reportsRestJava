package br.srv.company.reports.service;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.srv.company.reports.dao.ModeloDao;
import br.srv.company.reports.exceptions.BusinessException;
import br.srv.company.reports.model.Modelo;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Service
@Transactional
public class ModeloService extends BaseService<Modelo> {

	@Autowired
	private ModeloDao modeloDao;

	@Override
	public ModeloDao getDao() {
		return modeloDao;
	}

	@Override
	public void salvar(Modelo modelo) throws Exception {
		Objects.requireNonNull(modelo);
		validar(modelo);
		getDao().salvar(modelo);
	}

	@Override
	public Modelo atualizar(Modelo modelo) throws Exception {
		Objects.requireNonNull(modelo);
		validar(modelo);
		return getDao().atualizar(modelo);
	}
	
	@Override
	public void excluir(Modelo modelo) throws Exception {
		Objects.requireNonNull(modelo);
		if(modelo.getRelatorios().size() > 0) {
			throw new BusinessException("Modelo está sendo utilizado por "+modelo.getRelatorios().size()+" relatório(s)");
		}
		getDao().excluir(modelo);
	}

	public List<Modelo> listar(String termo) {
		List<Modelo> modelos = getDao().listar(termo);
		return modelos;
	}

	private void validar(Modelo modelo) throws Exception {
		if (getDao().existeNome(modelo.getModNome(), modelo.getModID())) {
			throw new BusinessException("Já existe um modelo com o mesmo nome.");
		}
		
		if(modelo.getModDados2() != null) {
			JasperReport modelReport = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modelo.getModDados()));
	        JasperReport model2Report = (JasperReport) JRLoader.loadObject(new ByteArrayInputStream(modelo.getModDados2()));
	        
	        JRParameter[] modelParam = modelReport.getParameters();
	        JRParameter[] model2Param = model2Report.getParameters();
	        
	        String modelValor = "";
	        String model2Valor = "";
	        
	        for(JRParameter jr : modelParam){
	            modelValor += jr.getName();
	        }
	        for(JRParameter jr : model2Param){
	            model2Valor += jr.getName();
	        }
	        
	        if (!modelValor.equals(model2Valor)) {
	        	throw new BusinessException("Parametros diferentes! Selecione um modelo tabulado com os mesmos parametros do modelo principal.");
	        }
		}
	}
}