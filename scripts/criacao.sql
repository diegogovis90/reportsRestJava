CREATE TABLE AnoRelatorio
(
    areAno smallint NOT NULL PRIMARY KEY
);

CREATE TABLE Categoria
(
    catID int IDENTITY PRIMARY KEY NOT NULL,
    catCodigo varchar(20) NOT NULL,
    catNome varchar(45) NOT NULL,
    catOrdenacao int NOT NULL
);

CREATE TABLE Relatorio
(
    relID bigint IDENTITY PRIMARY KEY NOT NULL,
    catID int REFERENCES Categoria(catID),
    relNome varchar(250) NOT NULL,
    relTipo varchar(100) NOT NULL,
    relMensagem varchar(250) NOT NULL DEFAULT (''),
    relCodigo varchar(20) NOT NULL,
    relOrdenacao int NOT NULL,
    relReferencia bigint REFERENCES Relatorio(relID)
);

CREATE TABLE NotaExplicativa
(
    nteID bigint IDENTITY PRIMARY KEY NOT NULL,
    relID bigint NOT NULL REFERENCES Relatorio(relID) ON DELETE CASCADE,
    nteSequencia int NOT NULL,
    nteTexto varchar(1000) NOT NULL,
    nteAtiva BIT NOT NULL
);

CREATE TABLE CampoInformativo
(
    cinID bigint IDENTITY PRIMARY KEY NOT NULL,
    relID bigint NOT NULL REFERENCES Relatorio(relID) ON DELETE CASCADE,
    cinSequencia int NOT NULL,
    cinTexto varchar(1000),
    cinAtivo BIT NOT NULL
);

CREATE TABLE RelatorioArquivo
(
    relID bigint PRIMARY KEY NOT NULL REFERENCES Relatorio(relID),
    rarDados varbinary(max) NOT NULL,
    rarFormato varchar(100) NOT NULL
);

CREATE TABLE JdbcDriver -- TODO: remover
(
    jdrID int IDENTITY PRIMARY KEY NOT NULL,
    jdrNome varchar(45) NOT NULL,
    jdrClasse varchar(255) NOT NULL
);

CREATE TABLE FonteDeDados
(
    fddID int IDENTITY PRIMARY KEY NOT NULL,
    jdrID int REFERENCES JdbcDriver(jdrID), -- TODO: remover
    fddTipo varchar(16) NOT NULL DEFAULT 'sqlserver-jdbc', -- TODO: remover DEFAULT
    fddNome varchar(45) NOT NULL,
    fddServidor varchar(128),
    fddNomeBD varchar(30),
    fddUrl varchar(200) NOT NULL, -- TODO: remover
    fddUsuario varchar(60),
    fddSenha varchar(100),
    fddOpcoes varchar(128)
);

CREATE TABLE RelatorioUsuario
(
    relID bigint NOT NULL PRIMARY KEY REFERENCES Relatorio(relID),
    rusExercicio smallint not null,
    fddID int NOT NULL REFERENCES FonteDeDados(fddID),
    rusUsuario varchar(14) NOT NULL,
    rusSql varchar(max) NOT NULL
);

CREATE TABLE RelatorioPermissao
(
    repID bigint IDENTITY PRIMARY KEY,
    repUsuario varchar(14) NOT NULL,
    relID bigint NOT NULL REFERENCES Relatorio(relID),
    UNIQUE(repUsuario, relID)
);

CREATE TABLE ColunaRelatorio
(
    colID bigint IDENTITY PRIMARY KEY NOT NULL,
    relID bigint NOT NULL REFERENCES RelatorioUsuario(relID),
    colNome varchar(45) NOT NULL,
    colRotulo varchar(60),
    colRotuloPersonalizado varchar(60),
    colVisivel bit NOT NULL,
    colPosicao int NOT NULL,
    colTipo varchar(100) NOT NULL
);

CREATE TABLE AnosRelatorios
(
    areID bigint IDENTITY PRIMARY KEY,
    areAno smallint NOT NULL REFERENCES AnoRelatorio(areAno),
    relID bigint NOT NULL REFERENCES Relatorio(relID),
    UNIQUE(areAno, relID)
);

CREATE TABLE Local
(
    locID int IDENTITY PRIMARY KEY NOT NULL,
    locCodigo varchar(100) NOT NULL,
    locNome varchar(100) NOT NULL
);

CREATE TABLE LocaisRelatorios
(
    lreID bigint IDENTITY PRIMARY KEY,
    relID bigint NOT NULL REFERENCES Relatorio(relID),
    locID int NOT NULL REFERENCES Local(locID),
    UNIQUE(relID, locID)
);

CREATE TABLE ConfigControle
(
    ccoClasse varchar(100) PRIMARY KEY NOT NULL,
    ccoControle int NOT NULL
);


CREATE TABLE PropriedadeControle
(
    pcoID int IDENTITY PRIMARY KEY NOT NULL,
    pcoNome varchar(250) NOT NULL,
    pcoTipo varchar(100) NOT NULL -- pcoTipo: ListaDeValores ou Consulta
);

CREATE TABLE ItemDeLista
(
    idlID bigint IDENTITY PRIMARY KEY NOT NULL,
    pcoID int REFERENCES PropriedadeControle(pcoID),
    idlNome varchar(45) NOT NULL,
    idlValor varchar(255),
    idlPosicao bigint NOT NULL
);

CREATE TABLE ListaDeValores
(
    pcoID int PRIMARY KEY NOT NULL REFERENCES PropriedadeControle(pcoID)
);

CREATE TABLE Consulta
(
    pcoID int PRIMARY KEY NOT NULL REFERENCES PropriedadeControle(pcoID),
    fddID int NOT NULL REFERENCES FonteDeDados(fddID),
    cosSql text NOT NULL,
    cosColunasExibicao text NOT NULL,
    cosColunaValor varchar(100) NOT NULL
);

CREATE TABLE Controle
(
    conID int IDENTITY PRIMARY KEY NOT NULL,
    conNome varchar(45) NOT NULL,
    conRotulo varchar(60),
    conObrigatorio bit NOT NULL,
    conSomenteLeitura bit NOT NULL,
    conVisivel bit NOT NULL,
    conAtualizarOutrosParametros bit
);

CREATE TABLE Imagem
(
    imaID int IDENTITY PRIMARY KEY NOT NULL,
    imaNome varchar(45) NOT NULL,
    imaFormato varchar(100) NOT NULL,
    imaDados image NOT NULL,
    imaData datetimeoffset NOT NULL
);

CREATE TABLE Modelo
(
    modID int IDENTITY PRIMARY KEY NOT NULL,
    modNome varchar(100),
    modNomeArquivo varchar(100) NOT NULL DEFAULT '', -- TODO: remover DEFAULT
    modDados varbinary(max) NOT NULL, -- TODO: renomear para modConteudoArquivo
    modDataEnvioArquivo datetimeoffset NOT NULL DEFAULT CURRENT_TIMESTAMP, -- TODO: remover DEFAULT
    modDataModifcacaoArquivo datetimeoffset NOT NULL DEFAULT CURRENT_TIMESTAMP, -- TODO: remover DEFAULT
    modCPFModificacaoArquivo varchar(14) NOT NULL DEFAULT '11111111111', -- TODO: remover DEFAULT
    modNomeArquivoTabulado varchar(100),
    modDados2 varbinary(max), -- TODO: renomear para modConteudoArquivoTabulado
    modDataEnvioArquivoTabulado datetimeoffset,
    modDataModificacaoArquivoTabulado datetimeoffset,
    modCPFModificacaoArquivoTabulado varchar(14),
    modCPFCriacao varchar(14) NOT NULL DEFAULT '11111111111', -- TODO: remover DEFAULT
    modCPFModificacao varchar(14) NOT NULL DEFAULT '11111111111', -- TODO: remover DEFAULT
    modData datetimeoffset NOT NULL, -- data de criação do modelo. TODO: renomear para modDataCriacao
    modDataModificacao datetimeoffset NOT NULL DEFAULT CURRENT_TIMESTAMP, -- TODO: remover DEFAULT
);

CREATE TABLE RelatorioModelo
(
    relID bigint PRIMARY KEY NOT NULL REFERENCES Relatorio(relID),
    modID int REFERENCES Modelo(modID),
    fddID int REFERENCES FonteDeDados(fddID),
    rmoPersonalizavel bit NOT NULL DEFAULT (0)
);

CREATE TABLE RelatoriosModelosImagens
(
    rmiID bigint IDENTITY PRIMARY KEY,
    relID bigint NOT NULL REFERENCES RelatorioModelo(relID),
    imaID int NOT NULL REFERENCES Imagem(imaID),
    UNIQUE(relID, imaID)
);

CREATE TABLE Parametro
(
    parID bigint IDENTITY PRIMARY KEY NOT NULL,
    pcoID int REFERENCES PropriedadeControle(pcoID),
    conID int REFERENCES Controle(conID) ON DELETE CASCADE,
    relID bigint REFERENCES Relatorio(relID),
    parNome varchar(45) NOT NULL,
    parTipoControle int NOT NULL, --parTipoControle: 0: valor único; 1: booleano; 2: lista de seleção única; 3: lista de seleção múltipla; 4: data; 5: moeda
    parTipoPropriedade int NOT NULL, -- parTipoPropriedade: 0: indefinida; 1: lista predefinida; 2: lista por consulta
    parTipoClasse varchar(100),
    parOrdem smallint,
    parValor varchar(512)
);
